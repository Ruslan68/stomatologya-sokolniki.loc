<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Стоматология метро Красносельская Преображенская площадь Солнечный остров звоните нам +7 495 603 30 00");
$APPLICATION->SetPageProperty("keywords", "Стоматология метро Красносельская, стоматология Красносельская, стоматология Преображенская площадь");
$APPLICATION->SetPageProperty("description", "Стоматология &quot;Солнечный остров&quot; приветствует Вас. Наш адрес: г. Москва, ст. м. &quot;Сокольники&quot;,  улица Шумкина, дом 11А");
$APPLICATION->SetTitle("Контакты");
?><section class="wrapper contact-info">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-6 wow  fadeInUp">
				<div class="title-section">
					<h1 class="text-block__title">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/title.php"
	)
);?>
					</h1>
				</div>
				<div class="contact-info-wrap" itemscope="" itemtype="http://schema.org/Organization">
					<meta itemprop="name" content="Солнечный остров">
					<a itemprop="url" href="https://stomatologya-sokolniki.ru" style="display:none" >https://stomatologya-sokolniki.ru</a>
					<img alt="Логотип Солнечный остров" src="/upload/template/image/logo-blue.png" title="Логотип Солнечный остров" style="display:none" itemprop="logo">
					<ul>
						<li>
							<i class="mdi mdi-phone"></i>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/phone.php"
	)
);?>
						</li>
						<li>
							<i class="mdi mdi-map-marker-radius"></i>
							<span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/address.php"
	)
);?>
							</span>
						</li>
						<li>
							<i class="mdi mdi-email-open"></i>
							<span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/email.php"
	)
);?>
							</span>
						</li>
						<li>
							<i class="mdi mdi-clock"></i>
							<span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/time.php"
	)
);?>
							</span>
						</li>
					</ul>
					<div class="social-contact">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/social_icon.php"
	)
);?>
					</div>
				</div>
			</div>
			
			<div class="col-xs-12 col-md-6 wow  fadeInUp">
<?$APPLICATION->IncludeComponent(
	"poiskpro:contact_form",
	"",
	Array(
		"AJAX_MODE" => "Y",
		"OK_TEXT" => "Сообщение отправлено",
		"REQUIRED_FIELDS" => array('NAME','EMAIL','MESSAGE'),
		"TYPE" => "contact_form"
	)
);?>
			</div>
        </div>
    </div>
</section>
	
<section class="wrapper address-info">
    <div class="container">
        <div class="title-section wow  fadeInUp">
			<div class="text-block__title">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/how_to_get.php"
	)
);?>
			</div>
        </div>
        <div class="row">
			<div class="col-xs-12 col-md-6 wow  fadeInLeft">
				<div class="address-info-item">
					<div class="title-wrap">
						<img src="/upload/template/image/parking.png"/><!--<i class="mdi mdi-subway-variant"></i>-->
						<span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text1.php"
	)
);?>
						</span>
					</div>
					<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text2.php"
	)
);?>
					</div>
				</div>
				<div class="address-info-item">
					<div class="title-wrap">
						<i class="mdi mdi-subway-variant"></i>
						<span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text3.php"
	)
);?>
						</span>
					</div>
					<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text4.php"
	)
);?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 wow  fadeInRight">
				<div class="address-info-item">
					<div class="title-wrap">
						<i class="mdi mdi-subway-variant"></i>
						<span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text5.php"
	)
);?>
						</span>
					</div>
					<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text6.php"
	)
);?>
					</div>
</div>

<div class="address-info-item">
					<div class="title-wrap">
						<i class="mdi mdi-subway-variant"></i>
					<span>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text7.php"
	)
);?>
					</span>
					</div>
					<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/contacts/text8.php"
	)
);?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
