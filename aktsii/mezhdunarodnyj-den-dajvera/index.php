<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Международный день дайвера");
?>
<div class="container">
	<div class="main-text">
		<p><a href="#" class="zapis_popup"><img src="/images/Mezhdunarodnyj-den-dajvera.png"></a></p>

		
        <h2 style="color:#FF0000">Акция окончена</h2><br>
		<h2>Акция “Международный день дайвера”</h2>
		<p>12 октября проходит акция “Международный день дайвера” — для всех водолазов и дайверов скидка на все терапевтические услуги 25%. Скидка действует целый день, независимо от времени приема. </p>
		
		<div class="action-implant">

			<table>
			<tbody style="max-height: none;">


			<tr class="parent" style="cursor: pointer;">
			<td colspan="2">Восстановление зуба пломбой 🔽</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td></td>
			<td>со скидкой</td>
			<td>старая цена</td>
			</tr>
			<tr class="child" style="display:  none;">
			<td class="first-col">Сошлифовывание твердых тканей зуба при лечении кариеса и его осложнений с диагностической целью</td>
			<td class="second-col">1 088 руб.</td>
			<td class="third-col">1450 руб.</td>
			</tr>

			<tr class="child" style="display:  none;"> 
			<td class="first-col">Избирательное пришлифовывание одного зуба</td>
			<td class="second-col">371 руб.</td>
			<td class="third-col">495 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Сошлифовывание твердых тканей зуба при лечении кариеса и его осложнений с помощью лазера</td>
			<td class="second-col">2 978 руб.</td>
			<td class="third-col">3970 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Штриппинг одного зуба</td>
			<td class="second-col">128 руб.</td>
			<td class="third-col">170 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой</td>
			<td class="second-col">2 624 руб.</td>
			<td class="third-col">3499 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой, с инсталляцией страза</td>
			<td class="second-col">5 569 руб.</td>
			<td class="third-col">7425 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Снятие страза</td>
			<td class="second-col">375 руб.</td>
			<td class="third-col">500 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой с использованием материалов из фотополимеров при лечении некариозных поражений</td>
			<td class="second-col">2 633 руб.</td>
			<td class="third-col">3510 руб.</td>
			</tr>

			<tr class="child" style="display:  none;"> 
			<td class="first-col">Восстановление зуба пломбой с использованием материалов из фотополимеров (Enamel+) при лечении некариозных поражений</td>
			<td class="second-col">4 088 руб.</td>
			<td class="third-col">5450 руб.</td>
			</tr>

			<tr class="child" style="display:  none;"> 
			<td class="first-col">Восстановление зуба пломбой I, V, VI класс по Блэку с использованием материалов из фотополимеров</td>
			<td class="second-col">4 110 руб.</td>
			<td class="third-col">5480 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой с нарушением контактного пункта II, III класс по Блэку с использованием материалов из фотополимеров</td>
			<td class="second-col">5 063 руб.</td>
			<td class="third-col">6750 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой IV класс по Блэку с использованием материалов из фотополимеров</td>
			<td class="second-col">5 843 руб.</td>
			<td class="third-col">7790 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой I, V, VI класс по Блэку с использованием материалов из фотополимеров (Enamel+)</td>
			<td class="second-col">5 843 руб.</td>
			<td class="third-col">7790 руб. </td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбой с нарушением контактного пункта II, III класс по Блэку с использованием материалов из фотополимеров (Enamel+)</td>
			<td class="second-col">7 365 руб.</td>
			<td class="third-col">9820 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба по поводу кариеса методом послойного художественного восстановления</td>
			<td class="second-col">9 308  руб.</td>
			<td class="third-col">12410 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба по поводу кариеса методом послойного художественного восстановления (Enamel+)</td>
			<td class="second-col">10 808 руб.</td>
			<td class="third-col">14410 руб. </td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба вкладками, виниром, полукоронкой из фотополимерного материала прямым методом </td>
			<td class="second-col">6 300 руб.</td>
			<td class="third-col">8400 руб. </td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Избирательное полирование зуба</td>
			<td class="second-col">375 руб.</td>
			<td class="third-col">500 руб. </td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбировочными материалами с использованием стекловолоконных штифтов </td>
			<td class="second-col">1 856 руб.</td>
			<td class="third-col">2475 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Восстановление зуба пломбировочными материалами с использованием титановых штифтов</td> 
			<td class="second-col">1 485 руб.</td>
			<td class="third-col">1980 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Трепанация зуба, искусственной коронки</td>
			<td class="second-col">825 руб.</td>
			<td class="third-col">1100 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Удаление эстетической вкладки (керамической, из диоксида циркония)</td>
			<td class="second-col">1 500 руб.</td>
			<td class="third-col">2000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Раскрытие полости зуба</td>
			<td class="second-col">525 руб.</td>
			<td class="third-col">700 руб.</td>
			</tr>


			<tr class="parent" style="cursor: pointer;">
			<td colspan="2">Временные пломбы 🔽</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td></td>
			<td>со скидкой</td>
			<td>старая цена</td>
			</tr>
			<tr class="child" style="display:  none;">
			<td class="first-col">Наложение временной пломбы</td>
			<td class="second-col">743 руб.</td>
			<td class="third-col">990 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Снятие временной пломбы</td>
			<td class="second-col">0 руб.</td>
			<td class="third-col">0 руб.</td>
			</tr>

			<tr class="parent" style="cursor: pointer;">
			<td colspan="3">Каппы и шины 🔽</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td></td>
			<td>со скидкой</td>
			<td>старая цена</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление страйкбольной каппы (2 челюсти)</td>
			<td class="second-col">4 125  руб.</td>
			<td class="third-col">5500 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление каппы для дайвинга (2 челюсти)</td>
			<td class="second-col">4 125 руб.</td>
			<td class="third-col">5500 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление спортивной защитной каппы (2 челюсти)</td>
			<td class="second-col">4 500 руб.</td>
			<td class="third-col">6000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление центрирующей спортивной защитной каппы (2 челюсти)</td>
			<td class="second-col">74 250 руб.</td>
			<td class="third-col">99000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление биометрической защитной каппы от храпа (2 челюсти)</td>
			<td class="second-col">74 250 руб.</td>
			<td class="third-col">99000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Разобщающая каппа</td>
			<td class="second-col">6 600 руб.</td>
			<td class="third-col">8800 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Каппа с set-ap перестановкой 2-4 зубов</td>
			<td class="second-col">8 250 руб.</td>
			<td class="third-col">11000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Защитная релаксационная каппа на 1 челюсть</td>
			<td class="second-col">4 819</td>
			<td class="third-col">6425 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление съемного артротика</td>
			<td class="second-col">74 250 руб.</td>
			<td class="third-col">99000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление элайнера для ортодонтического лечения lite case</td>
			<td class="second-col">79 500 руб.</td>
			<td class="third-col">106000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление элайнера для ортодонтического лечения medium case</td>
			<td class="second-col">123 000 руб.</td>
			<td class="third-col">164000 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Изготовление элайнера для ортодонтического лечения full case</td>
			<td class="second-col">165 000 руб.</td>
			<td class="third-col">220000 руб.</td>
			</tr>

			<tr class="parent" style="cursor: pointer">
			<td colspan="3" style="width: 100%;">Лечение осложнений кариеса (эндодонтическое лечение корневых каналов) 🔽</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td></td>
			<td>со скидкой</td>
			<td>старая цена</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Наложение девитализирующей пасты</td>
			<td class="second-col">510 руб.</td>
			<td class="third-col">680 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Наложение девитализирующей пасты при острой боли</td>
			<td class="second-col">1 163 руб.</td>
			<td class="third-col">1550 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Наложение медикаментозной повязки (пульпевит, ларгал, эндожи и проч.)</td>
			<td class="second-col">675 руб.</td>
			<td class="third-col">900 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Экстирпация пульпы</td>
			<td class="second-col">356 руб.</td>
			<td class="third-col">475 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Пульпотомия (ампутация коронковой пульпы)</td>
			<td class="second-col">1 740 руб.</td>
			<td class="third-col">2320 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Ультразвуковое расширение корневого канала зуба</td>
			<td class="second-col">675 руб.</td>
			<td class="third-col">900 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Распломбировка корневого канала, ранее леченного пастой</td>
			<td class="second-col">2 228 руб.</td>
			<td class="third-col">2970 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Распломбировка корневого канала, ранее леченного фосфат- цементом/ резорцинформальдегидным методом</td>
			<td class="second-col">2 970 руб.</td>
			<td class="third-col">3960 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Инструментальная и медикаментозная обработка корневого канала</td>
			<td class="second-col">1 856 руб. </td>
			<td class="third-col">2475 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Инструментальная и медикаментозная обработка - наложение коффердама</td>
			<td class="second-col">840 руб. </td>
			<td class="third-col">1120 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Инструментальная и медикаментозная обработка – наложение мягкого ретрактора Optragate</td>
			<td class="second-col">233 руб.</td>
			<td class="third-col">310 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Инструментальная и медикаментозная обработка – использование микроскопа в процессе лечения</td>
			<td class="second-col">2 925 руб.</td>
			<td class="third-col">3900 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Инструментальная и медикаментозная обработка плохо проходимого корневого канала</td>
			<td class="second-col">3 720 руб.</td>
			<td class="third-col">4960 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Инструментальная и медикаментозная обработка корневого канала повторная</td>
			<td class="second-col">743 руб.</td>
			<td class="third-col">990 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Пломбирование корневого канала зуба</td>
			<td class="second-col">1 856 руб. </td>
			<td class="third-col">2475 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Пломбирование корневого канала зуба пастой временное</td>
			<td class="second-col">1 125 руб.</td>
			<td class="third-col">1500 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Пломбирование корневого канала зуба гуттаперчевыми штифтами</td>
			<td class="second-col">1 856 руб.</td>
			<td class="third-col">2475 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Пломбирование корневого канала зуба методом вертикальной конденсации</td>
			<td class="second-col">3 713 руб.</td>
			<td class="third-col">4950 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Установка изолирующей (лечебной) прокладки</td>
			<td class="second-col">225 руб.</td>
			<td class="third-col">300 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Закрытие перфорации стенки корневого канала зуба</td>
			<td class="second-col">3 713 руб.</td>
			<td class="third-col">4950 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Фиксация внутриканального штифта/вкладки (титановый)</td>
			<td class="second-col">1 485 руб. </td>
			<td class="third-col">1980 руб.</td>
			</tr>


			<tr class="parent" style="cursor: pointer;">
			<td colspan="2">Шинирование зубов 🔽</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td></td>
			<td>со скидкой</td>
			<td>старая цена</td>
			</tr>

			<tr class="child" style="display:  none;" >
			<td class="first-col">Временное шинирование при заболеваниях пародонта (1 единица)</td>
			<td class="second-col">1 485 руб</td>
			<td class="third-col">1980 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Вантовое шинирование 1 зуба</td>
			<td class="second-col">3 713 руб.</td>
			<td class="third-col">4950 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Шинирование зубов системой Reebond, Glasspan (1 зуб)</td>
			<td class="second-col">1 485 руб. </td>
			<td class="third-col">1980 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Шинирование зубов с использованием флекс-дуг (1 зуб) без стоимости дуг</td>
			<td class="second-col">1 856 руб.</td>
			<td class="third-col">2475 руб.</td>
			</tr>

			<tr class="child" style="display:  none;">
			<td class="first-col">Лечебное шинирование подвижных зубов гелиокомпозитом</td>
			<td class="second-col">1 485 руб. </td>
			<td class="third-col">1980 руб.</td>
			</tr>


			<tr class="child" style="display:  none;">
			<td class="first-col">Постоянное шинирование цельнолитыми съемными конструкциями при заболеваниях пародонта (шинирующий бюгельный протез)</td>
			<td class="second-col">44 700 руб.</td>
			<td class="third-col">59600 руб.</td>
			</tr>

			<tr class="parent" style="cursor: pointer;">
			<td colspan="2"></td>
			</tr>

			</tbody>
			</table>

		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>