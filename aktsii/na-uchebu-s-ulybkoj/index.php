<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("на учебу с улыбкой");
?>

<div class="container">
	<div class="main-text">
		<p><a class="imgBattonForm" href=""><img src="/images/snova-v-chkoly.png"> <a class="imgBattonForm" href=""><img src="/images/button.png" style="margin-top: -80px;margin-left: 205px;width: 158px;"></a>
		</p>
		
		<h2 style= "color:#FF0000">Акция окончена</h2><br>
		
		<p>С 15 августа по 15 сентября проходит акция “На учебу с улыбкой” — для школьников, студентов и преподавателей* действует скидка 15% на все терапевтические услуги. Скидка действует каждый день, независимо от времени приема. </p>
		<p>*Чтобы воспользоваться скидкой, студентам нужно предъявить студенческий билет, преподавателям — удостоверение вуза. </p>


	
<div class="action-implant">

<table>
<tbody style="max-height: none;">


<tr class="parent" style="cursor: pointer;">
<td colspan="2">Восстановление зуба пломбой 🔽</td>
</tr>

<tr class="child" style="display:  none;">
<td></td>
<td>со скидкой</td>
<td>старая цена</td>
</tr>
<tr class="child" style="display:  none;">
<td class="first-col">Сошлифовывание твердых тканей зуба при лечении кариеса и его осложнений с диагностической целью</td>
<td class="second-col">1 232 руб.</td>
<td class="third-col">1450 руб.</td>
</tr>

<tr class="child" style="display:  none;"> 
<td class="first-col">Избирательное пришлифовывание одного зуба</td>
<td class="second-col">420 руб.</td>
<td class="third-col">495 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Сошлифовывание твердых тканей зуба при лечении кариеса и его осложнений с помощью лазера</td>
<td class="second-col">3 374 руб.</td>
<td class="third-col">3970 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Штриппинг одного зуба</td>
<td class="second-col">144 руб.</td>
<td class="third-col">170 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой</td>
<td class="second-col">2 974 руб.</td>
<td class="third-col">3499 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой, с инсталляцией страза</td>
<td class="second-col">6 311 руб.</td>
<td class="third-col">7425 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Снятие страза</td>
<td class="second-col">425 руб.</td>
<td class="third-col">500 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой с использованием материалов из фотополимеров при лечении некариозных поражений</td>
<td class="second-col">2 983 руб.</td>
<td class="third-col">3510 руб.</td>
</tr>

<tr class="child" style="display:  none;"> 
<td class="first-col">Восстановление зуба пломбой с использованием материалов из фотополимеров (Enamel+) при лечении некариозных поражений</td>
<td class="second-col">4 632 руб.</td>
<td class="third-col">5450 руб.</td>
</tr>

<tr class="child" style="display:  none;"> 
<td class="first-col">Восстановление зуба пломбой I, V, VI класс по Блэку с использованием материалов из фотополимеров</td>
<td class="second-col">4 658 руб.</td>
<td class="third-col">5480 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой с нарушением контактного пункта II, III класс по Блэку с использованием материалов из фотополимеров</td>
<td class="second-col">5 737 руб.</td>
<td class="third-col">6750 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой IV класс по Блэку с использованием материалов из фотополимеров</td>
<td class="second-col">6 621 руб.</td>
<td class="third-col">7790 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой I, V, VI класс по Блэку с использованием материалов из фотополимеров (Enamel+)</td>
<td class="second-col">6 621 руб.</td>
<td class="third-col">7790 руб. </td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбой с нарушением контактного пункта II, III класс по Блэку с использованием материалов из фотополимеров (Enamel+)</td>
<td class="second-col">8 347 руб.</td>
<td class="third-col">9820 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба по поводу кариеса методом послойного художественного восстановления</td>
<td class="second-col">10 548 руб.</td>
<td class="third-col">12410 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба по поводу кариеса методом послойного художественного восстановления (Enamel+)</td>
<td class="second-col">12 248 руб.</td>
<td class="third-col">14410 руб. </td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба вкладками, виниром, полукоронкой из фотополимерного материала прямым методом </td>
<td class="second-col">7 140 руб.</td>
<td class="third-col">8400 руб. </td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Избирательное полирование зуба</td>
<td class="second-col">425 руб.</td>
<td class="third-col">500 руб. </td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбировочными материалами с использованием стекловолоконных штифтов </td>
<td class="second-col">2 103 руб.</td>
<td class="third-col">2475 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Восстановление зуба пломбировочными материалами с использованием титановых штифтов</td> 
<td class="second-col">1 683 руб.</td>
<td class="third-col">1980 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Трепанация зуба, искусственной коронки</td>
<td class="second-col">935 руб.</td>
<td class="third-col">1100 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Удаление эстетической вкладки (керамической, из диоксида циркония)</td>
<td class="second-col">1 700 руб.</td>
<td class="third-col">2000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Раскрытие полости зуба</td>
<td class="second-col">595 руб.</td>
<td class="third-col">700 руб.</td>
</tr>


<tr class="parent" style="cursor: pointer;">
<td colspan="2">Временные пломбы 🔽</td>
</tr>

<tr class="child" style="display:  none;">
<td></td>
<td>со скидкой</td>
<td>старая цена</td>
</tr>
<tr class="child" style="display:  none;">
<td class="first-col">Наложение временной пломбы</td>
<td class="second-col">841 руб.</td>
<td class="third-col">990 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Снятие временной пломбы</td>
<td class="second-col">0 руб.</td>
<td class="third-col">0 руб.</td>
</tr>

<tr class="parent" style="cursor: pointer;">
<td colspan="3">Каппы и шины 🔽</td>
</tr>

<tr class="child" style="display:  none;">
<td></td>
<td>со скидкой</td>
<td>старая цена</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление страйкбольной каппы (2 челюсти)</td>
<td class="second-col">4 675 руб.</td>
<td class="third-col">5500 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление каппы для дайвинга (2 челюсти)</td>
<td class="second-col">4 675 руб.</td>
<td class="third-col">5500 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление спортивной защитной каппы (2 челюсти)</td>
<td class="second-col">5 100 руб.</td>
<td class="third-col">6000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление центрирующей спортивной защитной каппы (2 челюсти)</td>
<td class="second-col">84 150 руб.</td>
<td class="third-col">99000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление биометрической защитной каппы от храпа (2 челюсти)</td>
<td class="second-col">84 150 руб.</td>
<td class="third-col">99000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Разобщающая каппа</td>
<td class="second-col">7 480 руб.</td>
<td class="third-col">8800 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Каппа с set-ap перестановкой 2-4 зубов</td>
<td class="second-col">9 350 руб.</td>
<td class="third-col">11000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Защитная релаксационная каппа на 1 челюсть</td>
<td class="second-col">5 461</td>
<td class="third-col">6425 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление съемного артротика</td>
<td class="second-col">84 150 руб.</td>
<td class="third-col">99000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление элайнера для ортодонтического лечения lite case</td>
<td class="second-col">90 100 руб.</td>
<td class="third-col">106000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление элайнера для ортодонтического лечения medium case</td>
<td class="second-col">139 400 руб.</td>
<td class="third-col">164000 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Изготовление элайнера для ортодонтического лечения full case</td>
<td class="second-col">187 000 руб.</td>
<td class="third-col">220000 руб.</td>
</tr>

<tr class="parent" style="cursor: pointer">
<td colspan="3" style="width: 100%;">Лечение осложнений кариеса (эндодонтическое лечение корневых каналов) 🔽</td>
</tr>

<tr class="child" style="display:  none;">
<td></td>
<td>со скидкой</td>
<td>старая цена</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Наложение девитализирующей пасты</td>
<td class="second-col">578 руб.</td>
<td class="third-col">680 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Наложение девитализирующей пасты при острой боли</td>
<td class="second-col">1 317 руб.</td>
<td class="third-col">1550 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Наложение медикаментозной повязки (пульпевит, ларгал, эндожи и проч.)</td>
<td class="second-col">765 руб.</td>
<td class="third-col">900 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Экстирпация пульпы</td>
<td class="second-col">403 руб.</td>
<td class="third-col">475 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Пульпотомия (ампутация коронковой пульпы)</td>
<td class="second-col">1 972 руб.</td>
<td class="third-col">2320 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Ультразвуковое расширение корневого канала зуба</td>
<td class="second-col">765 руб.</td>
<td class="third-col">900 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Распломбировка корневого канала, ранее леченного пастой</td>
<td class="second-col">2 524 руб.</td>
<td class="third-col">2970 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Распломбировка корневого канала, ранее леченного фосфат- цементом/ резорцинформальдегидным методом</td>
<td class="second-col">3 366 руб.</td>
<td class="third-col">3960 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Инструментальная и медикаментозная обработка корневого канала</td>
<td class="second-col">2 103 руб. </td>
<td class="third-col">2475 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Инструментальная и медикаментозная обработка - наложение коффердама</td>
<td class="second-col">952 руб. </td>
<td class="third-col">1120 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Инструментальная и медикаментозная обработка – наложение мягкого ретрактора Optragate</td>
<td class="second-col">263 руб.</td>
<td class="third-col">310 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Инструментальная и медикаментозная обработка – использование микроскопа в процессе лечения</td>
<td class="second-col">3 315 руб.</td>
<td class="third-col">3900 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Инструментальная и медикаментозная обработка плохо проходимого корневого канала</td>
<td class="second-col">4 216 руб.</td>
<td class="third-col">4960 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Инструментальная и медикаментозная обработка корневого канала повторная</td>
<td class="second-col">841 руб.</td>
<td class="third-col">990 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Пломбирование корневого канала зуба</td>
<td class="second-col">2 103 руб. </td>
<td class="third-col">2475 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Пломбирование корневого канала зуба пастой временное</td>
<td class="second-col">1 275 руб.</td>
<td class="third-col">1500 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Пломбирование корневого канала зуба гуттаперчевыми штифтами</td>
<td class="second-col">2 103 руб.</td>
<td class="third-col">2475 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Пломбирование корневого канала зуба методом вертикальной конденсации</td>
<td class="second-col">4 207 руб.</td>
<td class="third-col">4950 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Установка изолирующей (лечебной) прокладки</td>
<td class="second-col">255 руб.</td>
<td class="third-col">300 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Закрытие перфорации стенки корневого канала зуба</td>
<td class="second-col">4 207 руб.</td>
<td class="third-col">4950 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Фиксация внутриканального штифта/вкладки (титановый)</td>
<td class="second-col">1 683 руб. </td>
<td class="third-col">1980 руб.</td>
</tr>


<tr class="parent" style="cursor: pointer;">
<td colspan="2">Шинирование зубов 🔽</td>
</tr>

<tr class="child" style="display:  none;">
<td></td>
<td>со скидкой</td>
<td>старая цена</td>
</tr>

<tr class="child" style="display:  none;" >
<td class="first-col">Временное шинирование при заболеваниях пародонта (1 единица)</td>
<td class="second-col">1 683 руб</td>
<td class="third-col">1980 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Вантовое шинирование 1 зуба</td>
<td class="second-col">4 207 руб.</td>
<td class="third-col">4950 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Шинирование зубов системой Reebond, Glasspan (1 зуб)</td>
<td class="second-col">1 683 руб. </td>
<td class="third-col">1980 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Шинирование зубов с использованием флекс-дуг (1 зуб) без стоимости дуг</td>
<td class="second-col">2 103 руб.</td>
<td class="third-col">2475 руб.</td>
</tr>

<tr class="child" style="display:  none;">
<td class="first-col">Лечебное шинирование подвижных зубов гелиокомпозитом</td>
<td class="second-col">1 683 руб. </td>
<td class="third-col">1980 руб.</td>
</tr>


<tr class="child" style="display:  none;">
<td class="first-col">Постоянное шинирование цельнолитыми съемными конструкциями при заболеваниях пародонта (шинирующий бюгельный протез)</td>
<td class="second-col">50 660 руб.</td>
<td class="third-col">59600 руб.</td>
</tr>

<tr class="parent" style="cursor: pointer;">
<td colspan="2"></td>
</tr>

</tbody>
</table>

		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

