<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("имплантация под ключ");
?>

<div class="container">
	<div class="main-text">
		<p><a href=""><img src="/images/implantacia_0818.png"></a></p>

		<h2 style= "color:#FF0000">Акция окончена</h2><br>

		<h2>Счастливые часы для имплантации</h2>
		<p>С 17.08.18 по 17.09.18 проходит акция — счастливые часы для имплантации. С 9:00 до 15:00 имплантация дешевле:</p>
		<div class="action-implant">
			<table>
				<tbody>
				<tr class="table-th">
					<td>Система имплантов</td>
					<td>Цена в счастливые часы</td>
					<td>Обычная цена</td> 
				</tr>
				<tr>
					<td class="first-col">Система Осстем (Корея)</td>
					<td class="second-col">35 000 Р</td>
					<td class="third-col">55 000 Р</td> 
				</tr>
				<tr>
					<td class="first-col">Система Анкилоз (Германия)</td>
					<td class="second-col">44 900 Р</td>
					<td class="third-col">73 700 Р</td> 
				</tr> 
				<tr>
					<td class="first-col">Система Нобель (США)</td>
					<td class="second-col">51 300 Р</td>
					<td class="third-col">95 900 Р</td> 
				</tr>
				<tr>
					<td class="first-col">Система Астратек (Швеция)</td>
					<td class="second-col">59 500 Р</td>
					<td class="third-col">105 100 Р</td> 
				</tr> 								
				</tbody>
			</table>
			<p>Стоимость включает имплантацию зуба под ключ: имплант + абатмент + коронка + анестезия + слепки. </p>
			<p>Имплантацию делает доктор <a href="/specialists/gaskin-dmitriy-vladimirovich/">Гаскин Дмитрий Владимирович.</a></p>

		</div>
	</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>