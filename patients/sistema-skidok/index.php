<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Система скидок");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» делает скидки в период с 1 июля 2017 года по 31 декабря 2018 года. Подробности можно узнать по телефонам, указанным на сайте.");
$APPLICATION->SetTitle("Система скидок");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumb",
	Array(
		"PATH" => "",
		"SITE_ID" => SITE_ID,
		"START_FROM" => "0"
	)
);?>

<section class="wrapper system-offers">
    <?/*<div class="left-img-container">
        <div class="main-about__image">
			<!--<img src="image/main-about-img.png" alt="">-->
        </div>
    </div>*/?>
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-12">

	<div class="left-img-container">
        <div class="main-about__image">
        </div>
    </div>

				<div class="title-section">
					<h1 class="text-block__title">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/sistema-skidok/text0.php"
	)
);?>
					</h1>
				</div>
				<div class="three-l-title-wrap title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/sistema-skidok/text1.php"
	)
);?>
				</div>
				<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/sistema-skidok/text2.php"
	)
);?>
				</div>
			</div>
        </div>
    </div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
