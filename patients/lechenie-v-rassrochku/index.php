<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Вставить зубы в рассрочку можно в нашей клинике. Звоните +7(495) 603-30-00");
$APPLICATION->SetPageProperty("keywords", "вставить зубы в рассрочку");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» предлагает лечение в рассрочку. Мы предлагаем выгодные условия, которые можно узнать, позвонив по телефонам, указанным на сайте. ");
$APPLICATION->SetTitle("Лечение в кредит");
?><?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumb",
	Array(
		"PATH" => "",
		"SITE_ID" => SITE_ID,
		"START_FROM" => "0"
	)
);?> <section class="wrapper credit-info">
<div class="container">
	<div class="col-xs-12 col-md-6 col-md-offset-6">
		<h1 class="title-section">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/installments_text1.php"
	)
);?>
		</h1>

<!--<div class="credit-item">
			 <?/*$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/installments_text2.php"
	)
);*/?>
		</div>-->

	</div>
</div>
 </section> <section class="wrapper credit-wrapper">
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-12">
			<div class="credit-item">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/installments_text3.php"
	)
);?>
			</div>
		</div>
		<div class="col-xs-12 col-md-12">
			<div class="credit-item">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/installments_text4.php"
	)
);?>
			</div>
		</div>
		<div class="col-xs-12 col-md-12">
			<div class="credit-item">
				 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/installments_text5.php"
	)
);?>
			</div>
		</div>
	</div>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"credit_form",
	Array(
		"AJAX_MODE" => "Y",
		"OK_TEXT" => "Заявка принята",
		"REQUIRED_FIELDS" => array('NAME','PHONE'),
		"TYPE" => "credit_form"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
