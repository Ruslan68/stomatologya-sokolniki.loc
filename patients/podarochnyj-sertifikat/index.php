<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Подарочный сертификат");
$APPLICATION->SetPageProperty("keywords", "стоматология в Сокольниках, стоматологическая клиника &quot;Солнечный остров&quot;, стоматологическая клиника, лазер, эстетика,");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» предлагает подарочные сертификаты. Вы можете приобрести сертификат на любую сумму. Подробности — по телефонам, указанным на сайте. ");
$APPLICATION->SetTitle("Подарочные сертификаты");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumb",
	Array(
		"PATH" => "",
		"SITE_ID" => SITE_ID,
		"START_FROM" => "0"
	)
);?>


<section class="wrapper present-info">
    <div class="container">
        <div class="title-section">
			<h1 class="text-block__title">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text0.php"
	)
);?>
			</h1>
        </div>
        <div class="row">
			<div class="col-xs-12 col-md-6 col-md-offset-6">
				<div class="present-item">
					<div class="three-l-title-wrap title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text1.php"
	)
);?>
					</div>
					<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text2.php"
	)
);?>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<section class="wrapper present-wrapper">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="present-item">
					<div class="title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text3.php"
	)
);?>
					</div>
					<div class="list-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text4.php"
	)
);?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="blue-text-item">
					<div class="img-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text5.php"
	)
);?>
					</div>
					<div class="text-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text6.php"
	)
);?>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>
<section class="wrapper present-look">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="blue-text-item">
					<div class="img-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text7.php"
	)
);?>
					</div>
					<div class="text-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text8.php"
	)
);?>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="present-item">
					<div class="title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text9.php"
	)
);?>
					</div>
					<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text10.php"
	)
);?>
					</div>
					<div class="image-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/podarochnyj-sertifikat/text11.php"
	)
);?>
					</div>
				</div>
			</div>
		</div>
    </div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
