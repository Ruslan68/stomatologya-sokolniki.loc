<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?$this->setFrameMode(true);?>	


<div class="poiskpro-form">
	<div class="title-form">
		<?=GetMessage("MFT_MAKE_APPOINT")?>
	</div>
	<div class="message">
	<?if(!empty($arResult["ERROR_MESSAGE"])){?>
		<div class="bg-danger">
		<?foreach($arResult["ERROR_MESSAGE"] as $v)
			ShowError($v);
		?>		
		</div>
	<?}?>
	<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
		<div class="bg-success">
			<?=$arResult["OK_MESSAGE"]?>
		</div>
	<?}?>

	<!-- targets form popup zapis na priem-->
	<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
		<script type="text/javascript">
			$(document).ready(function() {
				console.log("from_popup_zapis_na_priem");
		        sendYandex('Form_Submit_Order', 'Macroconversion');
		        sendGtag('Submit', 'Form', 'Order_header');

		        sendYandex('Form_Submitm_Schedule_an_appointment_popup', 'Macroconversion_marketing');
		        sendGtag('Submitm', 'Form', 'Schedule_an_appointment_popup'); 
			});
		</script>
	<?}?>

	</div>
	<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
		<?=bitrix_sessid_post()?>
		<label for="app-name">
			<span><?=GetMessage("MFT_NAME")?></span>
			<input type="text" id="app-name" placeholder=""<?if(isset($arResult['ERROR']["user_name"])) echo ' class="has-error"';?>  name="user_name" value="<?=$arResult['ACTIVE']["user_name"]?>"/>
		</label>
		<label for="app-tel">
			<span><?=GetMessage("MFT_PHONE")?></span>
			<input type="text" id="app-tel" placeholder="" <?if(isset($arResult['ERROR']["user_phone"])) echo ' class="has-error"';?> name="user_phone" value="<?=$arResult['ACTIVE']["user_phone"]?>"/>
		</label>
		<div class="personal-agreement">
			<input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-testim3" name="personal-agree" type="checkbox" value="1"<?if($arResult['ACTIVE']["personal-agree"] == 1) echo ' checked';?>>
			<label for="personal-agree-testim3"><div class="bold"><?=GetMessage("MFT_AGREE1")?>:</div></label><br>
			<span> <?=GetMessage("MFT_AGREE2")?> <br> <?=GetMessage("MFT_AGREE3")?> <a href="/upload/template/pdf/152-f3.pdf" target="_blank">«<?=GetMessage("MFT_AGREE4")?>»</a></span>
		</div>
		<label class="submit-pup" for="submit-app">
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
			<input type="hidden" name="action_order" value="" />
			<input id="submit-app" type="submit" class="btn-blue" value="<?=GetMessage("MFT_SUBMIT2")?>"/>
		</label>
	</form>
</div>
<script>
	$('.popup.popup-appointment form #submit-app').click(function(){
		yaCounter32528320.reachGoal('ysp-zapis-na-priem');
		ga('send', 'pageview', '/ysp-zapis-na-priem');
		console.log('ysp-zapis-na-priem');
	});
</script>
