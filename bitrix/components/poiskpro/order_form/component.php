<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
	$arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" && (isset($_POST["PARAMS_HASH"]) && $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	$arResult["ERROR_MESSAGE"] = array();
	$error=array();
	if(check_bitrix_sessid())
	{
		
		if(in_array("NAME",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_name"]) <= 1){
			$error["user_name"]=1;
		}else{
			$user_name = strip_tags($_POST['user_name']);
			$user_name = mysql_escape_string($user_name);
		}	
		if(in_array("PHONE",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_phone"]) <= 1){
			$error["user_phone"]=1;
		}else{
			$user_phone = strip_tags($_POST['user_phone']);
			$user_phone = mysql_escape_string($user_phone);
		}

		if (!isset($_POST["personal-agree"])) {
			$error["personal-agree"]=1;
		}

		if(!empty($error)){
			$arResult["ERROR_MESSAGE"][]=GetMessage("ERR_MESS");
		}	
						
		if(empty($arResult["ERROR_MESSAGE"]))
		{
			
			$arFields = Array(
				"AUTHOR" => $user_name,
				"AUTHOR_PHONE" => $user_phone
			);
			
			if($arParams['TYPE'] == 'popup_order'){
				
				$arFields['ACTION'] = intval($_POST['action_order']) > 0 ? GetMessage("ACTION_ID").' №'.intval($_POST['action_order']) : '';
				
			}
			
			if($arParams['TYPE'] == 'popup_call'){
				
				CEvent::Send("CALL_FEEDBACK", SITE_ID, $arFields);
				
			}elseif($arParams['TYPE'] == 'credit_form'){
				
				CEvent::Send("CREDIT_FEEDBACK", SITE_ID, $arFields);
				
			}elseif($arParams['TYPE'] == 'appointment'){
				$arFields['TIME_APPOINTMENT'] = htmlspecialchars($_POST['TIME_APPOINTMENT']);
				$arFields['DOCTOR_NAME'] = htmlspecialchars($_POST['doctor_name']);
				$arResult["TIME_APPOINTMENT"] =$arFields['TIME_APPOINTMENT'] ;
				$arResult["CEvent"] = CEvent::Send("FORM_APPOINTMENT_TIME", SITE_ID, $arFields);
			}
			else{
				
				CEvent::Send("ORDER_FEEDBACK", SITE_ID, $arFields);
				
			}
			
			$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
			
			$arResult['ACTIVE']=array();
			$arResult["ERROR_MESSAGE"]=array();
			$arResult['ERROR']=array();
			
		}else{
			$arResult['ERROR']=$error;
			$arResult['ACTIVE']["user_name"] = $user_name;
			$arResult['ACTIVE']["user_phone"] = $user_phone;
			
			$arResult['ACTIVE']["personal-agree"] = $_POST["personal-agree"];
		}
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}

$this->IncludeComponentTemplate();
