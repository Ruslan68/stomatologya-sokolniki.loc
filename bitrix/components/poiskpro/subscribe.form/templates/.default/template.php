<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?$this->setFrameMode(true);?>	

<div class="poiskpro-form">
	<div class="title-form">
		<?=GetMessage("MFT_SUBSCRIBE_TITLE")?>
	</div>
	<div class="message">
	<?if(!empty($arResult["ERROR_MESSAGE"])){?>
		<div class="bg-danger">
		<?foreach($arResult["ERROR_MESSAGE"] as $v)
			ShowError($v);
		?>		
		</div>
	<?}?>
	<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
		<div class="bg-success">
			<?=$arResult["OK_MESSAGE"]?>
		</div>
	<?}?>

	<!-- targets form subscribe -->
	<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
		<script type="text/javascript">
			$(document).ready(function() {
				console.log("from_subscribe");
		        sendYandex('Form_Submit_Subscribe');
		        sendGtag('Submit', 'Form', 'Submit_Subscribe'); 

		        sendYandex('Form_Subscribe_Subscribe');
		        sendGtag('Submit', 'Form', 'Subscribe');
			});
		</script>
	<?}?>

	</div>
	<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
		<?=bitrix_sessid_post()?>
		<label for="call-email">
			<span><?=GetMessage("MFT_EMAIL")?></span>
			<input type="text" id="call-email" placeholder=""<?if(isset($arResult['ERROR']["user_email"])) echo ' class="has-error"';?>  name="user_email" value="<?=$arResult['ACTIVE']["user_email"]?>"/>
		</label>
		<div class="personal-agreement">
			<input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-testim2" name="personal-agree" type="checkbox" value="1"<?if($arResult['ACTIVE']["personal-agree"] == 1) echo ' checked';?>>
			<label for="personal-agree-testim2"><div class="bold"><?=GetMessage("MFT_AGREE1")?>:</div></label><br>
			<span> <?=GetMessage("MFT_AGREE2")?> <br> <?=GetMessage("MFT_AGREE3")?> <a href="/upload/template/pdf/152-f3.pdf" target="_blank">«<?=GetMessage("MFT_AGREE4")?>»</a></span>
		</div>
		<label class="submit-pup" for="submit-call">
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
            <input id="submit-call" type="submit" class="btn-blue" value="<?=GetMessage("MFT_SUBMIT")?>"/>
        </label>
	</form>
</div>	
        
