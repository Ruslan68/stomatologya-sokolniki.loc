<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());


$arParams["OK_TEXT"] = GetMessage("MF_OK");

if($_SERVER["REQUEST_METHOD"] == "POST" && (isset($_POST["PARAMS_HASH"]) && $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	$arResult["ERROR_MESSAGE"] = array();
	$error=array();
	if(check_bitrix_sessid())
	{
		if (!isset($_POST["personal-agree"])) {
			$error["personal-agree"]=1;
		}
		
		if(in_array("EMAIL",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_email"]) <= 1){
			$error["user_email"]=1;
		}else{
			$user_email = strip_tags($_POST['user_email']);
			$user_email = mysql_escape_string($user_email);
		}
		

		if(!empty($error)){
			$arResult["ERROR_MESSAGE"][]=GetMessage("ERR_MESS");
		}	
		
		if(in_array("EMAIL",$arParams["REQUIRED_FIELDS"]) && strlen($user_email) > 1 && !check_email($user_email)){
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_EMAIL_NOT_VALID");
			$error["user_email"]=1;
		}
		
		$code = '';
		
		if(empty($arResult["ERROR_MESSAGE"]))
		{
			
			CModule::IncludeModule("iblock");
			
			$arSelect = Array("ID", "NAME","ACTIVE", 'PROPERTY_CODE');
			$arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], 'NAME' => $user_email);
	
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	
			if($ob = $res->GetNext()){
			
				if($ob['ACTIVE'] == 'Y'){
					
					$arResult["ERROR_MESSAGE"][] = 'Вы уже подписаны';
					
				}else{	
					
					$code = $ob['PROPERTY_CODE_VALUE'];
					
				}	
		
			}
			
			
		}
		
		if(empty($arResult["ERROR_MESSAGE"]))
		{	
			$el = new CIBlockElement;
			
			if(empty($code)){
				
				$code = md5($arParams['SOLT'].$user_email);
				
				$arLoadProductArray = Array(
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"      => $arParams["IBLOCK_ID"],
					"PROPERTY_VALUES"=> array(
						"CODE" => trim($code),
					),
					"NAME"           => trim($user_email),
					"ACTIVE"         => "N",
					"DATE_ACTIVE_FROM"	=> date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")))
				);
				
				$el->Add($arLoadProductArray);
			
			}
			
			$domen = COption::GetOptionString("main", "server_name");
			$link = 'http://'.$domen.$arParams['PAGE']. '?code='.$code;
			
			$arFields = Array(
				"AUTHOR_EMAIL" => $user_email,
				"LINK" => $link
			);
			
			
			CEvent::Send("SUBSCRIBE_FORM", SITE_ID, $arFields);
				
			$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"].' '.$user_email;
				
			$arResult['ACTIVE']=array();
			$arResult["ERROR_MESSAGE"]=array();
			$arResult['ERROR']=array();
			
			
			
		}else{
			$arResult['ERROR']=$error;
			$arResult['ACTIVE']["user_email"] = $user_email;
			$arResult['ACTIVE']["personal-agree"] = $_POST["personal-agree"];
		}
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}

$this->IncludeComponentTemplate();
