<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
 
$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
	$arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());


$cacheId = $arParams["IBLOCK_ID_DOCTOR"].$this->GetTemplateName();
$сache = Bitrix\Main\Data\Cache::createInstance();

if ($сache->initCache(86400, $cacheId, '/'))
{
    $result = $сache->getVars();
}
elseif ($сache->startDataCache())
{
	CModule::IncludeModule("iblock");
    $result = array();
    
	$arSelect = Array("ID", "NAME","IBLOCK_ID","PROPERTY_PROFESSION","ACTIVE");
	$arFilter = Array("ACTIVE"=>"Y","IBLOCK_ID" => $arParams["IBLOCK_ID_DOCTOR"]);
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	
	while($ob = $res->GetNext()){
		
		$result[$ob['ID']] = $ob;
		
	}
		
    $сache->endDataCache($result);
} 


$arResult["DOCTORS"] = $result;


if($_SERVER["REQUEST_METHOD"] == "POST" &&(isset($_POST["PARAMS_HASH"]) && $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	$arResult["ERROR_MESSAGE"] = array();
	$error=array();
	if(check_bitrix_sessid())
	{
		
		if(in_array("NAME",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_name"]) <= 1){
			$error["user_name"]=1;
		}else{
			$user_name = strip_tags($_POST['user_name']);
			$user_name = mysql_escape_string($user_name);
		}	
		
		if(in_array("EMAIL",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_email"]) <= 1){
			$error["user_email"]=1;
		}else{
			$user_email = strip_tags($_POST['user_email']);
			$user_email = mysql_escape_string($user_email);
		}
		
		if(in_array("PHONE",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_phone"]) <= 1){
			$error["user_email"]=1;
		}else{
			$user_phone = strip_tags($_POST['user_phone']);
			$user_phone = mysql_escape_string($user_phone);
		}
		
		if(in_array("MESSAGE",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["MESSAGE"]) <= 3){
			$error["MESSAGE"]=1;
		}else{
			$MESSAGE = strip_tags($_POST['MESSAGE']);
			$MESSAGE = mysql_escape_string($MESSAGE);
		}

		if (!isset($_POST["personal-agree"])) {
			$error["personal-agree"]=1;
		}

		if(!empty($error)){
			$arResult["ERROR_MESSAGE"][]=GetMessage("ERR_MESS");
		}	
		
		if(in_array("EMAIL",$arParams["REQUIRED_FIELDS"]) && strlen($user_email) > 1 && !check_email($user_email)){
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_EMAIL_NOT_VALID");
			$error["user_email"]=1;
		}
		
		$doctor = false;
		
		if(isset($_POST['professional']) && intval($_POST['professional'])>0){
			
			$doctor = intval($_POST['professional']);
			
		}
						
		if(empty($arResult["ERROR_MESSAGE"]))
		{
			if(sizeof($_FILES)!=0){
				$arIMAGE = $_FILES["file"];
				$arIMAGE["MODULE_ID"] = "iblock";
				$fid = CFile::SaveFile($arIMAGE, "reviews");
				$file = CFile::GetPath($fid);
			}

			$prop = array(                
				66 => CFile::GetFileArray($fid),
            );

			CModule::IncludeModule("iblock");
			
			$el = new CIBlockElement;
			
			
	
			$arLoadProductArray = Array(
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID"      => $arParams["IBLOCK_ID"],
				"PROPERTY_VALUES"=> array(
					"EMAIL" => trim($user_email),
					"PHONE" => trim($user_phone),
					'DOCTOR' => $doctor,
					'CLIENT_PHOTO' => CFile::GetFileArray($fid)
				),
				"NAME"           => trim($user_name),
				"ACTIVE"         => "N",
				"PREVIEW_TEXT"	 => $MESSAGE,
				"DATE_ACTIVE_FROM"	=> date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")))
			);
	
			$el->Add($arLoadProductArray);
			
			$about = $doctor > 0 ? $arResult["DOCTORS"][$doctor]['NAME'] : 'О компании';
			
			$arFields = Array(
				"AUTHOR" => $user_name,
				"AUTHOR_EMAIL" => $user_email,
				"AUTHOR_PHONE" => $user_phone,
				"TEXT" => $MESSAGE,
				'ABOUT' => $about
			);
			
			CEvent::Send("REVIEW_FORM", SITE_ID, $arFields);
			
			$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
			
			$arResult['ACTIVE']=array();
			$arResult["ERROR_MESSAGE"]=array();
			$arResult['ERROR']=array();
			
		}else{
			$arResult['ERROR']=$error;
			$arResult['ACTIVE']["MESSAGE"] = $MESSAGE;
			$arResult['ACTIVE']["user_name"] = $user_name;
			$arResult['ACTIVE']["user_phone"] = $user_phone;
			$arResult['ACTIVE']["user_email"] = $user_email;
			$arResult['ACTIVE']["professional"] = $doctor;
			
			$arResult['ACTIVE']["personal-agree"] = $_POST["personal-agree"];
		}
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}



$this->IncludeComponentTemplate();
