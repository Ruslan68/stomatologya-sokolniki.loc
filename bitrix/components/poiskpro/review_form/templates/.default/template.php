<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?><!--<?$this->setFrameMode(true);?>-->


<section class="wrapper form-bottom form-testim">
    <div class="container">
		<div class="footer-form poiskpro-form">
			<div class="title-form">
				<?=GetMessage("MFT_GIVE_FEEDBACK")?>
			</div>

<?php
if(isset($_GET['opa'])){
	echo "..........";
}
?>


			<div class="message">
				<?if(!empty($arResult["ERROR_MESSAGE"])){?>
				<div class="bg-danger">
				<?foreach($arResult["ERROR_MESSAGE"] as $v)
					ShowError($v);
				?>		
				</div>
				<?}?>
				<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
				<div class="bg-success">
					<?=$arResult["OK_MESSAGE"]?>
				</div>
				<?}?>
			</div>
			<form action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data">
			<?=bitrix_sessid_post()?>
				<label for="fot-name-testim">
					<span><?=GetMessage("MFT_NAME")?></span>
					<input type="text" id="fot-name-testim" placeholder=""<?if(isset($arResult['ERROR']["user_name"])) echo ' class="has-error"';?>  name="user_name" value="<?=$arResult['ACTIVE']["user_name"]?>"/>
				</label>
				<label>
					<span><?=GetMessage("MFT_ABOUT_FEED")?></span>
					<?if(isset($arResult["DOCTORS"]) && !empty($arResult["DOCTORS"])){?>
					
					<select name="professional" id="professional">
						<option value="0"><?=GetMessage("MFT_A_CLINICK")?></option>
						<?foreach($arResult["DOCTORS"] as $key=>$item){?>
						<option value="<?=$item["ID"]?>"<?if($arResult['ACTIVE']["professional"] == $item["ID"]){?> selected<?}?>><?=$item['NAME']?></option>
						<?}?> 
					</select>
					
					<?}?>
					
				</label>
				
				<label for="fot-mail-testim">
					<span><?=GetMessage("MFT_EMAIL")?></span>
					<input type="email" id="fot-mail-testim" placeholder=""<?if(isset($arResult['ERROR']["user_email"])) echo ' class="has-error"';?> name="user_email" value="<?=$arResult['ACTIVE']["user_email"]?>"/>
				</label>

				<label for="fot-tel-testim">
					<span><?=GetMessage("MFT_PHONE")?></span>
					<input type="text" id="fot-tel-testim" placeholder="" <?if(isset($arResult['ERROR']["user_phone"])) echo ' class="has-error"';?> name="user_phone" value="<?=$arResult['ACTIVE']["user_phone"]?>"/>
				</label>
				<label for="fot-mes-testim">
					<span><?=GetMessage("MFT_MESSAGE")?></span>
					<textarea id="fot-mes-testim"<?if(isset($arResult['ERROR']["MESSAGE"])) echo ' class="has-error"';?> placeholder="" name="MESSAGE"><?=$arResult['ACTIVE']["MESSAGE"]?></textarea>
				</label>
				<div class="file-upload">
                    <span>Прикрепить фото</span>
                    <div class="button">
						<span>Выберите файл</span> <input type="file" name="file">
					</div>
                    <div class="file-upload__text">
                        Файл не выбран
                    </div>
                </div>
				<div class="personal-agreement">
					<input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-testim" name="personal-agree" type="checkbox" value="1"<?if($arResult['ACTIVE']["personal-agree"] == 1) echo ' checked';?>>
					<label for="personal-agree-testim"><div class="bold"><?=GetMessage("MFT_AGREE1")?>:</div></label><br>
					<span> <?=GetMessage("MFT_AGREE2")?> <br> <?=GetMessage("MFT_AGREE3")?> <a href="/upload/template/pdf/152-f3.pdf" target="_blank">«<?=GetMessage("MFT_AGREE4")?>»</a></span>
				</div>
				<label for="submit-fot">
					<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
					<input id="submit-fot" type="submit" class="btn-blue" value="<?=GetMessage("MFT_SUBMIT")?>">
				</label>
			</form>
		</div>
    </div>
</section>
