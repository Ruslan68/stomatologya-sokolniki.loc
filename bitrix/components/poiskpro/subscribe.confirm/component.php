<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);

if(isset($_GET['code']) && !empty($_GET['code']))
{
			
	CModule::IncludeModule("iblock");
			
	$arSelect = Array("ID", "NAME","ACTIVE", 'PROPERTY_CODE');
	$arFilter = Array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"], 
		'ACTIVE' => 'N',
		'PROPERTY_CODE_VALUE' => $_GET['code']
	);
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	
	if($ob = $res->GetNext()){
		
		//echo "<pre>";print_r($ob);echo "</pre>";die();
			
		$el = new CIBlockElement;
		
		$el->Update($ob['ID'],array('ACTIVE' => 'Y'));	
		
		$arResult['EMAIL'] = $ob['NAME'];
		
			
	}else{
		
		LocalRedirect('/');
		die();
		
	}

}else{
	
	LocalRedirect('/');
	die();
	
}

$this->IncludeComponentTemplate();
