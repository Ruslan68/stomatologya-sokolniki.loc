<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?$this->setFrameMode(true);?>	

<section class="wrapper price-wrapper">
    <div class="container">
        <div class="title-section">
			<div class="text-block__title">
				Подписка			
			</div>
        </div>
    </div>

<div class="container">
    <div class="row">
		<div class="subscribe_confirm">
			Спасибо, ваш почтовый адрес <span><?=$arResult['EMAIL']?></span> подтвержден
		</div>
    </div>
</div>	

</section>
<script>
	
	setTimeout(function(){
		window.location.reload();
	},5000);	

</script>
        