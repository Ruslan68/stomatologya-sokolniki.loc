<?php
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$arResult["PARAMS_HASH"] = md5(serialize($arParams).$this->GetTemplateName());

$arParams["OK_TEXT"] = trim($arParams["OK_TEXT"]);
if($arParams["OK_TEXT"] == '')
	$arParams["OK_TEXT"] = GetMessage("MF_OK_MESSAGE");

if($_SERVER["REQUEST_METHOD"] == "POST" &&(!isset($_POST["PARAMS_HASH"]) || $arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
{
	$arResult["ERROR_MESSAGE"] = array();
	$error=array();
	if(check_bitrix_sessid())
	{
		
		if(in_array("NAME",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_name"]) <= 1){
			$error["user_name"]=1;
		}else{
			$user_name = strip_tags($_POST['user_name']);
			$user_name = mysql_escape_string($user_name);
		}	
		
		if(in_array("EMAIL",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["user_email"]) <= 1){
			$error["user_email"]=1;
		}else{
			$user_email = strip_tags($_POST['user_email']);
			$user_email = mysql_escape_string($user_email);
		}
		
		if(in_array("MESSAGE",$arParams["REQUIRED_FIELDS"]) && strlen($_POST["MESSAGE"]) <= 3){
			$error["MESSAGE"]=1;
		}else{
			$MESSAGE = strip_tags($_POST['MESSAGE']);
			$MESSAGE = mysql_escape_string($MESSAGE);
		}

		if (!isset($_POST["personal-agree"])) {
			$error["personal-agree"]=1;
		}

		if(!empty($error)){
			$arResult["ERROR_MESSAGE"][]=GetMessage("ERR_MESS");
		}	
		
		if(in_array("EMAIL",$arParams["REQUIRED_FIELDS"]) && strlen($user_email) > 1 && !check_email($user_email)){
			$arResult["ERROR_MESSAGE"][] = GetMessage("MF_EMAIL_NOT_VALID");
			$error["user_email"]=1;
		}
						
		if(empty($arResult["ERROR_MESSAGE"]))
		{
			
			$arFields = Array(
				"AUTHOR" => $user_name,
				"AUTHOR_EMAIL" => $user_email,
				"TEXT" => $MESSAGE,
			);
			if($arParams['TYPE'] == 'popup_director'){
				
				CEvent::Send("DIRECTOR_FORM", SITE_ID, $arFields);
				
			}else{
				
				CEvent::Send("CONTACT_FORM", SITE_ID, $arFields);
				
			}
			
			
			
			$arResult["OK_MESSAGE"] = $arParams["OK_TEXT"];
			
			$arResult['ACTIVE']=array();
			$arResult["ERROR_MESSAGE"]=array();
			$arResult['ERROR']=array();
			
		}else{
			$arResult['ERROR']=$error;
			$arResult['ACTIVE']["MESSAGE"] = $MESSAGE;
			$arResult['ACTIVE']["user_name"] = $user_name;
			$arResult['ACTIVE']["user_email"] = $user_email;
			$arResult['ACTIVE']["personal-agree"] = $_POST["personal-agree"];

			$arResult["AUTHOR_NAME"] = $user_name;
			$arResult["AUTHOR_EMAIL"] = $user_email;
			$arResult["MESSAGE"] = $MESSAGE;
		}
	}
	else
		$arResult["ERROR_MESSAGE"][] = GetMessage("MF_SESS_EXP");
}

$this->IncludeComponentTemplate();
