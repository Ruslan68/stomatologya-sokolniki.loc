<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?$this->setFrameMode(true);?>

<div class="form-contact poiskpro-form">
	<div class="title-form">
		<?=GetMessage("MFT_GIVE_FEEDBACK")?>
	</div>
	<div class="message">
		<?if(!empty($arResult["ERROR_MESSAGE"])){?>
			<div class="bg-danger">
			<?foreach($arResult["ERROR_MESSAGE"] as $v)
				ShowError($v);
			?>		
			</div>
		<?}?>
		<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
			<div class="bg-success">
				<?=$arResult["OK_MESSAGE"]?>
			</div>
		<?}?>

	<!-- targets form contacts-->
	<?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
		<script type="text/javascript">
			$(document).ready(function() {
				console.log("from_contacts");
		        sendYandex('Form_Submitm_Contacts', 'Macroconversion_marketing');
		        sendGtag('Submitm', 'Form', 'Contacts');  
			});
		</script>
	<?}?>

	</div>
	<form action="<?=POST_FORM_ACTION_URI?>" method="POST">
		<?=bitrix_sessid_post()?>
		<label for="fot-name-contact">
			<span><?=GetMessage("MFT_NAME")?></span>
			<input type="text" id="fot-name-contact" placeholder=""<?if(isset($arResult['ERROR']["user_name"])) echo ' class="has-error"';?>  name="user_name" value="<?=$arResult['ACTIVE']["user_name"]?>"/>
		</label>
		<label for="fot-mail-contact">
			<span><?=GetMessage("MFT_EMAIL")?></span>
			 <input type="email" id="fot-mail-contact" placeholder=""<?if(isset($arResult['ERROR']["user_email"])) echo ' class="has-error"';?> name="user_email" value="<?=$arResult['ACTIVE']["user_email"]?>"/>
		</label>
		<label for="fot-mes-contact">
			<span><?=GetMessage("MFT_MESSAGE")?></span>
			<textarea id="fot-mes-contact"<?if(isset($arResult['ERROR']["MESSAGE"])) echo ' class="has-error"';?> placeholder="" name="MESSAGE"><?=$arResult['ACTIVE']["MESSAGE"]?></textarea>
		</label>
		<div class="personal-agreement">
			<input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-testim" name="personal-agree" type="checkbox" value="1"<?if($arResult['ACTIVE']["personal-agree"] == 1) echo ' checked';?>/> &nbsp;
			<label for="personal-agree-testim"><strong><?=GetMessage("MFT_AGREE1")?></strong></label>
			<span> <a href="/upload/template/pdf/152-f3.pdf" target="_blank"><?=GetMessage("MFT_AGREE4")?></a></span>
		</div>
		<label for="submit-fot">
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
			<input id="submit-fot" type="submit" class="btn-blue" value="<?=GetMessage("MFT_SUBMIT")?>">
		</label>
	</form>
</div>
<script>
	$('.form-contact.poiskpro-form form #submit-fot').click(function(){
		yaCounter32528320.reachGoal('ysp-otpr-obrst-svjaz');
		ga('send', 'pageview', '/ysp-otpr-obrst-svjaz');
		console.log('ysp-otpr-obrst-svjaz');
	});
</script>
