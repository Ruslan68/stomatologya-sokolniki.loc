<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?//global $USER;if($USER->IsAdmin()){ echo "<pre>";print_r($arResult['FILTER']);echo "</pre>";}?>
<?//global $USER;if($USER->IsAdmin()){ echo "<pre>";print_r($arResult['ITEMS'][35]);echo "</pre>";}?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper gallery-page-wrapper new-gallery-page-wrapper">
    <div class="container">
	
		<div class="title-section">
			<h1 class="text-block__title">
				<?=$arResult['NAME']?>
			</h1>
        </div>
		<div class="gallery-page-wrap">
			<div class="gallery-page-nav">
				<?if(!empty($arResult['FILTER']['SERVICES_CATALOG'])){?>
				<div class="custom_select-item custom_select-left wow fadeInLeft">
					<div class="current_option" data-filter="">
						<span><?=GetMessage("GALLERY_SERV")?></span>
					</div>
					<ul class="custom_options hidden">
						<?$i=1;?>
						<?foreach($arResult['FILTER']['SERVICES_CATALOG'] as $key=>$item){?>
						
						<li data-value="<?=$i?>"><button class="filter" data-filter=".serv-<?=$key?>"><?=$item?></button></li>
						
						<?$i++;?>
						<?}?>
						<li data-value="<?=$i?>"><button class="filter" data-filter=".serv-0"><?=GetMessage("GALLERY_ANOTHER_SERV")?></button></li>
					</ul>
				</div>
				<?}?>
				
				<?if(!empty($arResult['FILTER']['DOCTORS'])){?>
				<div class="custom_select-item custom_select-right wow fadeInRight">
					<div class="current_option" data-filter="">
						<span><?=GetMessage("GALLERY_DOCT")?></span>
					</div>
					<ul class="custom_options hidden">
						<?$i=1;?>
						<?foreach($arResult['FILTER']['DOCTORS'] as $key=>$item){?>
						
						<li data-value="<?=$i?>"><button class="filter" data-filter=".doctor-<?=$key?>"><?=$item?></button></li>
						
						<?$i++;?>
						<?}?>
						<li data-value="<?=$i?>"><button class="filter" data-filter=".doctor-0"><?=GetMessage("GALLERY_ANOTHER_DOCT")?></button></li>
					</ul>
				</div>
				<?}?>
				
			</div>
			<div class="gallery-page-container row">
				<div id="gallery-container">
				<?$i=0;?>
				<?foreach($arResult["ITEMS"] as $key=>$item){?>
					
					<?
					$class='';
					if(isset($item["PROPERTIES"]['SERVICES_CATALOG']) && !empty($item['PROPERTIES']['SERVICES_CATALOG']['CLASS'])){
						
						$class .= $item['PROPERTIES']['SERVICES_CATALOG']['CLASS'];
						
					}
					if(isset($item["PROPERTIES"]['DOCTORS']) && !empty($item['PROPERTIES']['DOCTORS']['CLASS'])){
						
						$class .= $item['PROPERTIES']['DOCTORS']['CLASS'];
						
					}
					
					$class = trim($class);
					
					?>

<?if(isset($item["DETAIL_PICTURE"]) && !empty($item["DETAIL_PICTURE"]['SRC'])){
	$width = 180;
	$height = 200;
	$file2 = CFile::ResizeImageGet($item["DETAIL_PICTURE"]["ID"], array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_EXACT, false);
} else {
	$width = 360;
	$height = 200;
}?>

<?$file = CFile::ResizeImageGet($item["PREVIEW_PICTURE"]["ID"], array('width'=>$width, 'height'=>$height), BX_RESIZE_IMAGE_EXACT, false); ?>


					
					<div  data-my-order="<?=++$i?>" class="col-xs-12 col-sm-6 col-md-4 gallery-item mix <?=$class?>">
						<a href="<?=$item['PREVIEW_PICTURE']['SRC']?>" class="fancybox" rel="our_work_<?=$key?>" <?if ($width==360){echo 'style="width:100%"';}?> >
							<img src="<?=$file["src"]?>" alt="<?=$item['PREVIEW_PICTURE']['ALT']?>">
							<span class="text-block">
								<?=$item['NAME']?>
							</span>
						</a>
						<?if(isset($item["DETAIL_PICTURE"]) && !empty($item["DETAIL_PICTURE"]['SRC'])){?>
							<a href="<?=$item['DETAIL_PICTURE']['SRC']?>" class="fancybox" rel="our_work_<?=$key?>">
								<img src="<?=$file2["src"]?>" alt="<?=$item['DETAIL_PICTURE']['ALT']?>">
								<span class="text-block">
									<?=$item['NAME']?>
								</span>
							</a>
						<?}?>
					</div>

					
					<?if($i%3==0){?>
					
					<div class="clearfix visible-md visible-lg"></div>
					
					<?}?>
					
					<?if($i%2==0){?>
					
					<div class="clearfix visible-sm"></div>
					
					<?}?>
					
				<?}?>
				</div>
			</div>	
		</div>
	</div>
</section>	

<?}?>

