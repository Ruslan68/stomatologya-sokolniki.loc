<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){

	$arResult['FILTER']['SERVICES_CATALOG'] = array();
	$arResult['FILTER']['DOCTORS'] = array();
	
	foreach($arResult["ITEMS"] as $key=>&$item){
	
		if(isset($item["PROPERTIES"]['SERVICES_CATALOG']) && !empty($item["PROPERTIES"]['SERVICES_CATALOG']['VALUE'])){
			
			$class = '';
			
			foreach($item["PROPERTIES"]['SERVICES_CATALOG']['VALUE'] as $catalog){
				
				if(!isset($arResult['FILTER']['SERVICES_CATALOG'][$catalog])){
					
					$arResult['FILTER']['SERVICES_CATALOG'][$catalog] = $item["DISPLAY_PROPERTIES"]['SERVICES_CATALOG']['LINK_SECTION_VALUE'][$catalog]['NAME'];
				
				}
				
				
				$class .= ' serv-'.$catalog;
			}

			$class .= ' serv-0';
			
			$item["PROPERTIES"]['SERVICES_CATALOG']['CLASS'] = $class;
			
		}else{
			
			$item["PROPERTIES"]['SERVICES_CATALOG']['CLASS'] = ' serv-0 ';
			
		}	
		
		if(isset($item["PROPERTIES"]['DOCTORS']) && !empty($item["PROPERTIES"]['DOCTORS']['VALUE'])){
			
			$class = '';
			
			foreach($item["PROPERTIES"]['DOCTORS']['VALUE'] as $doctor){
				
				if(!isset($arResult['FILTER']['DOCTORS'][$doctor])){
					
					$arResult['FILTER']['DOCTORS'][$doctor] = $item["DISPLAY_PROPERTIES"]['DOCTORS']['LINK_ELEMENT_VALUE'][$doctor]['NAME'];
				
				}
				
				$class .= ' doctor-'.$doctor;
				
			}

			$class .= ' doctor-0';
			
			$item["PROPERTIES"]['DOCTORS']['CLASS'] = $class;
			
		}else{
			
			$item["PROPERTIES"]['DOCTORS']['CLASS'] = ' doctor-0';
			
		}
	
	}
	
}	
?>
