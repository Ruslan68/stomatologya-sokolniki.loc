<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper about-principles new-about-principles">
	<div class="container">
		<div class="title-section">
			<div class="text-block__title">
				<?=$arResult['NAME']?>
			</div>
		</div>
	
		<div class="row">

		<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
		
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
		<div class="col-xs-12 col-sm-6 col-md-4 wow fadeInUp principles-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="media-block">
				<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
            </div>
            <div class="text-item">
				<div class="main-title">
					<?=$arItem['NAME']?>
				</div>
				<div class="main-text">
					<?=$arItem['PREVIEW_TEXT']?>
				</div>
            </div>
        </div>
		
		<?}?>
		</div>
	</div>
</section>	
<?}?>