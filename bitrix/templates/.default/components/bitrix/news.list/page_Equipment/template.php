<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

<section class="wrapper equipment-page-info">
    <div class="container">
        <div class="title-section">
			<h1 class="text-block__title">
				<?=$arResult['NAME']?>
			</h1>
        </div>
        <div class="row">
			<div class="col-xs-12 wow fadeInLeftBig">
				<div class="main-text">
					<?=$arResult['DESCRIPTION']?>
				</div>
			</div>
        </div>
    </div>
</section>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper equipment-page-wrapper">
    <div class="container">
        <div class="row">

		<?foreach($arResult["ITEMS"] as $arItem){?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>

		<?if ($arItem['PROPERTIES']['SHOW_IN_BUTTON']['VALUE'] != "Y") {?>
		<div class="col-xs-12 col-sm-6 col-md-3 wow fadeInUpBig equipment-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="equipment-container">
				<div class="equipment-media">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
				</div>
				<div class="equipment-info">
					<?=$arItem['NAME']?>
				</div>
			</div>
			<div class="popup popup-equ">
				<div class="popup-container">
					<div class="close">
						<img src="/upload/template/image/close.png" alt="" />
					</div>
					<div class="popup-centered">
						<?if(isset($arItem['PROPERTIES']['PHOTO']) &&  !empty($arItem['PROPERTIES']['PHOTO']['VALUE'])){?>
						<div class="popup-slider">
							<?foreach($arItem['PROPERTIES']['PHOTO']['VALUE'] as $file){?>
							<?$resf = CFile::ResizeImageGet($file, array('width'=>905, 'height'=>667), BX_RESIZE_IMAGE_EXACT, false); ?>
							<div class="slide">
								<div class="image-block">
									<img src="<?=$resf["src"]?>" alt="<?=$arItem['NAME']?>" />
								</div>
							</div>
							<?}?>
						</div>
						<?}?>
						<div class="popup-slider__info">
							<div class="title-block">
								<?=$arItem['NAME']?>
							</div>
							<div class="text-block">
								<?=$arItem['PREVIEW_TEXT']?>
							</div>
						</div>
					</div>
				</div>
				<div class="popup-overlay"></div>
            </div>
        </div>
		<?} else {

		$eqbName = $arItem['NAME'];
		$eqbPrevText = $arItem['PREVIEW_TEXT'];
		$eqbPictSrc = $arItem['PREVIEW_PICTURE']['SRC'];
		$eqbPictAlt = $arItem['PREVIEW_PICTURE']['ALT'];
		if(isset($arItem['PROPERTIES']['PHOTO']) && !empty($arItem['PROPERTIES']['PHOTO']['VALUE'])){
		$eqbPictSlider = $arItem['PROPERTIES']['PHOTO']['VALUE'];
		}

		}?>
		
		<?}?>

		</div>
		<?if (isset($eqbName) && (!empty($eqbName))) {?>
		<div class="row">
			<div class="col-xs-12 col-md-6 wow fadeInRight equipment-item">
				<div class="equipment-media equipment-media-button">
					<a href="#" class="btn-right-ar">
						<?=$eqbName?>
					</a>
				</div>

			<div class="popup popup-equ">
				<div class="popup-container">
					<div class="close">
						<img src="/upload/template/image/close.png" alt="" />
					</div>
					<div class="popup-centered">
						<?if(isset($eqbPictSlider) && !empty($eqbPictSlider)){?>
						<div class="popup-slider">
							<?foreach($eqbPictSlider as $file){?>
							<?$resf = CFile::ResizeImageGet($file, array('width'=>905, 'height'=>667), BX_RESIZE_IMAGE_EXACT, false); ?>
							<div class="slide">
								<div class="image-block">
									<img src="<?=$resf["src"]?>" alt="<?=$eqbName?>" />
								</div>
							</div>
							<?}?>
						</div>
						<?}?>
						<div class="popup-slider__info">
							<div class="title-block">
								<?=$eqbName?>
							</div>
							<div class="text-block">
								<?=$eqbPrevText?>
							</div>
						</div>
					</div>
				</div>
				<div class="popup-overlay"></div>
            </div>

			</div>
		</div>
		<?}?>
    </div>
</section>
<?}?>