<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<div class="<?=isset($arParams['CLASS']) ? $arParams['CLASS'] : 'team-direction';?>"> 	

	<div class="title-section">
		<div class="text-block__title">
			<?=GetMessage("DOCTORS_SECTION")?>
		</div>
    </div>
	
    <div class="team-direction-slider">

		<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
		
		<div class="slide">
            <div class="team-slider__item">
                <div class="media-item">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" target="_blank">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
					</a>
                </div>
                <div class="team-slider__info">
                    <div class="title-item">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>"  target="_blank">
							<?=$arItem['NAME']?>
						</a>
                    </div>
					<?if(isset($arItem['PROPERTIES']['PROFESSION']) &&  !empty($arItem['PROPERTIES']['PROFESSION']['VALUE'])){?>
                    <div class="text-item">
                        <?=$arItem['PROPERTIES']['PROFESSION']['VALUE']?>
                    </div>
					<?}?>
                </div>
            </div>
        </div>
		
		<?}?>
    </div>

</div>	

<?}?>