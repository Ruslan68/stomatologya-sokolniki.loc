<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){
	
	foreach($arResult["ITEMS"] as $key=> &$arItem){
		if(isset($arItem["PROPERTIES"]['DOCTOR']) && !empty($arItem["PROPERTIES"]['DOCTOR']['VALUE'])){
				

$arSelect = Array("ID", "PROPERTY_PROFESSION");
$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_DOC_ID'], "ID" => $arItem["DISPLAY_PROPERTIES"]['DOCTOR']['VALUE'] );
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement())
{
$arFields = $ob->GetFields();
$arProps = $ob->GetProperties();
}

$arItem["DISPLAY_PROPERTIES"]['DOCTOR']['PROFESSION'] = $arFields['~PROPERTY_PROFESSION_VALUE'];
				
				}
	}	
	
}


?>
