<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";

if (isset($_GET['PAGEN_1']) && $_GET['PAGEN_1'] != '') {
	$APPLICATION->SetPageProperty("description", "Отзывы клиентов о стоматологической клинике «СОЛНЕЧНЫЙ ОСТРОВ». Страница ".$_GET['PAGEN_1'].". В нашей клинике работают профессиональные врачи. Подробности можно узнать по телефонам, указанным на сайте.");
}

$iblock = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 1));
$request_uri = explode('/', $_SERVER['REQUEST_URI']);
$demis_doctor_id = null;

while ($ar_res = $iblock->GetNext()) {
	if (in_array($ar_res['CODE'], $request_uri) !== false) {
       $demis_doctor_id = $ar_res['ID'];
    }
}

?>


<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper testimonial-wrapper" itemscope itemtype="http://schema.org/WebPage">
    <div class="container">
        <div class="title-section">
           <!-- Если мы на странице доктора -->
           <?
			 if (!is_null($demis_doctor_id)) {

           ?>
              <div class="text-block__title">
                 <?=$arResult['NAME']?>
			  </div>

              <div class="add-review">
			       <a href="/patients/testimonial/">Добавить отзыв</a>
		        </div>
            <!--Если мы находимся на странице отзывов -->
            <?
			 } else { 
            ?>
              <h1 class="text-block__title">
				<?=$arResult['NAME']?>
		 	  </h1>
			<? } ?>
        </div>
    </div>
	<div class="testimonial-doctor">
        <div class="container">

		<?foreach($arResult["ITEMS"] as $arItem){?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<?
		$doctor = '';
		if(isset($arItem["PROPERTIES"]['DOCTOR']) && !empty($arItem["PROPERTIES"]['DOCTOR']['VALUE'])){
		
			$doctor = $arItem["DISPLAY_PROPERTIES"]['DOCTOR']['LINK_ELEMENT_VALUE'][$arItem["PROPERTIES"]['DOCTOR']['VALUE']];
		
		}
		
		?>

<?
	//if ($arItem["PROPERTIES"]['DOCTOR']['VALUE'] == $demis_doctor_id) {
?>


<? if (is_null($demis_doctor_id)) { ?>
		<?if ($arItem["PROPERTIES"]['REW_TYPE']['VALUE_XML_ID'] != 'type1') {?>
		<? /* <div class="testimonial-doctor__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="doctor-block">
                <div class="doctor-media">
					<?if(!empty($doctor)){?>
					<?$file = CFile::ResizeImageGet($doctor["PREVIEW_PICTURE"], array('width'=>70, 'height'=>70), BX_RESIZE_IMAGE_EXACT, false); ?>
					<img src="<?=$file["src"]?>" alt="<?=$doctor['NAME']?>">
					
					<?}else{?>
					
					<img src="/upload/template/image/doctor-testimonial-2.png" alt="">
					
					<?}?>
                </div>
                <div class="doctor-info">
					<?if(!empty($doctor)){?>
					
					<div class="title-block">
						<?=$doctor['NAME']?>
					</div>
					<div class="doctor-description">
						<?=$arItem["DISPLAY_PROPERTIES"]['DOCTOR']['PROFESSION']?>
					</div>
					
					<?}else{?>
					
					<div class="title-block">
						<?=GetMessage("MESS_TESTIM2")?>
					</div>
					
					<?}?>
                </div>
            </div>
            <div class="doctor-testimonial">
                <div class="doctor-testimonial__text">
					<?=$arItem['PREVIEW_TEXT']?>
						<?if (!empty($arItem["PROPERTIES"]['VIDEO_LINK']['VALUE'])) {?>
							<div class="link-text">
			                    <a href="<?=$arItem["PROPERTIES"]['VIDEO_LINK']['VALUE']?>" class="fancybox fancybox.iframe">Смотреть видео <i class="mdi mdi-play-circle"></i></a>
			                </div>
						<?}?>
                </div>
                <div class="doctor-testimonial__author">
					<div class="title-block"><?=$arItem['NAME']?></div>
					<div class="description-block"><?=GetMessage("MESS_TESTIM3")?></div>
                </div>
            </div>
        </div> */ ?>

		<div class="testimonial-doctor__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" itemprop="review" itemscope itemtype="http://schema.org/Review">
            <div class="doctor-block">
                <div class="doctor-media">
									
					<img src="/upload/template/image/doctor-testimonial-2.png" alt="">
									
                </div>
                <div class="doctor-info">
					
					<div class="title-block" itemprop="name author">
						<a itemprop="url" href="/patients/testimonial/"></a><?=$arItem['NAME']?>
					</div>
					
                </div>
            </div>
            <div class="doctor-testimonial">
                <div class="doctor-testimonial__text no-before-class">
					<div itemprop="reviewBody"><?=$arItem['PREVIEW_TEXT']?></div>
					<p><?=$arItem['DISPLAY_ACTIVE_FROM']?></p>
					
						<?if (!empty($arItem["PROPERTIES"]['VIDEO_LINK']['VALUE'])) {?>
							<div class="link-text">
			                    <a href="<?=$arItem["PROPERTIES"]['VIDEO_LINK']['VALUE']?>" class="fancybox fancybox.iframe">Смотреть видео <i class="mdi mdi-play-circle"></i></a>
			                </div>
						<?}?>

		                <?if (!empty($arItem["PROPERTIES"]['REW_FILE']['VALUE'])){?>
			                <div class="link-text" style="margin-top:0;">
			                  	<a href="<?=CFile::GetPath($arItem["PROPERTIES"]['REW_FILE']['VALUE'])?>" class="fancybox blue-line">
			                    посмотреть отзыв</a>
			                </div>
		                <?}?>

                </div>     

            </div>
            <?
				$datePublished = explode('.',$arItem['DISPLAY_ACTIVE_FROM']);
				$datePublished = array_reverse($datePublished);
				$datePublished = implode("-", $datePublished);
            ?>
            <meta itemprop="datePublished" content="<?=$datePublished?>" />
            <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization" style="display: none;">
				<span itemprop="name">Солнечный остров</span>
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress">метро Сокольники ул. Шумкина, дом 11А</span>
				<span itemprop="postalCode"></span>
				<span itemprop="addressLocality">Москва, Россия</span>
				</div>
				<span itemprop="telephone">+7 (495) 603-30-00</span>
				<span itemprop="email">info@stomatologya-sokolniki.ru</span>
				<span itemprop="email">reception@stomatologya-sokolniki.ru</span>
			</div>
        </div>


	
<?} else {?>
<?$file2 = CFile::ResizeImageGet($arItem["PROPERTIES"]['CLIENT_PHOTO']['VALUE'], array('width'=>160, 'height'=>160), BX_RESIZE_IMAGE_EXACT, false); ?>

		<div class="testimonial-patient__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
            <div class="testimonial-media">
              <img src="<?=$file2["src"]?>" alt="">
            </div>
            <div class="testimonial-item__info">
              <div class="testimonial-text" itemprop="reviewBody">
                <?=$arItem['PREVIEW_TEXT']?>
              </div>
              <div class="testimonial-info">
                <div class="author-wrap">
                  <div class="author-title" itemprop="name author">
                    <a itemprop="url" href="/patients/testimonial/"></a><?=$arItem['NAME']?>
                  </div>
                  <div class="author-description">
                    <?=$arItem["PROPERTIES"]['PROFESSION']['VALUE']?>
                  </div>
                </div>
                <?if (!empty($arItem["PROPERTIES"]['REW_FILE']['VALUE'])){?>
                <div class="link-page">
                  <a href="<?=CFile::GetPath($arItem["PROPERTIES"]['REW_FILE']['VALUE'])?>" class="fancybox blue-line">
                    посмотреть отзыв
                  </a>
                </div>
                <?}?>
              </div>
            </div>
            <?
				$datePublished = explode('.',$arItem['DISPLAY_ACTIVE_FROM']);
				$datePublished = array_reverse($datePublished);
				$datePublished = implode("-", $datePublished);
            ?>
            <meta itemprop="datePublished" content="<?=$datePublished?>" />
            <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization" style="display: none;">
				<span itemprop="name">Солнечный остров</span>
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress">метро Сокольники ул. Шумкина, дом 11А</span>
				<span itemprop="postalCode"></span>
				<span itemprop="addressLocality">Москва, Россия</span>
				</div>
				<span itemprop="telephone">+7 (495) 603-30-00</span>
				<span itemprop="email">info@stomatologya-sokolniki.ru</span>
				<span itemprop="email">reception@stomatologya-sokolniki.ru</span>
			</div>
          </div>
<?}?>
<? } else { ?><!--Если мы находимся на странице доктора -->

	<? if ($demis_doctor_id == $arItem['PROPERTIES']['DOCTOR']['VALUE']) { ?>
     <?if ($arItem["PROPERTIES"]['REW_TYPE']['VALUE_XML_ID'] != 'type1') {?>
		<div class="testimonial-doctor__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">

            <div class="doctor-testimonial">


                <div class="doctor-testimonial__text no-before-class">
                     <div class="doctor-block">
						<div class="doctor-media">
	
							<img src="/upload/template/image/doctor-testimonial-2.png" alt="">
	
						</div>
						<div class="doctor-info">
	
							<div class="title-block">
								<?=$arItem['NAME']?>
							</div>
	
						</div>
             		</div>


					<?=$arItem['PREVIEW_TEXT']?>
					<p><?=$arItem['DISPLAY_ACTIVE_FROM']?></p>
					
						<?if (!empty($arItem["PROPERTIES"]['VIDEO_LINK']['VALUE'])) {?>
							<div class="link-text">
			                    <a href="<?=$arItem["PROPERTIES"]['VIDEO_LINK']['VALUE']?>" class="fancybox fancybox.iframe">Смотреть видео <i class="mdi mdi-play-circle"></i></a>
			                </div>
						<?}?>

		                <?if (!empty($arItem["PROPERTIES"]['REW_FILE']['VALUE'])){?>
			                <div class="link-text" style="margin-top:0;">
			                  	<a href="<?=CFile::GetPath($arItem["PROPERTIES"]['REW_FILE']['VALUE'])?>" class="fancybox blue-line">
			                    посмотреть отзыв</a>
			                </div>
		                <?}?>

                </div>     

            </div>
        </div>


	
<?} else {?>
<?$file2 = CFile::ResizeImageGet($arItem["PROPERTIES"]['CLIENT_PHOTO']['VALUE'], array('width'=>160, 'height'=>160), BX_RESIZE_IMAGE_EXACT, false); ?>

		<div class="testimonial-patient__item" itemprop="review" itemscope itemtype="http://schema.org/Review">
            <div class="testimonial-media">
              <img src="<?=$file2["src"]?>" alt="">
            </div>
            <div class="testimonial-item__info">
              <div class="testimonial-text" itemprop="reviewBody">
                <?=$arItem['PREVIEW_TEXT']?>
              </div>
              <div class="testimonial-info">
                <div class="author-wrap">
                  <div class="author-title" itemprop="name author">
                    <a itemprop="url" href="/patients/testimonial/"></a><?=$arItem['NAME']?>
                  </div>
                  <div class="author-description">
                    <?=$arItem["PROPERTIES"]['PROFESSION']['VALUE']?>
                  </div>
                </div>
                <?if (!empty($arItem["PROPERTIES"]['REW_FILE']['VALUE'])){?>
                <div class="link-page">
                  <a href="<?=CFile::GetPath($arItem["PROPERTIES"]['REW_FILE']['VALUE'])?>" class="fancybox blue-line">
                    посмотреть отзыв
                  </a>
                </div>
                <?}?>
              </div>
            </div>
            <?
				$datePublished = explode('.',$arItem['DISPLAY_ACTIVE_FROM']);
				$datePublished = array_reverse($datePublished);
				$datePublished = implode("-", $datePublished);
            ?>
            <meta itemprop="datePublished" content="<?=$datePublished?>" />
            <div itemprop="itemReviewed" itemscope itemtype="http://schema.org/Organization" style="display: none;">
				<span itemprop="name">Солнечный остров</span>
				<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress">метро Сокольники ул. Шумкина, дом 11А</span>
				<span itemprop="postalCode"></span>
				<span itemprop="addressLocality">Москва, Россия</span>
				</div>
				<span itemprop="telephone">+7 (495) 603-30-00</span>
				<span itemprop="email">info@stomatologya-sokolniki.ru</span>
				<span itemprop="email">reception@stomatologya-sokolniki.ru</span>
			</div>
          </div>
<? } ?>

    <? } ?>

<style type="text/css">
	.nav-testimonial {
		display: none;
    }

	.review-direction .container {
		width: 100% !important;
		padding-left: 0;
    }

	.testimonial-wrapper .testimonial-doctor__item .doctor-testimonial {
		width: 100%;
    }

	.testimonial-wrapper {
		padding-bottom: 0;
    }

	.doctor-testimonial .doctor-block {
		display: block !important;
    }

	.add-review {
       text-align: center;
       font-size: 20px;
    }

	.testimonial-wrapper .testimonial-doctor__item {
		margin-bottom: 0 !important;
    }
</style>

<?}?>



		<?}?>
			<?//}?>
		</div>
	</div>

</section>





<?=$arResult["NAV_STRING"]?>
<?}?>
