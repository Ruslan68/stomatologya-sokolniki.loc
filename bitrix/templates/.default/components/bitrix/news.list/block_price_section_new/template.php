<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?//global $USER;if($USER->isAdmin()){ echo "<pre>";print_r($arResult);echo "</pre>";}?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<div class="price-direction"> 	
        <div class="title-section" style="padding-top: 60px;padding-bottom: 60px; display: none;">
            <div class="text-block__title">
				<?=$arResult["NAME"]?>
            </div>
        </div>
        <div class="price-info-item shadow" style="z-index: 2;padding-top: 50px;">
            <div class="price-item1_custom">
                <table>
					<thead>
						<tr>
							<td class="price_title"><?=GetMessage("TBL_PR_NAME")?></td>
							<td class="price_title"><?=GetMessage("TBL_PR_COST")?></td>
						</tr>
					</thead>
                    <tbody>
                    <?$count = 1;?>
						<?foreach($arResult["ITEMS"] as $item){?>

						<?php if ($count == 1): ?>
							<tr class="tr_p td_first">
								<td><p><?=$item["NAME"]?></p></td>
								<td><p><?=!empty($item["PROPERTIES"]['PRICE']['VALUE']) ? $item["PROPERTIES"]['PRICE']['VALUE'] .' руб.' : 'Бесплатно';?></p></td>
						   </tr>
						    <?$count = 0;?>	
							<?php elseif($count == 0): ?>
				          	<tr class="tr_p td_second">
								<td><p><?=$item["NAME"]?></p></td>
								<td><p><?=!empty($item["PROPERTIES"]['PRICE']['VALUE']) ? $item["PROPERTIES"]['PRICE']['VALUE'] .' руб.' : 'Бесплатно';?></p></td>
							</tr>
							 <?$count = 1;?>
							<?php endif; ?>	
						<?}?>
					</tbody>
				</table>
			</div>
		</div>
</div>

<?}?>
