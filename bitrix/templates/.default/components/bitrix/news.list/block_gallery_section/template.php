<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<div class="<?=isset($arParams['CLASS']) ? $arParams['CLASS'] : 'work-direction';?>"> 	

<div class="title-section">
    <div class="text-block__title">
        <?=GetMessage("EXAMPLES_TITLE")?>
    </div>
</div>
<div class="work-direction-slider">

	<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
	
		<div class="slide">
            <a href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="fancybox" rel="our_work">
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
            </a>
        </div>
		
		<?if(isset($arItem["DETAIL_PICTURE"]) && !empty($arItem["DETAIL_PICTURE"]['SRC'])){?>
		
			<div class="slide">
				<a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="fancybox" rel="our_work">
					<img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="<?=$arItem['DETAIL_PICTURE']['ALT']?>" />
				</a>
			</div>
		
		<?}?>
		
	<?}?>
				
</div>

</div>		
<?}?>