<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper main-gallery new-main-gallery">
    <div class="container containerForIE class-z-index-35">
        <div class="row">

			<div class="col-xs-12 col-md-6 col-md-push-3 wow fadeInLeft">
				<div class="title-section">
					<div class="text-block__title">
						<?=$arResult['NAME']?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">	
			<div class="col-xs-12 col-md-8 col-md-push-2 wow fadeInRight photo-gallery-slider-border">
				<div class="main-gallery-slider">

				<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
				<?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>750, 'height'=>500), BX_RESIZE_IMAGE_EXACT, false); ?>				
				<div class="slide">
					<img src="<?=$file["src"]?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
				</div>
		
				<?}?>
			
				</div>
			<div class="photo-gallery-slider-img"><img src="/upload/template/image/border_images.png"></div>
			</div>
		</div>
	</div>
</section>	
<?}?>