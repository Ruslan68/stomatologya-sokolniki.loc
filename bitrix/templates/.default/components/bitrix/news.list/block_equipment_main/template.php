<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>


<section class="wrapper equipment-wrapper new-equipment-wrapper">
    <div class="container">
        <div class="row">


	<div class="col-xs-12 col-md-6 col-md-push-3 wow fadeInLeft">
		<div class="title-section">
			<div class="text-block__title">
				<?=$arResult['NAME']?>
			</div>
		</div>
	</div>
		</div>
		<div class="row">	
	
    <div class="col-xs-12 col-md-12 fadeInRight">
		<div class="equipment-wrap">

		<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
		
		
		<div class="equipment-item">
            <div class="equipment-container">
				<div class="equipment-media">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
				</div>
				<div class="equipment-info">
					<?=$arItem['NAME']?>
				</div>
			</div>
			
			<div class="popup popup-equ">
				<div class="popup-container">
					<div class="close">
						<img src="/upload/template/image/close.png" alt="" />
					</div>
					<div class="popup-centered">
						<?if(isset($arItem['PROPERTIES']['PHOTO']) &&  !empty($arItem['PROPERTIES']['PHOTO']['VALUE'])){?>
						<div class="popup-slider">
							<?foreach($arItem['PROPERTIES']['PHOTO']['VALUE'] as $file){?>
							<div class="slide">
								<div class="image-block">
									<img src="<?=CFile::GetPath($file)?>" alt="<?=$arResult['NAME']?>" />
								</div>
							</div>
							<?}?>
						</div>
						<?}?>
						<div class="popup-slider__info">
							<div class="title-block">
								<?=$arItem['NAME']?>
							</div>
							<div class="text-block">
								<?=$arItem['PREVIEW_TEXT']?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="popup-overlay"></div>
				
            </div>
        </div>
		
		<?if($key%4 == 3){?>
		<div class="clearfix"></div>
		<?}?>
		
		<?}?>
		</div>
		
		<div class="equipment-link">
            <a href="<?=$arResult['LIST_PAGE_URL']?>" class="btn-right-ar">
				<?=GetMessage("EQUIPMENT_ALL")?>
			</a>
        </div>
		
    </div>
	
	
		</div>
    </div>
</section>	

<?}?>