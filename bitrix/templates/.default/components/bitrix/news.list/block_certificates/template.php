<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper certificates-wrapper new-certificates-wrapper">
    <div class="container">
        <div class="row">

			<div class="col-xs-12 col-md-6 col-md-push-3 wow fadeInLeft">
				<div class="title-section">
					<div class="text-block__title">
						<?=$arResult['NAME']?>
					</div>
				</div>
			</div>
		
		</div>
		<div class="row">	
			
			<div class="col-xs-12 col-md-8 col-md-push-2 wow fadeInRight">
				<div class="certificates-slider">

				<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
				
				<div class="certificates-item">
					<a href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="fancybox" rel="certificates">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
					</a>
				</div>
				
				<?}?>
				
				</div>
			</div>
		</div>
	</div>
</section>	
<?}?>