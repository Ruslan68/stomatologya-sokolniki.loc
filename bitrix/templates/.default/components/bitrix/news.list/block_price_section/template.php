<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?//global $USER;if($USER->isAdmin()){ echo "<pre>";print_r($arResult);echo "</pre>";}?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<style type="text/css">
		.price-item1 table{
			background-color: #fff;
			border: none;
			border-radius: 0;
			box-shadow: none;
			height: auto;
		}

		.price-item1 table tbody {
			overflow: hidden;
			max-height: none;
		}

		.price-item1 table tr {
			background-color: #fff !important;
        }

        .price-item1 table td:first-child {
        	width: 80%;
        }

        .price-item1 table td:last-child {
        	width: 20%;
        }

		.price-item1 table td {
			color: #2B33B0;
			background-color: #fff;
			line-height: 20px;
        }

        #show-more-prices {
			font-size: 16px;
			display: inline-block;
        }

		#show-more-prices i.material-icons {
			vertical-align: sub;
		}

	</style>

<div class="<?=isset($arParams['CLASS']) ? $arParams['CLASS'] : 'price-direction';?>"> 	
        <div class="title-section">
            <div class="text-block__title">
				<?=$arResult["NAME"]?>
            </div>
        </div>
        <div class="price-info-item">
            <div class="price-item1">
                <table>
					<thead>
						<tr>
							<td><?=GetMessage("TBL_PR_NAME")?></td>
							<td><?=GetMessage("TBL_PR_COST")?></td>
						</tr>
					</thead>
                    <tbody>
						<?foreach($arResult["ITEMS"] as $item){?>
							<tr>
								<td><?=$item["NAME"]?></td>
								<td><?=!empty($item["PROPERTIES"]['PRICE']['VALUE']) ? $item["PROPERTIES"]['PRICE']['VALUE'] .' руб.' : 'Бесплатно';?></td>
							</tr>	
						<?}?>
					</tbody>
				</table>
			</div>
		</div>
</div>





<!-- 
	<div class="<?/*=isset($arParams['CLASS']) ? $arParams['CLASS'] : 'price-direction';*/?>"> 	
        <div class="title-section">
            <div class="text-block__title">
				<?/*=$arResult["NAME"]*/?>
            </div>
        </div>
        <div class="price-info-item">
            <div class="price-item">
                <table>
					<thead>
						<tr>
							<td><?/*=GetMessage("TBL_PR_NAME")*/?></td>
							<td><?/*=GetMessage("TBL_PR_COST")*/?></td>
						</tr>
					</thead>
                    <tbody>
					<?/*foreach($arResult["ITEMS"] as $item){*/?>
						<tr>
							<td><?/*=$item["NAME"]*/?></td>
							<td><?/*=!empty($item["PROPERTIES"]['PRICE']['VALUE']) ? $item["PROPERTIES"]['PRICE']['VALUE'] : 'Бесплатно';*/?></td>
						</tr>	
					<?/*}*/?>
					</tbody>
				</table>
			</div>
		</div>
-->
<?}?>
