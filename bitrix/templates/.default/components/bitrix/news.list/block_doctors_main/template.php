<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>
<section class="wrapper <?if(isset($arParams['CLASS'])) echo $arParams['CLASS']; else echo 'team-wrapper';?>">
    <div class="container">
	
	
	<div class="title-section">
		<div class="text-block__title">
			<?=$arResult['NAME']?>
		</div>
    </div>
	
    <div class="team-slider wow fadeInUp">

		<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
		
		<div class="slide">
            <div class="team-slider__item">
                <div class="media-item">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
					</a>
                </div>
                <div class="team-slider__info">
                    <div class="title-item">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
							<?=$arItem['NAME']?>
						</a>
                    </div>
					<?if(isset($arItem['PROPERTIES']['PROFESSION']) &&  !empty($arItem['PROPERTIES']['PROFESSION']['VALUE'])){?>
                    <div class="text-item">
                        <?=$arItem['PROPERTIES']['PROFESSION']['VALUE']?>
                    </div>
					<?}?>
                </div>
            </div>
        </div>
		
		<?}?>
    </div>
	
	</div>
</section>	
<?}?>