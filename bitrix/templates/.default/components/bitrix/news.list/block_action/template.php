<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper main-offers class-z-index-35">
	<div class="container">
		<div class="title-vertical">
			<a href="<?=$arResult['LIST_PAGE_URL']?>">
				<?=GetMessage("ACTIONS_1")?>
				<span><?=$arResult['NAME']?></span>
			</a>
        </div>

		<div class="row">
			<div class="col-xs-12 col-md-8 wow fadeInLeft">
				<div class="main-offers-slider">

	<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
	
	<div class="slide">
        <div class="slide-text">
            <div class="slide-text__title">
                <?=$arItem['NAME']?>
            </div>
			<?if(!empty($arItem['PREVIEW_TEXT'])){?>
            <div class="slide-text__text">
                <?=$arItem['PREVIEW_TEXT']?>
            </div>
			<?}?>
            <div class="sale-block">
                <?if(isset($arItem["PROPERTIES"]['DISCOUNT']) && !empty($arItem["PROPERTIES"]['DISCOUNT']['DESCRIPTION'])){?>
				<div class="title-sale">
					<?=$arItem["PROPERTIES"]['DISCOUNT']['DESCRIPTION']?>
				</div>
				<?}?>
                <?if(isset($arItem["PROPERTIES"]['DISCOUNT']) && !empty($arItem["PROPERTIES"]['DISCOUNT']['VALUE'])){?>
				<div class="text-sale">
					<?=$arItem["PROPERTIES"]['DISCOUNT']['VALUE']?>
				</div>
				<?}?>
                <?if(isset($arItem['DATE_ACTIVE_TO']) && !empty($arItem['DATE_ACTIVE_TO'])){?>
				<?$date = new DateTime($arItem['DATE_ACTIVE_TO']);?>
				<div class="info-sale">
					<?=GetMessage("ACTIONS_2")?> <?=$date->format('d.m.Y')?>
				</div>
				<?}?>
            </div>
        </div>
    </div>

	<?}?>
				
				
				</div>
				<div class="slide-text__link">
					<a href="<?=$arResult['LIST_PAGE_URL']?>"><?=GetMessage("ACTIONS_3")?></a>
				</div>
			</div>
		</div>
	</div>		
	
</section>	

<?}?>


