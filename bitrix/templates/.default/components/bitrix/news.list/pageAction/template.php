<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult['ITEMS']);echo "</pre>";
?>

<section class="wrapper offers-wrapper">
	<div class="container">
		<div class="title-section">
			<h1 class="text-block__title">
				<?=$arResult['NAME']?>
			</h1>
		</div>
	</div>	

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

    <div class="offers-container">

	<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
	
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

		<div class="offers-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="container">
				<?if(isset($arResult["SECTIONS"]) && !empty($arResult["SECTIONS"][$arItem['IBLOCK_SECTION_ID']])){?>
				<div class="type-offers<?if($arResult["SECTIONS"][$arItem['IBLOCK_SECTION_ID']]['ID'] == 60){?> offers-service<?}?>"><?=$arResult["SECTIONS"][$arItem['IBLOCK_SECTION_ID']]['NAME']?></div>
				<?}?>
				<?if ($arItem['PROPERTIES']['CLEAN']['VALUE'] != "Y"){?>
				<div class="offers-inner">
					<div class="offers-media">
						<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
					</div>

					<div class="text-block">
						<div class="slide-text__title">
							<?=$arItem['NAME']?>
							<?if(isset($arItem["PROPERTIES"]['MORE_TITLE']) && !empty($arItem["PROPERTIES"]['MORE_TITLE']['VALUE'])){?>
							<span>
								<?=$arItem["PROPERTIES"]['MORE_TITLE']['VALUE']?>
							</span>
							<?}?>
						</div>
						<?if(!empty($arItem['PREVIEW_TEXT'])){?>
						<div class="slide-text__text">
							<?=$arItem['PREVIEW_TEXT']?>
						</div>
						<?}?>
						<div class="sale-block">
							<?if(isset($arItem["PROPERTIES"]['DISCOUNT']) && !empty($arItem["PROPERTIES"]['DISCOUNT']['DESCRIPTION'])){?>
							<div class="title-sale">
								<?=$arItem["PROPERTIES"]['DISCOUNT']['DESCRIPTION']?>
							</div>
							<?}?>
							<?if(isset($arItem["PROPERTIES"]['DISCOUNT']) && !empty($arItem["PROPERTIES"]['DISCOUNT']['VALUE'])){?>
							<div class="text-sale">
								<?=$arItem["PROPERTIES"]['DISCOUNT']['VALUE']?>
							</div>
							<?}?>
							<?if(isset($arItem['DATE_ACTIVE_TO']) && !empty($arItem['DATE_ACTIVE_TO'])){?>
							<?$date = new DateTime($arItem['DATE_ACTIVE_TO']);?>
							<div class="info-sale">
								<?=GetMessage("REC_ACTION_TO")?> <?=$date->format('d.m.Y')?>
							</div>
							<?}?>
							<?if(isset($arItem["PROPERTIES"]['PASSWORD']) && !empty($arItem["PROPERTIES"]['PASSWORD']['VALUE'])){?>
							<div class="pass-word">
								<?=$arItem["PROPERTIES"]['PASSWORD']['VALUE']?>
							</div>
							<?}?>
						</div>
					</div>

					<div class="clear"></div>
				</div>
				<?}else{?>
				<div class="offers-inner-free">
				<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
				</div>
				<?}?>
				<div class="link-offers btn-sub">
					<a href="#"><?=GetMessage("REC_SUBSCRIBE")?></a>
				</div>
				<? /*<div class="link-offers btn-app" data-id="<?=$arItem['ID']?>">
					<a href="#"><?=GetMessage("REC_ACTION")?></a>
				</div>*/ ?>
			</div>
        </div>
		
	<?}?>
				
				
	</div>

<?}?>


</section>	