<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){
	
	$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID']); // ������� �������� ��� ����� ����������

	foreach($arResult["ITEMS"] as $key=>$arItem){
		if(!isset($arResult["SECTIONS"][$arItem['IBLOCK_SECTION_ID']])){
			
			$arFilter['ID'] = $arItem['IBLOCK_SECTION_ID'];
			
			$rsSect = CIBlockSection::GetList(
				array("SORT"=>"ASC"),
				$arFilter,
				false, 
				array('ID', 'NAME')
			);
			if ($arSect = $rsSect->GetNext()){
		
				$arResult["SECTIONS"][$arItem['IBLOCK_SECTION_ID']] = $arSect;
					
			}
			
		}
	}	
	
}


?>