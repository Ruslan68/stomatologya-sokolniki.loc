<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

       

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<div class="<?=isset($arParams['CLASS']) ? $arParams['CLASS'] : 'video-direction';?>"> 	

<div class="title-section">
    <div class="text-block__title">
        <?=$arResult['NAME']?>
    </div>
</div>
<div class="video-block">
	<?foreach($arResult["ITEMS"] as $key=>$arItem){?>
    <iframe width="100%" height="400" src="<?=$arItem['PROPERTIES']['VIDEO']['VALUE']?>" frameborder="0" allowfullscreen></iframe>
    <p>Все видео являются постановочными</p>
	<?}?>
</div>

</div>

<?}?>