<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?$this->setFrameMode(true);?>
<?//global $USER;if($USER->isAdmin()){ echo "<pre>";print_r($arResult);echo "</pre>";}?>

<?if (!empty($arResult)){?>


<div class="top-nav-btn btn-open"></div>
<div class="nav-block nav-block_menu">
	<div class="container">
		<ul>
<?
$previousLevel = 0;

$arReplace = array(
	'/patients/'=>'/patients/tax/',
);

foreach($arResult as $arItem){?>
	
	<?
	if($arItem['DEPTH_LEVEL'] > 2) continue;
	?>	
	
	<?if($previousLevel > $arItem["DEPTH_LEVEL"]){?>
		
		<?=str_repeat('</ul></li>',$previousLevel - $arItem["DEPTH_LEVEL"]);?>	

	<?}?>
	<?
		if(isset($arReplace[$arItem['LINK']])) {
			$arItem['LINK'] = $arReplace[$arItem['LINK']];
		}
	?>
	<?if($arItem['IS_PARENT']){?>
		
		
		<li class="sub-nav <?=(isset($arItem['PARAMS']['CLASS'])) ? $arItem['PARAMS']['CLASS'] : '';?>">
			<a class="sub-nav-link<?if($arItem['SELECTED']) echo " active";?>" href="<?=$arItem['LINK']?>">
               <?=$arItem['TEXT']?>
            </a>
            <ul>
	
	<?}else{?>
		
		<li>
            <a href="<?=$arItem['LINK']?>"<?if($arItem['SELECTED']) echo ' class="active"';?>><?=$arItem['TEXT']?></a>
        </li>
	
	<?}?>
	
	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>


<?}?>	  

	<?if($previousLevel > 1){?>
	
		<?=str_repeat('</ul></li>',$previousLevel - 1);?>
	
	<?}?>

		</ul>
	</div>
</div>


<?}?>
              
