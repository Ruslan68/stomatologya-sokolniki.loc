<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="wrapper price-wrapper">
    <div class="container">
        <div class="title-section">
			<h1 class="text-block__title">
				<?=$arResult['IBLOCK']['NAME']?>
			</h1>
			<div class="main-text">
				<?=$arResult['IBLOCK']['DESCRIPTION']?>
			</div>
        </div>
    </div>
	<div class="price-info-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."price/price_blocks.php"
	)
);?>	
	</div>
</section>

<style type="text/css">
	.price-item1 {
		background-color: rgba(255, 255, 255, .3);
	}
	.price-item1 table,
	.price-item1 table tbody,
	.price-item1 table tr,
	.price-item1 table td {
		background: transparent !important;
	}

	.price-item1 table {
		border: none;
		border-radius: 0;
		box-shadow: none;
	}

	.price-item1 table tbody {
		max-height: none;
	}

	.price-item1 table tbody tr.type-price {
		background-color: #EFF9FB !important;
	}

	.price-item1 table tbody tr.type-price td {
		background-color: #EFF9FB !important;
		text-align: center;
		font-size: 20px;
		font-weight: bold;
	}

	.show-more-prices,
	.show-less-prices,
	 .more-prices-page {
		font-size: 16px;
    line-height: 24px;
    width: 100%;
    padding: 24px;
    display: inline-block;
	}

	.show-more-prices i.material-icons,
	.show-less-prices i.material-icons {
    vertical-align: sub;
	}

</style>

<?if(!empty($arResult['SECTIONS'])){?>

<section class="wrapper price-info">

	<?foreach($arResult['SECTIONS'] as $up_section){?>

		<div class="price-info-item">
			<div class="container">
				<div class="title-wrap">
					<?=$up_section['NAME']?>
				</div>
				<div class="price-item1">
					<table>
						<thead>
							<tr>
								<td><?=GetMessage("TBL_PR_NAME")?></td>
								<td><?=GetMessage("TBL_PR_COST")?></td>
							</tr>
						</thead>

						<tbody>
							<?if(!empty($up_section['ITEMS'])){?>

							<?foreach($up_section['ITEMS'] as $item){?>
							<tr>
								<td><?=$item['NAME']?></td>
								<td><?=(!empty($item['PROPERTY_PRICE_VALUE'])) ?  $item['PROPERTY_PRICE_VALUE'].' руб.' : 'Бесплатно';?></td>
							</tr>
							<?}?>

						<?}?>
						<?if(!empty($up_section['SECTIONS'])){?>

							<?foreach($up_section['SECTIONS'] as $section){?>
							<tr class="type-price">
								<td><?=$section['NAME']?></td>
								<td></td>
							</tr>

								<?if(!empty($section['ITEMS'])){?>

									<?foreach($section['ITEMS'] as $item){?>
									<tr>
										<td><?=$item['NAME']?></td>
										<td><?=(!empty($item['PROPERTY_PRICE_VALUE'])) ? $item['PROPERTY_PRICE_VALUE']. ' руб.' : 'Бесплатно';?></td>
									</tr>
									<?}?>

								<?}?>
							<?}?>

						<?}?>
						</tbody>

					</table>
					
					<? if ($up_section['ID'] == 235) { ?>
						<a href="/price/protezirovanie-zubov-tseny/" class="more-prices-page">Показать ещё</a>
					<? } ?>											
					
				</div>
				<!--<div class="price-item">
					<table>
						<thead>
							<tr>
								<td><?=GetMessage("TBL_PR_NAME")?></td>
								<td><?=GetMessage("TBL_PR_COST")?></td>
							</tr>
						</thead>
						<tbody>
						<?if(!empty($up_section['ITEMS'])){?>

							<?foreach($up_section['ITEMS'] as $item){?>
							<tr>
								<td><?=$item['NAME']?></td>
								<td><?=(!empty($item['PROPERTY_PRICE_VALUE'])) ?  $item['PROPERTY_PRICE_VALUE'] : 'Бесплатно';?></td>
							</tr>
							<?}?>

						<?}?>
						<?if(!empty($up_section['SECTIONS'])){?>

							<?foreach($up_section['SECTIONS'] as $section){?>
							<tr class="type-price">
								<td><?=$section['NAME']?></td>
								<td></td>
							</tr>

								<?if(!empty($section['ITEMS'])){?>

									<?foreach($section['ITEMS'] as $item){?>
									<tr>
										<td><?=$item['NAME']?></td>
										<td><?=(!empty($item['PROPERTY_PRICE_VALUE'])) ? $item['PROPERTY_PRICE_VALUE'] : 'Бесплатно';?></td>
									</tr>
									<?}?>

								<?}?>
							<?}?>

						<?}?>
						</tbody>
					</table>		
				</div>-->
			</div>	
		</div>

	<?}?>


</section>

<?}?>	  


<?//echo "<pre>";print_r($arResult);echo "</pre>";?>
