<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?
$res = CIBlock::GetList(
    Array(), 
    Array(
        'TYPE'=>$arParams["IBLOCK_TYPE"], 
		'ID' => $arParams["IBLOCK_ID"]
    ), false
);
if($ar_res = $res->Fetch())
{
    $arResult['IBLOCK'] = $ar_res;
}

if(!empty($arResult['SECTIONS'])){
	
	$arSelect = Array("ID", "NAME","PROPERTY_PRICE");
	$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ACTIVE"=>"Y");
	
	$arr = array();
	foreach($arResult['SECTIONS'] as $section){
			
		if($section['ELEMENT_CNT'] > 0){
			
			$sect = array();
			$sect['ID'] = $section['ID'];
			$sect['SORT'] = $section['SORT'];
			$sect['IBLOCK_SECTION_ID'] = $section['IBLOCK_SECTION_ID'];
			$sect['NAME'] = $section['NAME'];
			$sect['DEPTH_LEVEL'] = $section['DEPTH_LEVEL'];
			$sect['ELEMENT_CNT'] = $section['ELEMENT_CNT'];
			$sect['ITEMS'] = array();
			
			$arFilter['SECTION_ID'] = $section['ID'];
			
			$res = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement()){ 
				$arFields = $ob->GetFields();  
				
				$sect['ITEMS'][] = $arFields;
			}
			
			if(!empty($section['IBLOCK_SECTION_ID'])){
				
				$arr[$sect['IBLOCK_SECTION_ID']]['SECTIONS'][] = $sect;	
				
			}else{
				
				$arr[$sect['ID']] = $sect;
				
			}
			
		}
		
	}
	
	
	$arResult['SECTIONS'] = $arr;
	
}

?>