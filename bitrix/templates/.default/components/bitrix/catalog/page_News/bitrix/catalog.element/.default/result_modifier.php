<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?
if(!empty($arResult)){
	
	$navElement = array();
	$ElementID = $arResult["ID"];
	
	$res = CIBlockElement::GetList(
		array($arParams["ELEMENT_SORT_FIELD"]=>$arParams['ELEMENT_SORT_ORDER'],$arParams["ELEMENT_SORT_FIELD2"]=>$arParams['ELEMENT_SORT_ORDER2']),
		array(
			"IBLOCK_ID"=>$arParams["IBLOCK_ID"], 
			"ACTIVE_DATE"=>"Y", 
			"ACTIVE"=>"Y",
			"SECTION_ID" => $arResult["SECTION"]["ID"]
		),
		false, 
		array("nPageSize" => "1","nElementID" => $ElementID), 
		array('ID',"NAME","CODE","DETAIL_PAGE_URL") 
	);
		
	while($ob = $res->GetNext()){
		$navElement[] = $ob;
	}

	$NAVIGATOR=array();

	if(count($navElement)==2){
		if($navElement[0]['ID']==$ElementID){	
			$NAVIGATOR['NEXT']=$navElement[1]['DETAIL_PAGE_URL'];
		}else if($navElement[1]['ID']==$ElementID){
			$NAVIGATOR['PREV']=$navElement[0]['DETAIL_PAGE_URL'];
		}
	}
	if(count($navElement)==3){
		$NAVIGATOR['PREV']=$navElement[0]['DETAIL_PAGE_URL'];
		$NAVIGATOR['NEXT']=$navElement[2]['DETAIL_PAGE_URL'];
	}
	
	$arResult["NAVIGATOR"] = $NAVIGATOR;
	
}



?>