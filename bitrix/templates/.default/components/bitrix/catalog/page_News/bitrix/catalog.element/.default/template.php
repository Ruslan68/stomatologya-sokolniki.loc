<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?//global $USER;if($USER->isAdmin()){ echo "<pre>";print_r($arResult);echo "</pre>";}?>

 <section class="wrapper news-info">
	<div class="container">
		<div class="title-section">
			<h1 class="text-block__title">
				<?=$arResult["NAME"]?>
			</h1>
		</div>
		<div class="news-info__item">
			<div class="col-xs-12 col-md-6">
				<div class="news-media">
					<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>" />
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="news-info__wrap">
					<?=$arResult["DETAIL_TEXT"]?>
				</div>
			</div>		
		</div>
		
	</div>
	 <div class="left-img-container left-img-news">
        <div class="main-about__image">
          <!--<img src="image/main-about-img.png" alt="">-->
        </div>
    </div>
</section>
	
<section class="wrapper nav-wrapper">			
	<div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-6 col-md-offset-6">
				<ul>
			
				<?if(isset($arResult["NAVIGATOR"]["PREV"]) && !empty($arResult["NAVIGATOR"]["PREV"])){?>
				<li>
					<a class="prev" href="<?=$arResult["NAVIGATOR"]["PREV"]?>"><?=GetMessage("BTN_PREV")?></a>
				</li>
				<?}?>
				<li>
					<a class="btn-right-ar" href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=$arResult["SECTION"]["NAME"]?></a>
				</li>
				<?if(isset($arResult["NAVIGATOR"]["NEXT"]) && !empty($arResult["NAVIGATOR"]["NEXT"])){?>
				<li>
					<a class="next" href="<?=$arResult["NAVIGATOR"]["NEXT"]?>"><?=GetMessage("BTN_NEXT")?></a>
				</li>
				<?}?>
				
				</ul>
			</div>
		</div>
	</div>	
</section>
