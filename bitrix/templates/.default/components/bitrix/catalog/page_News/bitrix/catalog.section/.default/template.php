<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>




<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

<section class="wrapper news-wrapper news-wrapper2">

<div class="news-wrap">
	<div class="container">
		<div class="news-wrap__container">
            <div class="row">

	<?foreach($arResult["ITEMS"] as $key=>$item){?>
		
		
		<div class="col-xs-12 col-md-4 news-item">
            <div class="news-item-container">
                <div class="news-item__media">
                    <a href="<?=$item["DETAIL_PAGE_URL"]?>">
						<img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$item["PREVIEW_PICTURE"]["ALT"]?>">
                    </a>
                </div>
                <div class="news-item__info">
                    <div class="news-info__data">
						<div class="news-type">
							<a href="<?=$arResult["CATEGORIES"][$item["IBLOCK_SECTION_ID"]]["SECTION_PAGE_URL"]?>">
								<?=$arResult["CATEGORIES"][$item["IBLOCK_SECTION_ID"]]["NAME"]?>
							</a>
						</div>
						<?if(isset($item["DATE_ACTIVE_FROM"]) && !empty($item["DATE_ACTIVE_FROM"])){?>
						<div class="news-data">
							<?$date = explode(' ',$item["DATE_ACTIVE_FROM"]); ?>
							<?=$date[0]?>
						</div>
						<?}?>
                    </div>
                    <div class="news-info__des">
                    	<a href="<?=$item["DETAIL_PAGE_URL"]?>">
							<?=$item["NAME"]?>
						</a>
                    </div>
                </div>
            </div>
        </div>


	<?}?>
			</div>
		</div>
	</div>
</div>

<?}?>


<?=$arResult['NAV_STRING']?>


<?//global $USER;if($USER->isAdmin()){ echo "<pre>";print_r($arResult);echo "</pre>";}?>


