<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?//global $USER;if($USER->isAdmin()){echo "<pre>";print_r($arResult);echo "</pre>";}?>

<section class="wrapper service-main-page">
	<div class="left-img-container wow fadeInLeft">
        <div class="main-about__image">
          <!--<img src="image/main-about-img.png" alt="">-->
        </div>
    </div>
	<div class="container">
		<div class="title-section">
			<h1 class="text-block__title">
				<?=$arResult["IBLOCK"]["NAME"]?>
			</h1>
			<div class="main-text">
				<?=$arResult["IBLOCK"]["DESCRIPTION"]?>
			</div>
        </div>

		<?if(!empty($arResult["SECTIONS"])){?>

		<div class="row">
			
		<?foreach($arResult["SECTIONS"] as $key=>$item){?>
		
			<div class="col-xs-12 col-md-5 main-service-item wow fadeInUp">
				<div class="icon-block">
					<a href="<?=$item["SECTION_PAGE_URL"]?>">
						<img src="<?=$item["PICTURE"]['SRC']?>" alt="<?=$item["PICTURE"]['ALT']?>" />
					</a>
				</div>
				<div class="text-block">
					<div class="text-block__title">
						<a href="<?=$item["SECTION_PAGE_URL"]?>">
							<?=$item["NAME"]?>
						</a>
					</div>
					<div class="text-block__text">
						<?=$item["UF_PREVIEW_TEXT"]?>
					</div>
				</div>
			</div>
		
					
		<?}?>
		

				
				
		</div>
	
		<?}?>
		
	</div>

</section>
