<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?//global $USER;if($USER->isAdmin()){echo "<pre>";print_r($arResult);echo "</pre>";}?>


<div class="title-section">
    <h1 class="text-block__title">
        <?
		if(isset($arResult["IPROPERTY_VALUES"]['ELEMENT_PAGE_TITLE']) && !empty($arResult["IPROPERTY_VALUES"]['ELEMENT_PAGE_TITLE'])){
			echo $arResult["IPROPERTY_VALUES"]['ELEMENT_PAGE_TITLE'];
		}else{
			echo $arResult["NAME"];
		}
		?>

    </h1>
</div>
<div class="service-direction-img">
	<?/*if(isset($arResult['SECTION']['DETAIL_PICTURE']) && !empty($arResult['SECTION']['DETAIL_PICTURE'])){
		$img_src = CFile::GetPath($arResult['SECTION']['DETAIL_PICTURE']);
	}elseif (isset($arResult['DETAIL_PICTURE']) && !empty($arResult['DETAIL_PICTURE']['SRC'])) {
		$img_src = $arResult["DETAIL_PICTURE"]["SRC"];
	}*/
	$img_src = '';
	$alt = '';
	if(isset($arResult['DETAIL_PICTURE']) && !empty($arResult['DETAIL_PICTURE']['SRC'])){
		$img_src = $arResult["DETAIL_PICTURE"]["SRC"];
		$alt = $arResult["DETAIL_PICTURE"]["ALT"];
	}elseif (isset($arResult['SECTION']['DETAIL_PICTURE']) && !empty($arResult['SECTION']['DETAIL_PICTURE'])) {
		//$img_src = CFile::GetPath($arResult['SECTION']['DETAIL_PICTURE']);
	}
	?>
	<?if(!empty($img_src)){?>
	<img src="<?=$img_src?>" alt="<?=$alt?>"/>
	<?}?>
</div>
<div>
	<?=$arResult["DETAIL_TEXT"]?>
</div>

<?//выводим калькулятор
//if($arResult['ID']=='540'){
//	require($_SERVER['DOCUMENT_ROOT'].'/include/calc.php');
//}
?>
