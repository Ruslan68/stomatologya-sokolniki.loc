<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>
<div class="custom_left_menu custom_left_menu--fixed">
<div class="left-nav-custom">
	<?if(isset($arParams['h1'])){?>
	<span class="title-nav bold"><?=$arParams['h1'];?></span>
	<?}?>
    <ul class="left-nav-container">	

    	<? if (isset($arResult['LEFT_MENU']) && $arResult['LEFT_MENU'] != NULL) { ?>
    		<?php $keyItem = 0 ; ?>
    		<? foreach ($arResult['LEFT_MENU'] as $key => $value) { ?>
    			<li class="left-item sub-nav-left-item <?php if($keyItem % 2 != 0):?> left-item-color<?php endif;?>">
    				<?php $keyItem++;?>
    				<a href="<?=$value["ROOT_URL"]?>"<?//if(isset($arParams['ELEMENT_CODE']) && $arParams['ELEMENT_CODE'] == $item['CODE']) echo ' class="active"';?>><?=$key?></a>	
					<?php if (count($value['ELEMENTS'])) : ?>
						<i class="fa fa-play"></i>
					<?php endif;?>
					<ul class="sub-nav-item-content">                
                		<?foreach($value['ELEMENTS'] as $param){?>
							<li class="left-item"><a href="<?=$param['URL']?>"><?=$param['NAME']?></a></li>
                		<?}?>                	
					</ul>

    			</li>
    		<? } ?>
    	<? } ?>
    	
		<?php $keyItem = 0 ; ?>
		<?foreach($arResult["ITEMS"] as $key=>$item){?>			

			<?	if ($item["~IBLOCK_SECTION_ID"] == $item["IBLOCK_SECTION_ID"]) { ?>
				
			<li class="left-item sub-nav-left-item <?php if($keyItem % 2 != 0):?> left-item-color<?php endif;?>">
				<?php $keyItem++;?>
                <a href="<?=$item['DETAIL_PAGE_URL']?>"<?if(isset($arParams['ELEMENT_CODE']) && $arParams['ELEMENT_CODE'] == $item['CODE']) echo ' class="active"';?>><?=$item['NAME']?></a>
                <?if (isset($item['GROUPED_SERVICES']) && (!empty($item['GROUPED_SERVICES']))) {?>
                <i class="fa fa-play"></i>
				<ul class="sub-nav-item-content">
                	
                		<?foreach($item['GROUPED_SERVICES'] as $k=>$i){?>
							<li class="left-item"><a href="<?=$i['DETAIL_PAGE_URL']?>"><?=$i['NAME']?></a></li>
                		<?}?>
                	
				</ul>
                <?}?>
            </li>

        	<? } ?>
		
		<?}?>
	</ul>
</div>
<div class="sub-menu-container"></div>
</div>	

<script>

</script>
<?}?>

