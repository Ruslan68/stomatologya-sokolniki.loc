`<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?//global $USER;if($USER->isAdmin()){echo "<pre>";print_r($arResult);echo "</pre>";}?>

<div class="service-direction-img">
	<?/*if(isset($arResult['SECTION']['DETAIL_PICTURE']) && !empty($arResult['SECTION']['DETAIL_PICTURE'])){
		$img_src = CFile::GetPath($arResult['SECTION']['DETAIL_PICTURE']);
	}elseif (isset($arResult['DETAIL_PICTURE']) && !empty($arResult['DETAIL_PICTURE']['SRC'])) {
		$img_src = $arResult["DETAIL_PICTURE"]["SRC"];
	}*/
	$img_src = '';
	$alt = '';
	if(isset($arResult['DETAIL_PICTURE']) && !empty($arResult['DETAIL_PICTURE']['SRC'])){
		$img_src = $arResult["DETAIL_PICTURE"]["SRC"];
		$alt = $arResult["DETAIL_PICTURE"]["ALT"];
	}elseif (isset($arResult['SECTION']['DETAIL_PICTURE']) && !empty($arResult['SECTION']['DETAIL_PICTURE'])) {
		//$img_src = CFile::GetPath($arResult['SECTION']['DETAIL_PICTURE']);
	}
	?>
	<?if(!empty($img_src)){?>
	<img src="<?=$img_src?>" alt="<?=$alt?>"/>
	<?}?>
</div>


<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>
<div class="left-nav-menu no-fixed-position">
<div class="left-nav">
	<?if(isset($arParams['ELEMENT_CODE'])){?>
	<span class="title-nav bold"><?=$arResult['NAME']?></span>
	<?}?>
    <div class="left-nav-container">	

    	<? if (isset($arResult['LEFT_MENU']) && $arResult['LEFT_MENU'] != NULL) { ?>
    		<? foreach ($arResult['LEFT_MENU'] as $key => $value) { ?>
    			<div class="left-item sub-nav-left-item">
    				<a href="<?=$value["ROOT_URL"]?>"<?//if(isset($arParams['ELEMENT_CODE']) && $arParams['ELEMENT_CODE'] == $item['CODE']) echo ' class="active"';?>><?=$key?></a>	

					<div class="sub-nav-item-content">                
                		<?foreach($value['ELEMENTS'] as $param){?>
							<div class="left-item"><a href="<?=$param['URL']?>"><?=$param['NAME']?></a></div>
                		<?}?>                	
					</div>

    			</div>
    		<? } ?>
    	<? } ?>
    	

		<?foreach($arResult["ITEMS"] as $key=>$item){?>			

			<?	if ($item["~IBLOCK_SECTION_ID"] == $item["IBLOCK_SECTION_ID"]) { ?>

			<div class="left-item sub-nav-left-item">
                <a href="<?=$item['DETAIL_PAGE_URL']?>"<?if(isset($arParams['ELEMENT_CODE']) && $arParams['ELEMENT_CODE'] == $item['CODE']) echo ' class="active"';?>><?=$item['NAME']?></a>
                <?if (isset($item['GROUPED_SERVICES']) && (!empty($item['GROUPED_SERVICES']))) {?>
				<div class="sub-nav-item-content">
                	
                		<?foreach($item['GROUPED_SERVICES'] as $k=>$i){?>
							<div class="left-item"><a href="<?=$i['DETAIL_PAGE_URL']?>"><?=$i['NAME']?></a></div>
                		<?}?>
                	
				</div>
                <?}?>
            </div>

        	<? } ?>
		
		<?}?>
	</div>
</div>
<div class="sub-menu-container"></div>
</div>	

<script>

</script>
<?}?>

