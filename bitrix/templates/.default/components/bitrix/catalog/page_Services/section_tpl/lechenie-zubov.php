<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
?>

<div class="customSectionPage">
	<!-- begin head_slider  -->
	<div class="head_slider">
	    <div>
	        <div class="hs_inner hs_bg1">
	            <div class="hs_title">
	                <div class="hs_title_inner">
	                	<span class="hs_title_inner-p1">Лечение зубов</span>
	                	<span class="hs_title_inner-p2">Первичная консультация терапевта — <b>бесплатно!</b></span>
	                    <span>
	                        <span>+</span> карта с возможностью накопления скидок от 2% до 10% в подарок
	                    </span>
	                </div>
	            </div>
	            <div class="align_center">
	                <a id="btn_zapis" class="btn_main zapis_popup" href="#">ЗАПИСАТЬСЯ</a>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end head_slider -->


	<?$APPLICATION->IncludeComponent(
		"bitrix:breadcrumb",
		"breadcrumb",
		Array(
			"PATH" => "",
			"SITE_ID" => SITE_ID,
			"START_FROM" => "0"
		)
	);?>

	<section class="wrapper service-direction test">
		<div class="container">
	        <div class="row">
				<div class="col-xs-12 col-md-10 col-md-push-2 marginBottom200">
					<div class="title-section">
						<h1 class="text-block__title">
							<?
							$h1 = $arCurSection['NAME'];
							if(isset($arCurSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']) && !empty($arCurSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])){
								echo $arCurSection['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'];
							}else{
								echo $arCurSection['NAME'];
							}	
							?>
						</h1>
					</div>

					<?//=$arCurSection['DESCRIPTION']?>

					<div class="row">
						<div class="col-md-4 mainText">
							<p>Мы безболезненно лечим кариес, его осложнения и некариозные поражения зубов. Наша цель — избавить вас от боли, вылечить зубы и восстановить их анатомическую форму и жевательную функцию так, чтобы они не отличались от здоровых.</p> 
							<p>Самое распространенное заболевание зубов — это кариес. Если вместо лечения кариеса пить обезболивающие, зубной нерв начнет отмирать, а кариес — поражать зуб все больше. Все это может привести к осложнениям, и тогда потребуется более длительное и дорогостоящее лечение. Чем раньше вы обратитесь к врачу, тем быстрее вылечите зубы.</p>
						</div>
						<div class="col-md-8">
							<img src="/bitrix/templates/sokolniki_dev/img/beauty_girl.png" alt="">
						</div>
					</div>

					<ul class="spa_preim_list">
						<li class="spa_preim_list_li_">
							<div class="spl_ico">
								<span>
									<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico1.png"></span>
							</div>
							<div class="spl_name">Без боли</div>
							<div class="spl_info">
								Благодаря компьютерной анестезии вы ничего не почувствуете даже во время введения обезболивающего препарата
							</div>
						</li>
						<li class="spa_preim_list_li_">
							<div class="spl_ico">
								<span>
									<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico2.png"></span>
							</div>
							<div class="spl_name">Без стресса</div>
							<div class="spl_info">
								Во время лечения пациент смотрит фильм, слушает любимую музыку или аудиокнигу или спит в мягком ортопедическом кресле
							</div>
						</li>
						<li>
							<div class="spl_ico">
								<span>
									<img src="<?=SITE_TEMPLATE_PATH?>/img/Group 12.png"></span>
							</div>
							<div class="spl_name">Домашняя обстановка</div>
							<div class="spl_info">
								Наша клиника не похожа на больницу. У нас уютный интерьер, теплая атмосфера и пейзажи Мальдивского острова
							</div>
						</li>
						<li>
							<div class="spl_ico">
								<span>
									<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico4.png"></span>
							</div>
							<div class="spl_name">Приветливые врачи</div>
							<div class="spl_info">
								Лечащий врач подробно рассказывает о лечении и отвечают на вопросы. Вы понимаете, за какие услуги платите и какой результат получите
							</div>
						</li>
					</ul>

					<div class="custom_header">
						<div class="tm">
							<div class="tm_inner">
								<span>Этапы лечения</span>
							</div>
						</div>
					</div>

					<!-- starttabs -->
					<div class="imBlock withShadow">

						<div class="innerBlockTabs">
							<!-- Tab links -->
							<div class="tab">
								<button class="tablinks active tablinks1" onclick="openTabs(event, 'Survey')">Обследование</button>
								<button class="tablinks tablinks2" onclick="openTabs(event, 'Preparation')">Обезболивание</button>
								<button class="tablinks tablinks3" onclick="openTabs(event, 'Implantation')">Удаление пораженных тканей</button>
								<button class="tablinks last tablinks4" onclick="openTabs(event, 'Making')">Восстановление зуба</button>
							</div>
							<!-- Tab content -->
							<div id="Survey" class="tabcontent tabcontentFirst">
								<div class="tab_block">
									<div class="stages_text">
										<h4>Оцениваем состояние зубов и составляем схему лечения</h4>
										<ul>
											<li><span>Слушаем жалобы пациента.</span></li>
											<li><span>Проводим визуальный осмотр полости рта.</span></li>
											<li><span>Если осмотра недостаточно для постановки диагноза, назначаем дополнительную диагностику. Для этого делаем рентгенограмму.</span></li>
											<li><span>Выявляем даже скрытый кариес с помощью лучевой диагностики.</span></li>
											<li><span>Составляем схему лечения и рассказываем о каждом этапе.</span></li>
										</ul>
									</div>
								</div>
								<div class="result_text">
									<p> <strong>Результат:</strong>
										вы знаете точный диагноз и понимаете, какое лечение последует после диагностики.
									</p>
								</div>
							</div>

							<div id="Preparation" class="tabcontent">
								<div class="tab_block">
									<div class="stages_text">
										<h4>Используем безболезненную компьютерную анестезию</h4>
										<ul>
											<li><span>Вы ничего не чувствуете во время введения обезболивающего. </span></li>
											<li><span>Анестетик подается в область одного зуба, не затрагивая мягкие ткани. Язык, губы и щеки не теряют своей чувствительности. После анестезии не нарушается дикция, не болит голова, поэтому сразу после лечения можно заниматься привычными делами.</span></li>
											<li><span>Аппарат для компьютерной анестезии больше похож на что-то вроде авторучки. Поэтому пациент не испытывает такое напряжение, как при виде шприца. </span></li>
											<li><span>Компьютерная анестезия утверждена Американской Стоматологической Ассоциацией как приоритетный метод подачи анестетика.</span></li>
										</ul>
									</div>
								</div>
								<div class="result_text">
									<p> <strong>Результат:</strong>
										вы даже не заметите, как врач введет обезболивающее.
									</p>
								</div>
							</div>

							<div id="Implantation" class="tabcontent">
								<div class="tab_block">
									<div class="stages_text">
										<h4>
											Сохраняем максимальное количество здоровой ткани
										</h4>
										<ul>
											<li><span>Используем микроскоп с 25-кратным увеличением. Это позволяет избавиться от пораженных тканей, не задев здоровые ткани и нерв. </span></li>
											<li><span>Применяем лазер. Лазерное лечение исключает риск инфекционного заражения, а также вторичного кариеса. Отсутствует неприятный шум бормашины. </span></li>
											<li><span>Если обнаружен кариес в стадии пятна, проводим реминерализацию. Обогащение зубной эмали минеральными веществами помогает устранить повреждения и восстановить ее плотность.</span> </li>
										</ul>
									</div>
								</div>
								<div class="result_text">
									<p>
										<strong>Результат:</strong>
										пораженная ткань полностью удалена, рецидив исключен.
									</p>
								</div>
							</div>

							<div id="Making" class="tabcontent">
								<div class="tab_block">
									<div class="stages_text">
										<h4>
											Восстанавливаем анатомическую форму и жевательную функцию зуба при помощи пломбировочных материалов
										</h4>
										<ul>
											<li><span>Используем композиционные пломбировочные материалы световой полимеризации Enamenl+. Это единственный композиционный материал, который преломляет свет так же, как натуральная эмаль. Поэтому запломбированный зуб не выделяется в зубном ряду, независимо от освещения.</span></li>
											<li><span>Подбираем оттенок пломбировочных материалов под цвет зубов пациента.</span></li>
											<li><span>Моделируем пломбу, чтобы она повторяла естественную форму зуба.</span></li>
										</ul>
									</div>
								</div>
								<div class="result_text">
									<p>
										<strong>Результат:</strong>
										зуб выглядит и функционирует как здоровый.
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- endtabs -->

					<div class="custom_header2">
						<div class="tm">
							<div class="tm_inner">
								<span>Цены</span>
							</div>
						</div>
					</div>

					<?
					global $arrPrice;
					$arrPrice = array("PROPERTY"=>array("SERVICES_CATALOG"=>$arCurSection["ID"]));
					?>	

					<?$APPLICATION->IncludeComponent("bitrix:news.list", "block_price_section_new", Array(
						"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
							"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
							"AJAX_MODE" => "N",	// Включить режим AJAX
							"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
							"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
							"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
							"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
							"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
							"CACHE_GROUPS" => "N",	// Учитывать права доступа
							"CACHE_TIME" => "864000",	// Время кеширования (сек.)
							"CACHE_TYPE" => "A",	// Тип кеширования
							"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
							"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
							"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
							"DISPLAY_DATE" => "N",	// Выводить дату элемента
							"DISPLAY_NAME" => "Y",	// Выводить название элемента
							"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
							"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
							"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
							"FIELD_CODE" => array(	// Поля
								0 => "",
								1 => "",
							),
							"FILTER_NAME" => "arrPrice",	// Фильтр
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
							"IBLOCK_ID" => $arParams['IBLOCK_PRICE']["IBLOCK_ID"],	// Код информационного блока
							"IBLOCK_TYPE" => $arParams['IBLOCK_PRICE']["IBLOCK_TYPE"],	// Тип информационного блока (используется только для проверки)
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
							"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
							"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
							"NEWS_COUNT" => "100",	// Количество новостей на странице
							"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
							"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
							"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
							"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
							"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
							"PAGER_TITLE" => "Новости",	// Название категорий
							"PARENT_SECTION" => "",	// ID раздела
							"PARENT_SECTION_CODE" => "",	// Код раздела
							"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
							"PROPERTY_CODE" => array(	// Свойства
								0 => "PRICE",
								1 => "",
							),
							"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
							"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
							"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
							"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
							"SET_STATUS_404" => "N",	// Устанавливать статус 404
							"SET_TITLE" => "N",	// Устанавливать заголовок страницы
							"SHOW_404" => "N",	// Показ специальной страницы
							"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
							"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
							"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
							"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
							"CLASS" => 'price-direction wow fadeInUp'
						),
						false
					);?>
				</div>

				<div class="col-xs-12 col-md-2 col-md-pull-10 paddClear">
				
					<?$intSectionID = $APPLICATION->IncludeComponent(
						"bitrix:catalog.section",
						"fixed_menu",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
							"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
							"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
							"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
							"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
							"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
							"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
							"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
							"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
							"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"FILTER_NAME" => $arParams["FILTER_NAME"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SET_TITLE" => $arParams["SET_TITLE"],
							"MESSAGE_404" => $arParams["MESSAGE_404"],
							"SET_STATUS_404" => $arParams["SET_STATUS_404"],
							"SHOW_404" => $arParams["SHOW_404"],
							"FILE_404" => $arParams["FILE_404"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

							"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
							"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
							"PAGER_TITLE" => $arParams["PAGER_TITLE"],
							"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
							"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
							"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
							"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
							"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
							"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
							"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
							"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
							"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
							"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

							'LABEL_PROP' => $arParams['LABEL_PROP'],
							'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
							'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

							'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
							'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
							'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
							'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
							'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
							'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
							'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
							'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"ADD_SECTIONS_CHAIN" => "N",
							'ADD_TO_BASKET_ACTION' => $basketAction,
							'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
							'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
							'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
							'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
							'h1' => $h1
						),
						$component
						);?>	
				</div>
			</div>
		</div>

		<div class="lineInfo">
			<div class="container">
				<div class="col-xs-12 col-md-10 col-md-push-2">
					<span>Вы можете оплачивать лечение в нашей клинике в рассрочку</span>
					<a href="/patients/lechenie-v-rassrochku/">Подробнее</a>
				</div>
			</div>
		</div>
		
		<div class="container">
	        <div class="row">
				<div class="col-xs-12 col-md-10 col-md-push-2 marginBottom200">
					<!-- begin priem  -->
				    <?$APPLICATION->IncludeComponent(
				        "poiskpro:order_form",
				        "popup_order",
				        array(
				            'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
				            'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
				            "AJAX_MODE" => 'Y',
				            "TYPE" => 'order'
				        ),
				        false
				    );?>
				    <!-- end priem -->

				    <!-- begin what  -->
					<div class="what_custom">
						<div class="tm">
							<div class="tm_inner">
								<span>почему мы?</span>
							</div>
						</div>
						<ul class="what_list_custom">
							<li>
								<div class="wl_circle">
									<div>
										<div class="wl_inner">
											<span> <b>ГАРАНТИЯ КАЧЕСТВА</b>
												Мы отвечаем за качество своей работы. Генеральный директор и главной врач лично контролируют качеству
											</span>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="wl_circle">
									<div>
										<div class="wl_inner">
											<span> <b>100% СТЕРИЛЬНО</b>
												Строго соблюдаем правила дезинфекции и стерилизации. Лазерное лечение исключает риск инфекционного заражения
											</span>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="wl_circle">
									<div>
										<div class="wl_inner">
											<span>
												<b>ФОРМА И ЦВЕТ КАК У ЗДОРОВЫХ ЗУБОВ</b>
												Запломбированные зубы по цвету и форме не отличаются от здоровых
											</span>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="wl_circle">
									<div>
										<div class="wl_inner">
											<span>
												<b>БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ</b>
												Первичная консультация любого специалиста бесплатно
											</span>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="wl_circle">
									<div>
										<div class="wl_inner">
											<span>
												<b>ПРИЕМ В ЛЮБОЕ ВРЕМЯ</b>
												Мы работаем с 9 до 21, но если вы не можете прийти в это время, примем поздно вечером или рано утром
											</span>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="wl_circle">
									<div>
										<div class="wl_inner">
											<span>
												<b>БЕСПЛАТНЫЕ ОСМОТРЫ РАЗ В ПОЛГОДА</b>
												Регулярный профосмотр — залог здоровых зубов
											</span>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
					<!-- end what -->

					<!-- begin doctor  -->
					<?
					global $arrDoctors;
					$arrDoctors = array("PROPERTY"=>array("SERVICES_CATALOG"=>$arCurSection["ID"]));
					?>	
					<?$APPLICATION->IncludeComponent("bitrix:news.list", "block_doctors_custom", Array(
					    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
					    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					    "AJAX_MODE" => "N",	// Включить режим AJAX
					    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
					    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
					    "CACHE_GROUPS" => "N",	// Учитывать права доступа
					    "CACHE_TIME" => "864000",	// Время кеширования (сек.)
					    "CACHE_TYPE" => "A",	// Тип кеширования
					    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
					    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
					    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
					    "DISPLAY_DATE" => "N",	// Выводить дату элемента
					    "DISPLAY_NAME" => "Y",	// Выводить название элемента
					    "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
					    "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
					    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
					    "FIELD_CODE" => array(	// Поля
					        0 => "",
					        1 => "",
					    ),
					    "FILTER_NAME" => "arrDoctors",	// Фильтр
					    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
					    "IBLOCK_ID" => 1,	// Код информационного блока
					    "IBLOCK_TYPE" => 'main',	// Тип информационного блока (используется только для проверки)
					    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
					    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
					    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
					    "NEWS_COUNT" => "20",	// Количество новостей на странице
					    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
					    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
					    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
					    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
					    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
					    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
					    "PAGER_TITLE" => "Новости",	// Название категорий
					    "PARENT_SECTION" => "",	// ID раздела
					    "PARENT_SECTION_CODE" => "",	// Код раздела
					    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
					    "PROPERTY_CODE" => array(	// Свойства
					        0 => "PROFESSION",
					        1 => "",
					    ),
					    "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
					    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
					    "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
					    "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
					    "SET_STATUS_404" => "N",	// Устанавливать статус 404
					    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
					    "SHOW_404" => "N",	// Показ специальной страницы
					    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
					    "SORT_BY2" => "ID",	// Поле для второй сортировки новостей
					    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
					    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
					    "CLASS" => 'main-team'
					),
					    false
					);?>

					<!-- end doctor -->
					<!-- begin otz  -->
					<?$APPLICATION->IncludeComponent("bitrix:news.list", "block_reviews", Array(
					    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
					    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					    "AJAX_MODE" => "N",	// Включить режим AJAX
					    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
					    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
					    "CACHE_GROUPS" => "N",	// Учитывать права доступа
					    "CACHE_TIME" => "864000",	// Время кеширования (сек.)
					    "CACHE_TYPE" => "A",	// Тип кеширования
					    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
					    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
					    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
					    "DISPLAY_DATE" => "N",	// Выводить дату элемента
					    "DISPLAY_NAME" => "Y",	// Выводить название элемента
					    "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
					    "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
					    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
					    "FIELD_CODE" => array(	// Поля
					        0 => "",
					        1 => "",
					    ),
					    "FILTER_NAME" => "",	// Фильтр
					    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
					    "IBLOCK_ID" => 16,	// Код информационного блока
					    "IBLOCK_TYPE" => 'other',	// Тип информационного блока (используется только для проверки)
					    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
					    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
					    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
					    "NEWS_COUNT" => "10",	// Количество новостей на странице
					    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
					    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
					    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
					    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
					    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
					    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
					    "PAGER_TITLE" => "Новости",	// Название категорий
					    "PARENT_SECTION" => "",	// ID раздела
					    "PARENT_SECTION_CODE" => "",	// Код раздела
					    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
					    "PROPERTY_CODE" => array(	// Свойства
					        0 => "",
					        1 => "",
					    ),
					    "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
					    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
					    "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
					    "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
					    "SET_STATUS_404" => "N",	// Устанавливать статус 404
					    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
					    "SHOW_404" => "N",	// Показ специальной страницы
					    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
					    "SORT_BY2" => "ID",	// Поле для второй сортировки новостей
					    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
					    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
					),
					    false
					);?>
					<!-- end otz -->

					<!-- begin equipment  -->
					<div class="equipment">
						<div class="wmain">
							<div class="tm tm2">
								<div class="tm_inner">
									<span>ПЕРЕДОВОЕ ОСНАЩЕНИЕ</span>
								</div> <i>Используем последние модели стоматологического оборудования из России, США, Германии, Японии и Израиля.
									<br>
									Оно помогает быстро поставить точный диагноз и проводить безболезненные процедуры</i> 
							</div>
							<ul class="equipment_list">
								<li>
									<div class="eql_inner">
										<div class="eql_img">
											<img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img2.jpg"></div>
										<div class="eql_info block_height">
											3D-томограф Veraviewepocs — снимок за 5 секунд
											<span>
												Используется при протезировании, ортодонтическом лечении и имплантации
											</span>
										</div>
									</div>
								</li>
								<li>
									<div class="eql_inner">
										<div class="eql_img">
											<img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img3.jpg"></div>
										<div class="eql_info block_height">
											Стоматологический микроскоп Opmi Proergo
											<span>Помогает выявить патологии, невидимые невооруженным глазом</span>
										</div>
									</div>
								</li>
								<li>
									<div class="eql_inner">
										<div class="eql_img">
											<img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img6.jpg"></div>
										<div class="eql_info block_height">
											Представитель принципиально новой лазерной технологи BIOLASE MD
											<span>
												Высочайшие клинические результаты в гораздо более приятных для пациента условиях
											</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<!-- end equipment -->

					<div class="tm tm2 custom_header">
						<div class="tm_inner">
							<span>Вас также может заинтересовать</span>
						</div> 
					</div>

					<ul class="ysl_list">
						<li class="yl_1">
							<div class="yl_name_main">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/Group 4.png" alt="Гигиена и профилактика">				
								<div class="yl_name_inner">
									<span>Гигиена и профилактика</span>
								</div>
							</div>
							<div class="yl_inner_list">
								<a href="/uslugi/gigiena-i-profilaktika/">Подробнее</a>
							</div>
						</li>
						<li class="yl_2">
							<div class="yl_name_main">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/Group 6.png" alt="Лечение десен ">				
								<div class="yl_name_inner">
									<span>Лечение десен</span>
								</div>
							</div>
							<div class="yl_inner_list">
								<a href="/uslugi/lechenie-desen/">Подробнее</a>
							</div>
						</li>
						<li class="yl_3">
							<div class="yl_name_main">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/Component 3.png" alt="Эстетическая стоматология">				
								<div class="yl_name_inner">
									<span>Эстетическая стоматология</span>
								</div>
							</div>
							<div class="yl_inner_list">
								<a href="/uslugi/estetika/">Подробнее</a>
							</div>
						</li>
						<li class="yl_4">
							<div class="yl_name_main">
								<img src="<?=SITE_TEMPLATE_PATH?>/img/Group 9.png" alt="">				
								<div class="yl_name_inner">
									<span>Протезирование</span>
								</div>
							</div>
							<div class="yl_inner_list">
								<a href="/uslugi/protezirovanie/">Подробнее</a>
							</div>
						</li>
					</ul>
					
					<div class="wrapperForm">
					<!-- begin priem  -->
					    <?$APPLICATION->IncludeComponent(
					        "poiskpro:order_form",
					        "popup_order",
					        array(
					            'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
					            'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
					            "AJAX_MODE" => 'Y',
					            "TYPE" => 'order'
					        ),
					        false
					    );?>
					    <!-- end priem -->		    	
				    </div>

				    <!-- begin licens  -->
					<div class="licens_custom">
						<div class="wmain">
							<div class="tm tm5">
								<div class="tm_inner">
									<span>лицензии и сертификаты</span>
								</div>
							</div>
							<ul class="licens_list">

								<li>
									<a  data-fancybox="licens_group1" rel="group" class="fb" href="<?=SITE_TEMPLATE_PATH?>
										/img/licens_img1.jpg">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img1.jpg"></a>
								</li>
								<li>
									<a data-fancybox="licens_group1" rel="group" class="fb" href="<?=SITE_TEMPLATE_PATH?>
										/img/licens_img2.jpg">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img2.jpg"></a>
								</li>
								<li>
									<a  data-fancybox="licens_group1" rel="group" class="fb" href="<?=SITE_TEMPLATE_PATH?>
										/img/licens_img3.jpg">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img3.jpg"></a>
								</li>
								<li>
									<a  data-fancybox="licens_group1" rel="group" class="fb" href="<?=SITE_TEMPLATE_PATH?>
										/img/licens_img4.jpg">
										<img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img4.jpg"></a>
								</li>
							</ul>
						</div>
					</div>
					<!-- end licens -->

					<?$intSectionID = $APPLICATION->IncludeComponent(
						"bitrix:catalog.section",
						"left_menu",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
							"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
							"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
							"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
							"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
							"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
							"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
							"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
							"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
							"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"FILTER_NAME" => $arParams["FILTER_NAME"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SET_TITLE" => $arParams["SET_TITLE"],
							"MESSAGE_404" => $arParams["MESSAGE_404"],
							"SET_STATUS_404" => $arParams["SET_STATUS_404"],
							"SHOW_404" => $arParams["SHOW_404"],
							"FILE_404" => $arParams["FILE_404"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
							"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
							"PRICE_CODE" => $arParams["PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

							"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
							"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
							"PAGER_TITLE" => $arParams["PAGER_TITLE"],
							"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
							"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
							"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
							"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
							"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
							"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
							"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
							"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],

							"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
							"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
							"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],

							'LABEL_PROP' => $arParams['LABEL_PROP'],
							'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
							'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

							'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
							'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
							'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
							'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
							'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
							'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
							'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
							'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"ADD_SECTIONS_CHAIN" => "N",
							'ADD_TO_BASKET_ACTION' => $basketAction,
							'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
							'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
							'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
							'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
							'h1' => $h1
						),
						$component
						);?>
				</div>
			</div>
		</div>
	</section>			
</div>