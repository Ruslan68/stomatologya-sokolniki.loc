$(document).ready(function(){
	$('.doctor_slider_custom').slick({
            dots: false,
            infinite: true,
            speed: 400,
            slidesToShow: 3,
            slidesToScroll: 1,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: true,
            fade: false,
            responsive: [
                {
                    breakpoint: 1550,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 920,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 602,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 460,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                    }
                },
            ]
        });

    if ($('[data-fancybox="licens_group1"]').length) {
        $('[data-fancybox="licens_group1"]').fancybox();
    }

});

$(document).scroll(function(){
    scrollTop = $(this).scrollTop();
    windowHeight = $(window).height();
    offsetTopMenu = $('.text-block__title').offset().top;
    if (offsetTopMenu - scrollTop - $('.hl_fixed').outerHeight(true) - $('.bot-header.nav-block.nav-block_menu').outerHeight() - 23 < 0) {
        if (!$('.custom_left_menu--fixed').hasClass('fixed_menu_enabled')) {          
            $('.custom_left_menu--fixed').addClass('fixed_menu_enabled');
        }
    } else {
        $('.custom_left_menu--fixed').removeClass('fixed_menu_enabled');
    }
        
    if ($('.fixed_menu_enabled').length) {
        if (scrollTop + windowHeight >= $('.footer').offset().top - 100) {
            $('.fixed_menu_enabled').css({'top': $('.footer').offset().top - ($('.fixed_menu_enabled').outerHeight()*3) + 'px'}).addClass('abs-left-menu');
        } else {
            $('.fixed_menu_enabled').removeClass('abs-left-menu').removeAttr('style');
        }
    }          
});

function openTabs(evt, tabsStages) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabsStages).style.display = "block";
    evt.currentTarget.className += " active";
    $( "button.tablinks" ).prevAll().css( "background-color", "#0d7fc7" );
    $( "button.tablinks.active" ).nextAll().css( "background-color", "#7db0de" );
}

$(function () {
    function hideExtraPricesC(pricelist) {
        var $items = $(pricelist).find('tr');

        if ($items.length > 4) {
            $.each($items, function (index, item) {
                if (index >= 4) {
                    $(item).addClass('hidden');
                }
            });
            
            $(pricelist).closest('.price-item1_custom').append(
                '<a href="javascript:void(0);" '+
                'class="arrow-down show-more-prices-customLink">'+
                'Показать все</a>'
            );
        }
    }

    $(document).on('click.show_price_list', '.show-more-prices-customLink', function () {
        var $pricelist = $(this).parent();

        $(this).remove();
        $pricelist.find('tbody tr').removeClass('hidden');
        $pricelist.append(
            '<a href="javascript:void(0);" '+
            'class="arrow-down show-less-prices-customLink">'+
            'Свернуть список</a>'
        );
    });

    $(document).on('click.show_price_list', '.show-less-prices-customLink', function () {
        var $pricelist = $(this).parent();

        $(this).remove();
        hideExtraPricesC($pricelist.find('tbody'));

        $(document.scrollingElement).stop().animate({ scrollTop: $pricelist.offset().top - 200 }, 1000);
    });

    var $targets = $('.price-item1_custom tbody');
    $.each($targets, function (index, pricelist) {
        hideExtraPricesC(pricelist);
    });
});