<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$strReturn .= '<section class="wrapper bread-crumbs"><div class="container"><ul itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);

$arReplace = array(
	'/patients/'=>'/patients/tax/',
);

for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');
	
	if(isset($arReplace[$arResult[$index]["LINK"]])) {
		$arResult[$index]["LINK"] = $arReplace[$arResult[$index]["LINK"]];
	}
	
	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" id="bx_breadcrumb_'.$index.'">
				<a itemprop="item" href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">
					'.$title.'
					<meta itemprop="name" content="'.$title.'">
				</a>
				<meta itemprop="position" content="'.($index+1).'" />
			</li>';
	}
	else
	{
		$strReturn .= '
			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemprop="item">
				'.$title.'
				<meta itemprop="name" content="'.$title.'">
			</a><meta itemprop="position" content="'.($index+1).'" /></li>';
	}
}

$strReturn .= '</ul></div></section>';

return $strReturn;
