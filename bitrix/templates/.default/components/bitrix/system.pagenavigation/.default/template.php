<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

//echo "<pre>";print_r($arResult);echo "</pre>";
?>



<section class="wrapper nav-testimonial">
    <div class="container">
        <div class="row">
			<div class="col-xs-12">
				<ul>
					
					<?if($arResult["NavPageNomer"]==1){?>
						<li class="disabled prev-nav"><a><i class="mdi mdi-arrow-left"></i></a></li>
					<?}else{?>
						<?if($arResult["NavPageNomer"]==2){?>
							<li class="prev-nav"><a href="<?=$arResult["sUrlPath"]?>"><i class="mdi mdi-arrow-left"></i></a></li>
						<?}else{?>
							<li class="prev-nav"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageNomer"]-1;?>"><i class="mdi mdi-arrow-left"></i></a></li>
						<?}?>
					<?}?>
					
					<?for($i=1;$i<=$arResult["NavPageCount"];$i++){?>
					
					<?if($i==1){?>
						<li<?if($i==$arResult["NavPageNomer"]){?> class="active"<?}?>><a href="<?=$arResult["sUrlPath"]?>"><?=$i?></a></li>
					<?}else{?>
						<li<?if($i==$arResult["NavPageNomer"]){?> class="active"<?}?>><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$i?>"><?=$i?></a></li>
					<?}?>
					
					<?}?>
					
					
					<?if($arResult["NavPageNomer"]==$arResult["NavPageCount"]){?>
						<li class="disabled next-nav"><a><i class="mdi mdi-arrow-right"></i></a></li>
					<?}else{?>
						<li class="next-nav"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageNomer"]+1;?>" ><i class="mdi mdi-arrow-right"></i></a></li>
					<?}?>
					
				</ul>
			</div>
		</div>
	</div>
</section>	