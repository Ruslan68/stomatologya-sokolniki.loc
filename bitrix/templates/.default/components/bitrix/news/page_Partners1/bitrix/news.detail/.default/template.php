<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

<section class="wrapper partner-page">
    <div class="container">
        <div class="title-section new-title-section">
			<h1 class="text-block__title">
				<?=$arResult['NAME']?>
			</h1>
        </div>
        <div class="row">
			<div class="col-xs-12 col-md-6 wow fadeInLeft">
				<div class="partner-page-media<?=strlen($arResult['PROPERTIES']['STYLE_CLASSES']['VALUE'])>0?" ".$arResult['PROPERTIES']['STYLE_CLASSES']['VALUE']:""?>">
				<?if (!empty($arResult['PROPERTIES']['DETAIL_VIDEO']['VALUE'])){?>
				<a href="<?=$arResult['PROPERTIES']['DETAIL_VIDEO']['VALUE']?>" class="doctor_detail_video">
					<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>" onmouseover="$(this).click();">
				</a>
				<?}else{?>
					<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
				<?}?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 wow fadeInRight">
				<div class="partner-page-address">
					<ul>
						<?if(isset($arResult['PROPERTIES']['PHONE']) && !empty($arResult['PROPERTIES']['PHONE']['VALUE'])){?>
						<li>
							<i class="mdi mdi-phone"></i>
							<span>
								<?=$arResult['PROPERTIES']['PHONE']['~VALUE']?>
							</span>
						</li>
						<?}?>
						<?if(isset($arResult['PROPERTIES']['ADDRESS']) && !empty($arResult['PROPERTIES']['ADDRESS']['VALUE'])){?>
						<li>
							<i class="mdi mdi-map-marker-radius"></i>
							<span>
								<?=$arResult['PROPERTIES']['ADDRESS']['~VALUE']?>
							</span>
						</li>
						<?}?>
						<?if(isset($arResult['PROPERTIES']['TIME']) && !empty($arResult['PROPERTIES']['TIME']['VALUE'])){?>
						<li>
							<i class="mdi mdi-clock"></i>
							<span>
								<?=$arResult['PROPERTIES']['TIME']['~VALUE']?>
							</span>
						</li>
						<?}?>
					</ul>

					<?if (isset($arResult['PROPERTIES']['ADDITIONAL_PHOTO']) && (!empty($arResult['DISPLAY_PROPERTIES']['ADDITIONAL_PHOTO']['FILE_VALUE']['SRC']))){?>
					<div style="text-align:center;"><img src="<?=$arResult['DISPLAY_PROPERTIES']['ADDITIONAL_PHOTO']['FILE_VALUE']['SRC']?>"></div>
					<?}?>
				</div>
			</div>
		</div>
    </div>
</section>

<section class="wrapper partner-page-info">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 wow fadeInLeft">
				<div class="main-text">
					<?=$arResult['DETAIL_TEXT']?>
				</div>
			</div>
        </div>
    </div>
</section>
