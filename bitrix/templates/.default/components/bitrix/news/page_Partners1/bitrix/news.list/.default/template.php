<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?//echo "<pre>";print_r($arResult);echo "</pre>";?>

<section class="wrapper partners-wrapper new-partners-wrapper">

	<div class="container">
        <div class="row">

				<div class="title-section new-title-section">
					<h1 class="text-block__title">
						<?=$arResult['NAME']?>
					</h1>
				</div>

			<!--<div class="col-xs-12 col-md-6 wow fadeInLeft">

				<div class="main-text">
<?//=$arResult['DESCRIPTION']?>
				</div>
			</div>-->
			<div class="col-xs-12 col-md-12 wow fadeInRight">
				<div class="partner-slider">
				<?foreach($arResult["ITEMS"] as $arItem){?>
					<div class="slide">
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" />
						</a>
					</div>
				<?}?>
				</div>
			</div>
		</div>
	</div>	

</section>
