<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.sections.top",
	"specialist",
	Array(
		"ACTION_VARIABLE" => "action",
		"BASKET_URL" => "",
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"DISPLAY_COMPARE" => "N",
		"ELEMENT_COUNT" => "20",
		"ELEMENT_SORT_FIELD" => $arParams["SORT_BY1"],
		"ELEMENT_SORT_FIELD2" => $arParams["SORT_BY2"],
		"ELEMENT_SORT_ORDER" => $arParams["SORT_ORDER1"],
		"ELEMENT_SORT_ORDER2" =>  $arParams["SORT_ORDER2"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"LINE_ELEMENT_COUNT" => "3",
		"PRICE_CODE" => array(),
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"SECTION_COUNT" => "5",
		"SECTION_FIELDS" => array("NAME",""),
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_SORT_FIELD" => "sort",
		"SECTION_SORT_ORDER" => "asc",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array("",""),
		"SHOW_PRICE_COUNT" => "1",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N"
	),
	$component
);?>


<div class="popup popup-director">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:contact_form",
	"popup_director",
	array(
		'OK_TEXT' => GetMessage("MES_SEND"),
		'REQUIRED_FIELDS' => array('NAME', 'EMAIL', 'MESSAGE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'popup_director'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>
