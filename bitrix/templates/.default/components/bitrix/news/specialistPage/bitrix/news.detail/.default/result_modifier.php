<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if(!empty($arResult['PROPERTIES']['CERTIFICATIONS']['VALUE'])){

	
foreach($arResult['PROPERTIES']['CERTIFICATIONS']['VALUE'] as $f){
	
	$file = CFile::GetFileArray($f);
	$item = array();
	
	if($file['WIDTH'] >= $file['HEIGHT']){
		
		$item['SMALL'] = CFile::ResizeImageGet($file, array('width'=>261, 'height'=>188), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
		$item['BIG'] = CFile::ResizeImageGet($file, array('width'=>783, 'height'=>564), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
		
		
	}else{
		
		$item['SMALL'] = CFile::ResizeImageGet($file, array('width'=>140, 'height'=>187), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		$item['BIG'] = CFile::ResizeImageGet($file, array('width'=>420, 'height'=>561), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		
	}
	
	$arResult['PROPERTIES']['CERTIFICATIONS']['PICTURE'][] = $item;
}
	
	
}	


$res = CIBlockElement::GetList(
    array($arParams["SORT_BY1"]=>$arParams['SORT_ORDER1'],$arParams["SORT_BY2"]=>$arParams['SORT_ORDER2']),
    array(
        "IBLOCK_ID"=>$arResult["IBLOCK_ID"], 
        "ACTIVE_DATE"=>"Y", 
		"ACTIVE"=>"Y"
    ),
    false, 
    array("nPageSize" => "1","nElementID" => $arResult["ID"]), 
    array('ID',"NAME","CODE","DETAIL_PAGE_URL") 
);
$navElement = array();
while($ob = $res->GetNext()){
	$navElement[] = $ob;
}

$arResult['NAVIGATOR']=array();

if(count($navElement)==2){
	if($navElement[0]['ID']==$arResult['ID']){	
		$arResult['NAVIGATOR']['NEXT']=$navElement[1]['DETAIL_PAGE_URL'];
	}else if($navElement[1]['ID']==$arResult['ID']){
		$arResult['NAVIGATOR']['PREV']=$navElement[0]['DETAIL_PAGE_URL'];
	}
}
if(count($navElement)==3){
	$arResult['NAVIGATOR']['PREV']=$navElement[0]['DETAIL_PAGE_URL'];
	$arResult['NAVIGATOR']['NEXT']=$navElement[2]['DETAIL_PAGE_URL'];
	
}



?>