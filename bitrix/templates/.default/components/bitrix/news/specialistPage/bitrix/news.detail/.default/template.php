<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

/*
  CIBlockElement::GetList(
 array arOrder = Array("SORT"=>"ASC"),
 array arFilter = Array(),
 mixed arGroupBy = false,
 mixed arNavStartParams = false,
 array arSelectFields = Array()
);
*/

/*$iblock = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 1));
$request_uri = explode('/', $_SERVER['REQUEST_URI']);


while ($ar_res = $iblock->GetNext()) {
	if (in_array($ar_res['CODE'], $request_uri) !== false) {
		echo '<pre>';
			print_r($ar_res['ID']);
		echo '</pre>';
    }
}*/
?>


<section class="wrapper doctor-wrapper">
	<div class="container">
	
		<h1 class="title-wrap">
			<?=$arResult['NAME']?>
        </h1>
		<div class="row">
			<div class="col-xs-12 col-md-3 doctor-item  wow fadeInLeft">
				<div class="doctor-media">
					<?if (!empty($arResult['PROPERTIES']['DETAIL_VIDEO']['VALUE'])) {?>
					<a href="<?=$arResult['PROPERTIES']['DETAIL_VIDEO']['VALUE']?>" class="doctor_detail_video" rel="nofollow">
					<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>" onmouseover="$(this).click();">
					</a>
					<?}else {?>
					<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
					<?}?>
				</div>
				<?if($arResult['PROPERTIES']['WRITE_LETTER']['VALUE'] == "Y"){?>
				<div class="team-link write-to-dir">
					<a href="#" class="btn-director btn-right-ar">
						<?=GetMessage("WR_MES_DIR")?>
					</a>
				</div>
				<?}?>
			</div>
			<div class="col-xs-12 col-md-9 doctor-item">
				<div class="doctor-info">
					<div class="title-doctor wow fadeInUp">
						<?=$arResult['PROPERTIES']['PROFESSION']['VALUE']?>
					</div>
					<div class="doctor-item-container">
						<?if(!empty($arResult['PREVIEW_TEXT'])){?>
						<div class="doctor-info__item wow fadeInUp">
							<div class="title-block">
								<div class="title-block__title">
									<?=GetMessage("MESS_DOC_1")?>
								</div>
							</div>
							<div class="main-text">
								<?=$arResult['PREVIEW_TEXT']?>
							</div>
						</div>	
						<?}?>
						
						<?if(!empty($arResult['DETAIL_TEXT'])){?>
						<div class="doctor-info__item wow fadeInUp">
							<div class="title-block">
								<div class="title-block__title title-block__big">
									<?=GetMessage("MESS_DOC_2")?>
								</div>
							</div>
							<div class="main-text">
								<?=$arResult['DETAIL_TEXT']?>
							</div>
						</div>	
						<?}?>
						
						<?if(!empty($arResult['PROPERTIES']['MORE']['VALUE']['TEXT'])){?>
						<div class="doctor-info__item wow fadeInUp">
							<div class="title-block">
								<div class="title-block__title">
									<?=GetMessage("MESS_DOC_3")?>
								</div>
							</div>
							<div class="main-text">
								<?=$arResult['PROPERTIES']['MORE']['~VALUE']['TEXT']?>
							</div>
						</div>	
						<?}?>
						
						<?if(isset($arResult['PROPERTIES']['CERTIFICATIONS']['PICTURE']) && !empty($arResult['PROPERTIES']['CERTIFICATIONS']['PICTURE'])){?>
							<div class="doctor-info__item wow fadeInUp">
								<div class="title-block">
									<div class="title-block__title">
										<?=GetMessage("MESS_DOC_4")?>
									</div>
								</div>
								<div class="doctor-info__cert">
								<?foreach($arResult['PROPERTIES']['CERTIFICATIONS']['PICTURE'] as $file){?>
									
									<div class="doctor-cert__item">
										<a href="<?=$file['BIG']['src']?>" class="fancybox" rel="gallery">
											<img src="<?=$file['SMALL']['src']?>" alt="<?=$arResult['NAME']?>"/>
										</a>
									</div>
								<?}?>	
								</div>
							</div>
						<?}?>
					</div>
				</div>

			<div class="service-page marginBottom50">

					<?if (!empty($arResult['PROPERTIES']['DETAIL_VIDEO']['VALUE'])) {?>



<div class="video-direction"> 	

	<div class="title-section">
		<div class="text-block__title">
	Видео
		</div>
	</div>
	<div class="video-block">
		<iframe width="100%" height="400" src="<?=$arResult['PROPERTIES']['DETAIL_VIDEO']['VALUE']?>" frameborder="0" allowfullscreen></iframe>
		<?if (!empty($arResult['PROPERTIES']['DETAIL_VIDEO2']['VALUE'])) {?>
		<iframe width="100%" height="400" src="<?=$arResult['PROPERTIES']['DETAIL_VIDEO2']['VALUE']?>" frameborder="0" allowfullscreen></iframe>	
		<? } ?>	
		<?if (!empty($arResult['PROPERTIES']['DETAIL_VIDEO3']['VALUE'])) {?>
		<iframe width="100%" height="400" src="<?=$arResult['PROPERTIES']['DETAIL_VIDEO3']['VALUE']?>" frameborder="0" allowfullscreen></iframe>	
		<? } ?>	
		<p>Все видео являются постановочными</p>	
	</div>

</div>


<div class="review-direction">
    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"page_Testimonial", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "86400",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "16",
		"IBLOCK_TYPE" => "main",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "DOCTOR",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"IBLOCK_DOC_ID" => "1",
		"COMPONENT_TEMPLATE" => "page_Testimonial"
	),
	false
);?>
</div>


					<?}?>







			<?
/*global $arrVideo;
$arrVideo = array("PROPERTY"=>array("DOCTORS"=>$arResult['ID']));*/
			?>	
				<?/*$APPLICATION->IncludeComponent("bitrix:news.list", "block_video_section", Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
					"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					"AJAX_MODE" => "N",	// Включить режим AJAX
					"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
					"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
					"CACHE_GROUPS" => "N",	// Учитывать права доступа
					"CACHE_TIME" => "864000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
					"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
					"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
					"DISPLAY_DATE" => "N",	// Выводить дату элемента
					"DISPLAY_NAME" => "Y",	// Выводить название элемента
					"DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
					"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
					"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
					"FIELD_CODE" => array(	// Поля
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "arrVideo",	// Фильтр
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
					"IBLOCK_ID" => "15",	// Код информационного блока
					"IBLOCK_TYPE" => "other",	// Тип информационного блока (используется только для проверки)
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
					"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
					"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
					"NEWS_COUNT" => "1",	// Количество новостей на странице
					"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
					"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
					"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
					"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
					"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
					"PAGER_TITLE" => "Новости",	// Название категорий
					"PARENT_SECTION" => "",	// ID раздела
					"PARENT_SECTION_CODE" => "",	// Код раздела
					"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
					"PROPERTY_CODE" => array(	// Свойства
						0 => "VIDEO",
						1 => "",
					),
					"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
					"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
					"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
					"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
					"SET_STATUS_404" => "N",	// Устанавливать статус 404
					"SET_TITLE" => "N",	// Устанавливать заголовок страницы
					"SHOW_404" => "N",	// Показ специальной страницы
					"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
					"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
					"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
					"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
					"CLASS" => 'video-direction',
				),
				false
											);*/?>
			</div>

			</div>
		</div>
	
	</div>
</section>

<section class="wrapper nav-wrapper wow fadeInRight">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-9 col-md-offset-3">
				<ul>
				<?if(!empty($arResult['NAVIGATOR'])){?>
				
					<?if(isset($arResult['NAVIGATOR']['PREV'])){?>
						<li>
							<a href="<?=$arResult['NAVIGATOR']['PREV']?>" class="prev"><?=GetMessage("MESS_DOC_5")?></a>
						</li>
					<?}?>
					
						<li>
							<a href="<?=$arResult['LIST_PAGE_URL']?>" class="btn-right-ar"><?=GetMessage("MESS_DOC_6")?></a>
						</li>
					
					<?if(isset($arResult['NAVIGATOR']['NEXT'])){?>
						<li>
							
							<a href="<?=$arResult['NAVIGATOR']['NEXT']?>" class="next"><?=GetMessage("MESS_DOC_7")?></a>
						</li>
					<?}?>
				
				<?}else{?>
				
					<li>
						<a href="<?=$arResult['LIST_PAGE_URL']?>" class="btn-right-ar"><?=GetMessage("MESS_DOC_6")?></a>
					</li>
				
				<?}?>
					
				</ul>
			</div>
        </div>
    </div>
</section>



<?//echo "<pre>";print_r($arResult);echo "</pre>";?>
