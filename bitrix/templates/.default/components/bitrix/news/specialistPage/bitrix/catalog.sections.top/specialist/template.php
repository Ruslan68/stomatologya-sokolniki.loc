<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="wrapper team-page">


<div class="container">
    <div class="title-section">
        <h1 class="text-block__title">
            <?=$arResult['IBLOCK']['NAME']?>
        </h1>
        <div class="main-text">
            <?=$arResult['IBLOCK']['DESCRIPTION']?>
        </div>
    </div>
</div>

<?if(!empty($arResult['SECTIONS'])){?>

<div class="team-wrap-container">

<?foreach($arResult["SECTIONS"] as $arSection){?>

		<div class="team-wrap ">
			<div class="container">
				<div class="title-wrap">
					<?=$arSection['NAME']?>
				</div>
				<div class="row wow fadeInUp">

		<?foreach($arSection["ITEMS"] as $arElement){?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCST_ELEMENT_DELETE_CONFIRM')));
		?>
			<? 
				$aBlocksNextLine = array(
					'3013',
					'1816',
					'1818',
				);
			?>
			<?foreach($aBlocksNextLine as $blockNextLine){?>
				<?if($blockNextLine == $arElement['ID']):?>
					<div></div>
				<?endif;?>
			<?}?>
			<div class="col-xs-12 col-sm-6 col-md-3 team-item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
                <div class="team-slider__item">
					<div class="media-item">
					<?if(($arSection['ID'] == 1) || ($arSection['ID'] == 2)){?>
						<a href="<?=$arElement['DETAIL_PAGE_URL']?>">
							<img src="<?=$arElement['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arElement['PREVIEW_PICTURE']['ALT']?>">
						</a>
					<?}else{?>
						<span>
							<img src="<?=$arElement['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arElement['PREVIEW_PICTURE']['ALT']?>">						
						</span>
						<?}?>
					</div>
					<div class="team-slider__info">
						<div class="title-item">
							<?if(($arSection['ID'] == 1) || ($arSection['ID'] == 2)){?>
								<a href="<?=$arElement['DETAIL_PAGE_URL']?>">
									<?=$arElement['NAME']?>
								</a>
							<?}else{?>
								<span>
									<?=$arElement['NAME']?>
								</span>
							<?}?>
						</div>
						<div class="text-item">
							<?=$arElement['PROPERTIES']['PROFESSION']['VALUE']?>
						</div>
					</div>
					<?//if($_SERVER['REMOTE_ADDR'] == '195.98.68.56'){?>
						<?if(($arSection['ID'] == 1) || ($arSection['ID'] == 75)){?>
							<?if($arElement['ID']=='2'){?>
								<div class="team-link director-item-link">
									<a href="#" class="btn-director btn-right-ar">
										<?=GetMessage("WR_MES_DIR")?>
									</a>
								</div>
							<?}?>
						<?}?>
					<?//}?>
				</div>
            </div>

			
		<?}?>
		
				</div>
			</div>
		</div>
		
<?}?>

</div>

<?}?>


</section>


<?//echo "<pre>";print_r($arResult);echo "</pre>";?>
