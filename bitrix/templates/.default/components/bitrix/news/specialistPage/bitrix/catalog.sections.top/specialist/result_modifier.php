<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$res = CIBlock::GetList(
    Array(), 
    Array(
        'TYPE'=>$arParams["IBLOCK_TYPE"], 
		'ID' => $arParams["IBLOCK_ID"]
    ), false
);
if($ar_res = $res->Fetch())
{
    $arResult['IBLOCK'] = $ar_res;
}
?>