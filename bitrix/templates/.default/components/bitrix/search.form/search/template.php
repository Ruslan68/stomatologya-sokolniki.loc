<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style type="text/css">
    .search-block {
        width: 500px !important;
        height: auto !important;
        position: relative;
        text-align: right;
    }
    
    .search-form-wrapper {
        width: 100%;
    }

    .search-form {
        font-size: 0;
        width: 100%;
    }
    
    .search-form-input-query-field {
        height: 45px;
        border-bottom-left-radius: 20px !important;
        border-top-left-radius: 20px !important;
        padding-left: 10px !important;
        display: inline-block;
        width: 69% !important;  
        display: inline-block !important;
        font-size: 12px;
        vertical-align: top;
        border: none !important;
    }
    
    .search-form-category-filter {
        display: inline-block !important;
        background: #fff;
        height: 45px;
        font-size: 13px;
        vertical-align: top !important;
        width: 103px;
    }
    
    .search-form-category-filter span{
        vertical-align: middle;
        display: inline-block;
        cursor: pointer;
    }
    
    .search-form-category-filter-arrow{
        margin-top: 9px;
        margin-left: -10px;
    }
    
    .search-form-submit-button{
        height: 45px;
        width: 50px;
        background: #25B1EE;
        border: none;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
        color: #fff;
        cursor: pointer;
    }
    
    .search-form-call {
        height: 45px;
        width: 50px;
        background: #25B1EE;
        border: none;
        border-radius: 15px;
        color: #fff;
        cursor: pointer;
        vertical-align: top !important;
        text-align: center;
        display: none !important;
    }
    
    .search-form-call i {
        padding: 10px;
    }
    
    .search-form-categories-list-wrapper {
        text-align: left;
        width: 200px;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius:  10px;
        border-top-right-radius: 10px;
        background: #fff;
        border-top: none;
        box-shadow: 0 0 10px rgba(0,0,0,0.25);
    }

    .search-form-categories-list {
        margin: 0;
        padding: 0;
        list-style: none;
        border-top-right-radius: 10px;
    }

    .search-form-categories-list-item{
        font-family: Tahoma, Arial, sans-serif;
        padding: 5px 0;
        padding-left: 20px;
        cursor: pointer;
        color: #383D41;
        font-size: 14px;
    }

    .search-form-categories-list-item-caption{
        border-top-right-radius: 10px;
    }

    .search-form-categories-list-item:not(.search-form-categories-list-item-caption):hover {
        background-color: #25B1EE !important;
        color: #fff !important;
    }            

    .search-form-categories-list-item-caption {
        background: #E1F4F8;
    }
    
    .popular-queries-list-wrapper {
        border: 1px solid #cdc;
        border-top: none;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        border-bottom-left-radius: 20px;
        border-bottom-right-radius:  20px;
        background: #fff;
        text-align: left;
        max-height: 400px;
        overflow-y: auto;
        box-shadow: 0 0 10px rgba(0,0,0,0.25);
        margin-top: -5px;
        border: none;
    }

    .popular-queries-list{
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .popular-queries-item {
        font-family: Tahoma, Arial, sans-serif;
        padding: 5px 0;
        padding-left: 20px;
        cursor: pointer;
    }
    
    .popular-queries-item:not(:first-child):hover {
        color: #fff;        
    }
    
    .popular-queries-item a {
        color: #383D41;
    }

    .popular-queries-item:last-child {
        margin-bottom: 10px;
    }

    .popular-queries-item-caption {
        background: #E1F4F8;
    }

    .popular-queries-item:not(.popular-queries-item-caption):hover {
        background-color: #25B1EE !important;
    }

    .popular-queries-item:not(.popular-queries-item-caption):hover a{
        color: #fff !important;
    }

    .block-for-hide {
        width: 100%;
    }
    
    .border-bottom-left-radius-0 {
        border-bottom-left-radius: 0 !important;
    }

    @media (min-width: 1800px) {
        .search-block {
            width: 500px !important;   
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
        
        .block-for-hide {
            display: inline-block !important;
        }
    }

    @media (min-width: 1650px) and (max-width: 1799px) {
        .search-block {
            width: 400px !important;   
        }    

        .search-form-input-query-field {
            width: 60% !important;
        }
        
        .block-for-hide {
            display: inline-block !important;
        }
    }

    @media (min-width: 1530px) and (max-width: 1649px) {
        .search-block {
            width: 500px !important;
            position: absolute;
            left: -15%;
            top: 10px;
        }    

        .search-form-input-query-field {
            width: 35% !important;
        }
        
        .block-for-hide {
            display: inline-block !important;
        }
    }

    @media (min-width: 1355px) and (max-width: 1529px) {
        .search-block {
            width: 350px !important;
            position: absolute;
            top: 10px; 
            right: 608px;
        }    

        .search-form-input-query-field {
            width: 20% !important;
        }

        .block-for-hide {
            display: inline-block !important;
        }

        /*.main_phone {
         margin-left: 38% !important;
     }

     /*.container {
      margin-left: -37%;
     }*/

    }

    @media (max-width: 1530px) and (min-width: 1441px){
        .main_phone {
    margin-left: 27% !important;
}

}


    @media (max-width: 1430px) and (min-width: 1355px){
.main_phone {
    margin-left: 28% !important;
}
}
  @media (max-width: 1440px) and (min-width: 1400px){
.main_list {
    padding: 10px 26px !important;
}
}
 @media (max-width: 1400px) and (min-width: 1300px){
.con_menu {
    margin-left: -12%;
}

.main_list {
    padding: 10px 20px !important;
}

#horizontal-multilevel-menu{
float: left !important;
}
}




    @media (min-width: 1345px) and (max-width: 1354px) {
        .search-block {
            width: 350px !important;
            position: absolute;
            top: 10px;
            right: 605px;
        }    

        .search-form-input-query-field {
            width: 20% !important;
        }
        
        .block-for-hide {
            display: inline-block !important;
        }

    }



@media (max-width: 1300px) and (min-width: 1150px){
.con_menu {
    margin-left: -12%;
}

#horizontal-multilevel-menu {
    text-align: center !important;
}
}
 @media (max-width: 1300px) and (min-width: 1000px){
    #horizontal-multilevel-menu {
    text-align: center !important;
}

.nav-block {
    height: 111px;
}
 }

    @media (max-width: 1200px) and (min-width: 875px){
.con_menu {
    margin-left: -2%;
}
.logo_main {
    margin-left: 5%;
    width: 23%;
}
.nav-block {
    height: 111px;
}
#horizontal-multilevel-menu {
    text-align: center !important;
}

}



    @media (min-width: 1200px) and (max-width: 1344px) {
        .search-block {
            width: auto !important;
            position: absolute;
            top: 10px;
            right: 612px;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }

    @media (min-width:875px) and (max-width: 1199px) {
        .search-block {
             width: auto !important;
             position: absolute;
             top: 10px;
             /*right: 612px;*/
             right: 92%;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }
 @media (min-width:874px) and (max-width: 958px) {
    .search-block {
    right: 461px;
}
}

@media (min-width:400px) and (max-width: 874px) {
   
    .logo_main {
        width: 1%;
        margin-left: 68px;
    }
    .right-item{
        margin-left: 17%;
    }
 }
@media (max-width: 600px) {
 .adress {
    display: none !important;
}

.right-item {
    margin-left: 13%;
}

.search-block {
    right: 50px !important;
}
}
    @media (min-width: 769px) and (max-width: 874px) {
        .search-block {
            width: auto !important;
            position: absolute;
            right: 331px;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }

    @media (min-width: 350px) and (max-width: 850px) {
        .search-block {
            width: auto !important;
            position: absolute;
            right: 318px;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }

    @media (min-width: 450px) and (max-width: 874px)
    {
        .search-block {
            width: auto !important;
            position: absolute;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }
    
    @media(min-width: 486px) and (max-width: 776px) {
        .search-block {
            width: auto !important;
            position: absolute;
            right: 300px;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }
    
    @media(min-width: 450px) and (max-width: 485px) {
        .search-block {
            width: auto !important;
            position: absolute;
            right: 195px;
        }

        .block-for-hide {
            display: none !important;
        }

        .search-form-call {
            display: inline-block !important;
        }

        .search-form-input-query-field {
            width: 50% !important;
        }
    }


@media (max-width: 482px) and (min-width: 320px) {
.logo_main {
    margin-left: 16px !important;
}

.right-item {
    margin-left: 0% !important;
}
}



</style>

<script type="text/javascript">
    $(function () {
        var
            target = $('.search-form-call'),
            query_field = $('.search-form-input-query-field');
            
        /* Открытие формы поиска */
        target.off('click').on('click', function (e) {            
            $('.search-block').attr('style', 'width: 400px !important; position: absolute; text-align: right;');
            $('.search-block').addClass('toggle-element-search-form');
            $('.block-for-hide').attr('style', 'display: inline-block !important;');
            $('.block-for-hide').fadeIn();
            query_field.attr('style', 'width: 40% !important;');
            $(this).attr('style', 'display: none !important;');
            
            
            e.stopPropagation();
        });
        /* EOF Открытие формы поиска */
        
        
        /*Сокрытие формы поиска*/
        $(document).off('click.hide_search_from').on('click.hide_search_from', function () {
            var
                target_element = $('.toggle-element-search-form').find('.block-for-hide'),
                call_form_button = $('.search-form-call');
                
            
            target_element.fadeOut('fast', function () {
                call_form_button.fadeIn();
            });
            
        });
        /*EOF Сокрытие формы поиска*/
        
        $('.search-form-submit-button').on('click', function (e) {
            e.stopPropagation();
        });
        
        
    });
</script>


<div class="search-form-wrapper">
    <div class="search-form">        
        <form action="<?=$arResult["FORM_ACTION"]?>"  method="POST">
            <div class="block-for-hide">
                <input type="text" name="q"  class="search-form-input-query-field" placeholder="Поисковый запрос..." autocomplete="off">
            
                <div class="search-form-category-filter">
                    <span class="search-form-category-filter-label"style="border-left: 1px solid #E1E1E1; padding: 5px 3px; margin-top: 10px;">
                        Все разделы       
                    </span>
                    
                    <span class="search-form-category-filter-arrow arrow-down">
                        <i class="material-icons">&#xE5C5;</i>
                    </span>
                    
                </div>
            
                <button class="search-form-submit-button"><i class="material-icons">&#xE8B6</i></button>
            </div>

            <div class="search-form-call">
                <i class="material-icons">&#xE8B6</i>
            </div>
        </form>        
    </div>
    
    <div class="search-form-categories-list-wrapper" style="display: none;">
        <ul class="search-form-categories-list">
            <li data-item="0" class="search-form-categories-list-item search-form-categories-list-item-caption">Все разделы</li>
            <li data-item="1" class="search-form-categories-list-item">О нас</li>
            <li data-item="2" class="search-form-categories-list-item">Услуги</li>
            <!--<li data-item="3" class="search-form-categories-list-item">Пациентам</li>-->
            <li data-item="3" class="search-form-categories-list-item">Цены</li>
            <li data-item="4" class="search-form-categories-list-item">Акции</li>
            <li data-item="5" class="search-form-categories-list-item" style="margin-bottom: 10px;">Команда</li>
        </ul>
    </div>
    
    <div class="popular-queries-list-wrapper" style="display: none;">
        <ul class="popular-queries-list">
            <li class="popular-queries-item popular-queries-item-caption">Популярные запросы</li>
            <li class="popular-queries-item">Первый популярный запрос</li>
            <li class="popular-queries-item">Второй популярный запрос</li>
            <li class="popular-queries-item">Третий популярный запрос</li>
            <li class="popular-queries-item">Четвёртый популярный запрос</li>
        </ul>
    </div>
</div>

<!--открытие/сокрытие списка категорий-->
<script type="text/javascript">
    var
        search_block = $('.search-form-wrapper'),
        categories_list_block = search_block.find('.search-form-categories-list-wrapper'),
        target_element = search_block.find('.search-form-category-filter'),
        query_field = $('.search-form-input-query-field');

    $(document).on('click', function() {
        if (categories_list_block.css('display') !== 'none') {
            target_element.trigger('click');
        }        
    });

    /*Раскрытие/сокрытие списков*/
    target_element.off('click').on('click', function(e) {
        var
            arrow_down_str = '<i class="material-icons">&#xE5C5;</i>',
            arrow_up_str = '<i class="material-icons">&#xE5C7;</i>',
            arrow_element = $(this).find('.search-form-category-filter-arrow'),
            left = target_element.position().left,
            top = target_element.offset().top,
            description_block = $('.popular-queries-list-wrapper');

        if (arrow_element.hasClass('arrow-down')) {
            arrow_element.addClass('arrow-up').removeClass('arrow-down');
            arrow_element.empty().append(arrow_up_str);
            description_block.hide();
        } else if (arrow_element.hasClass('arrow-up')){
            arrow_element.addClass('arrow-down').removeClass('arrow-up');
            arrow_element.empty().append(arrow_down_str);
        }

        if (arrow_element.hasClass('arrow-up')) {
            categories_list_block.show();
            categories_list_block.css({
                'position': 'absolute',
                'left': left,
                'top': '40px',
                'width': '300px'
            });
        } else if (arrow_element.hasClass('arrow-down')) {
            categories_list_block.hide();
        }
        
        query_field.removeClass('border-bottom-left-radius-0');
        e.stopPropagation();
    });
    /*EOF Раскрытие/сокрытие списка категорий*/


    /*Выбор категории*/
    categories_list_block.find('.search-form-categories-list-item').on('click', function(e) {
        var
            target_field = $('.search-form-category-filter-label');

        target_field.text($(this).text());
        $(this).parent().find('.search-form-categories-list-item').removeClass('search-form-categories-list-item-caption');
        $(this).addClass('search-form-categories-list-item-caption');
        
        target_element.trigger('click');
        
        query_field.removeClass('border-bottom-left-radius-0');
        query_field.trigger('keyup');
        e.stopPropagation();
    });
    /*EOF Выбор категории*/
</script>
<!--EOF открытие/сокрытие списков-->



<script type="text/javascript">
   $(function () {
        var
            query_field = $('.search-form-input-query-field'),
            query_str = '',
            description_block = $('.popular-queries-list-wrapper'),
            description_list = description_block.find('.popular-queries-list'),
            timeout,
            index, i,
            search_block = $('.search-form-wrapper'),
            categories_list_block = search_block.find('.search-form-categories-list-wrapper'),
            category_id;
        
        description_list.on('click', 'li.popular-queries-item:not(:first-child)', function (e) {
            var
                link;
            
            if (!$(this).closest('div').hasClass('popular-queries-search')) {
                link = $(this).find('a').attr('href');
                location.href = link;
            } else {
                query_field.val($(this).text());            
                $('.search-form-submit-button').trigger('click');
            }
            
            query_field.removeClass('border-bottom-left-radius-0');
            //description_list.hide('500');
            description_list.fadeOut('500');
            
            e.stopPropagation();
        });
        
            
        $(document).on('click', function () {
            description_block.hide();
            query_field.removeClass('border-bottom-left-radius-0');            
        });

        
        query_field.on('click', function(e) {
            var
                str,
                i, width, tmp_left;

            if (categories_list_block.css('display') != 'none') {
                search_block.find('.search-form-category-filter').trigger('click');
            }
            
            if ($.trim($(this).val()) != '') {
                $(this).trigger('keyup');
            } else {
                description_list.empty();                
                description_block.addClass('popular-queries-search');
                
                $.post('/search/ajax_search_popular_query.php', {}, function(response) {
                    if (response.length > 0) {
                        str = '<li class="popular-queries-item popular-queries-item-caption"><strong>Популяпные запросы:</strong></li>';
                        
                        for (i in response) {
                            str += '<li class="popular-queries-item">' + response[i]["PHRASE"] + '</li>';
                        }
                        
                        query_field.addClass('border-bottom-left-radius-0');
                        
                        width = parseInt(query_field.width()) + parseInt($('.search-form-category-filter').width()) + 10;                        
                        tmp_left = query_field.position().left;
                        
                        
                        description_block.css(
                            {
                                'position': 'absolute',
                                'width': width,
                                'left': tmp_left
                            }
                        );
                        
                        description_list.append(str);
                        description_list.parent().show();
                    }
                    
                }, 'json');
            }
            
            e.stopPropagation();
        });

        
        query_field.off('keyup').on('keyup', function () {
            var
                self = $(this),
                tmp_left;
                
            query_str = $(this).val();
            category_id = categories_list_block.find('.search-form-categories-list-item-caption').data('item');
            clearTimeout(timeout);
            description_list.empty();
            description_list.parent().hide();
            
            
            description_block.removeClass('popular-queries-search');
            
            self.removeClass('border-bottom-left-radius-0');
            
            timeout = setTimeout(function () {
                $.post('/search/ajax_search.php', {'query_string': query_str, 'category_number': category_id}, function(response){
                    var
                        first_output = [],
                        str = '';
                    
                    if (response != null) {
                        index = 10;

                        for (i in response) {
                            if (index > 0) {
                                if (response[i]['OUTPUT'] == 'TITLE') {
                                    first_output.push({
                                        'phrase': response[i]['TITLE'],
                                        'url': response[i]['URL'],
                                        'razdel': ''
                                    });

                                    index--;                   
                                }
                            }
                        }

                        if (index > 0) {
                            for(i in response) {
                                if (index > 0) {
                                    if (response[i]['OUTPUT'] != 'TITLE') {
                                        first_output.push({
                                            'phrase': response[i]['SECOND_OUTPUT'],
                                            'url': response[i]['URL'],
                                            'razdel': response[i]['TITLE']
                                        });

                                        index--;
                                    }
                                }
                            }

                            index = 0;
                        }
                    }

                    if (first_output.length > 0) {
                        description_list.empty();

                        str = '<li class="popular-queries-item popular-queries-item-caption"><strong>Найдено :</strong></li>';

                        for (i in first_output) {
                            if (first_output[i]['phrase'] && first_output[i]['razdel'] === '') {
                                str += '<li class="popular-queries-item"><a href="'+first_output[i]['url']+'">';
                                str += first_output[i]['phrase']+'</a></li>';
                            } else if (first_output[i]['razdel'] !== '' || first_output[i]['razdel'] !== false){
                                str += '<li class="popular-queries-item"><span>раздел - <strong>' +first_output[i]['razdel']+ '</strong></span> <br />'+
                                    '<a href="'+first_output[i]['url']+'">' + first_output[i]['phrase'] + '</a>' +
                                '</li>';
                            }
                            
                        }

                        query_field.addClass('border-bottom-left-radius-0');
                        
                        width = parseInt(self.width()) + parseInt($('.search-form-category-filter').width()) + 10;                        
                        tmp_left = self.position().left;
                        
                        
                        description_block.css(
                            {
                                'position': 'absolute',
                                'width': width,
                                'left': tmp_left
                            }
                        );
                        
                        description_list.append(str);
                        description_list.parent().show();
                        
                        self.addClass('border-bottom-left-radius-0');
                    } else {
                        description_list.empty();
                        description_list.parent().hide();
                    }

                }, 'json').complete(function () {
                    clearTimeout(timeout);
                });    
            }, 300);
            
        });
    });      

</script>
<!--
<div class="ol-consultation" itemscope itemtype="http://schema.org/WebSite">
    <div class="top-search">
        <form action="<?=$arResult["FORM_ACTION"]?>" method="post" itemprop="potentialAction" itemscope itemtype="http://schema.org/SearchAction">
            <div class="cons-link">
                <meta itemprop="target" content="<?=$arResult["FORM_ACTION"]?>"/>
                <input type="text" itemprop="query-input" class="serch-form" placeholder="Поиск" name="q"/>
            </div>
            <button type="submit" name="s">
                <i class="mdi mdi-magnify"></i>
            </button>
        </form>
    </div>
</div>
-->
