<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$dir = $APPLICATION->GetCurDir();
$form_property = $APPLICATION->GetProperty("form");
?>

			
<?if(!$form_property){?>


<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'order'
	),
false
);?>

			
<?}?>
			</div>
			<!-- / #page-body-->
	
	
<footer id="footer" class="wrapper footer">
    <div class="scroll-to-top">
        <i class="mdi mdi-arrow-up"></i>
    </div>
    <section class="footer-wrapper">
        <div class="col-xs-12 col-md-9 col-md-push-3 footer__item">
            <div class="map" id = "map-footer">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/footer_map.php"
	)
);?>
            </div>
        </div>
        <div class="col-xs-12 col-md-3 col-md-pull-9 footer__item">
            <div class="footer-info">
                <div class="logo-fot">
                    <a href="<?=SITE_DIR?>">
                        <img src="/upload/template/image/logo-fot.png" alt="">
                    </a>
                </div>
                <div class="social-fot">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/social_icon.php"
	)
);?>
                </div>
                <div class="tel-fot">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/footer_phone.php"
	)
);?>
                </div>
                <div class="adr-fot">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/footer_address.php"
	)
);?>
                </div>
                <div class="time-fot">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/footer_time.php"
	)
);?>
                </div>
            </div>
        </div>
    </section>
    <div class="clear"></div>
    <div class="copy-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/copyright.php"
	)
);?>
	<div>
	<!--LiveInternet counter<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.12;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'style='visibility: hidden';><\/a>")
//</script>/LiveInternet--> 

  <!-- Yandex.Metrika informer -->
<!--<a href="https://metrika.yandex.ru/stat/?id=32528320&amp;from=informer" target="_blank" rel="nofollow">
	<img src="//informer.yandex.ru/informer/32528320/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:8px;height:31px;border:0;visibility:  hidden;" alt="яндекс.метрика" title="яндекс.метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:32528320,lang:'ru'});return false}catch(e){}"/>
</a>-->
<!-- /Yandex.Metrika informer -->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '255093545052833');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=255093545052833&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script>
$('body').on('submit', 'form', 
    function(){
        try{
            if(typeof fbq=='function'){
				var $form = $(this);
				var isSearch = ($form.attr('action') === '/search/') || ($form.attr('action') === '' && $form.attr('method') === 'get');
				if(isSearch){
					fbq('track', 'Search');
					console.log('Tracked Search event');
				}
				else{
					fbq('track', 'Lead');
					console.log('Tracked Lead event');
				}
            }
			else{
				console.warn('fbq is not a function');
			}
        }
        catch(e){
            console.error(e);
        }
    }
);
</script>



	</div>
    </div>
</footer>

<div class="popup popup-call">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"popup_call",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_ORDER_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'popup_call'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>



<div class="popup popup-appointment">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"popup_order",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'popup_order'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>


<div class="popup popup-sub">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:subscribe.form",
	"",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_SUBSCRIBE_OK'),
		'REQUIRED_FIELDS' => array('EMAIL'),
		'IBLOCK_ID' => 35,
		"AJAX_MODE" => 'Y',
		'SOLT' => 'email',
		'PAGE' => '/subscribe/'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>

<?$APPLICATION->IncludeComponent(
	"poiskpro:one_click",
	"",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_ORDER_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y'
	),
false
);?>


<?/*<div class="popup popup-director">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
            <div class="title-form">
                Написать письмо директору
            </div>
            <form action="#">
                <label for="fot-name">
                    <span>Ваше имя</span>
                    <input type="text" id="fot-name" placeholder="Владимир Иванович">
                </label>
                <label for="fot-mail">
                    <span>Ваш E-mail</span>
                    <input type="email" id="fot-mail" placeholder=clinic@me.com>
                </label>
                <label for="fot-mes">
                    <span>Сообщение</span>
                    <textarea id="fot-mes"></textarea>
                </label>
                <label class="submit-pup" for="submit-der">
                    <input id="submit-der" type="submit" class="btn-blue" value="отправить">
                </label>
            </form>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>	*/?>	

		</div>
		<!-- / #page-wrapper-->
		
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/script.php"
	)
);?>	

<?$APPLICATION->IncludeComponent("poiskpro:one_click","",Array());?>
	
	<!-- RedHelper -->
<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" 
src="https://web.redhelper.ru/service/main.js?c=stomatologyasokolniki">
</script> 
<!--/Redhelper -->
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/schema_contacts.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/seo.php"
	)
);?>	
	</body>
</html>
