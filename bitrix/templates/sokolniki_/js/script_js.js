// placeholder
$(document).ready(function(){
	//$("input, textarea").on("focus", function(){
	$("body").on("focus","input, textarea",function(){
		if($(this).attr("data") != ''){
			if($(this).val() == $(this).attr("data"))
				$(this).val("");
		}
	});
	//$("input, textarea").on("blur", function(){
	$("body").on("blur","input, textarea",function(){
		if($(this).attr("data") != ''){
			if($(this).val() == "")
				$(this).val($(this).attr("data"));
		}
	});
});

// h100
$(function(){
	resizeHeader();
});
	function resizeHeader(){
	var w=window,
	d=document,
	e=d.documentElement,
	g=d.getElementsByTagName('body')[0],
	x=w.innerWidth||e.clientWidth||g.clientWidth,
	y=w.innerHeight||e.clientHeight||g.clientHeight;
	wihdowH = y;
	$(".h100").css({'height':(wihdowH-0) + 'px'});
}
$(window).resize(function(){
	resizeHeader();
});

// fb
$(document).ready(function() {
	$(".fb").fancybox({
		padding: 40,
		scrolling: 'visible',
		helpers : {
             media: {
                 youtube : {
                     params : {
                         autoplay : 0
                     }
                 }
             }
         }
	});
});

// fixed_nav
$(function(){
    $(window).scroll(function() {
        var top = $(document).scrollTop();
        var height1 = $(document).height()-$(window).height()-100;   
        var result =  $(document).scrollTop();
        if (top < 660)
        {
        	$(".nav_scroll").removeClass("nav_scroll_act");
        }
        else
        {
        	$(".nav_scroll").addClass("nav_scroll_act");
        }
    });
});

// mask
$(document).ready(function(){
	$("body").on("click",".in_phone",function(){$(this).inputmask("9(999)9999999");});
});


// popup_open
$(document).ready(function(){

	$("#btn_zapis, #pu_zapis .closeform").click(function(){
		$(".pu_rgba").fadeToggle(400);
		$("#pu_zapis").slideToggle(700);
		$("body").toggleClass("overflow");
		$('.pu_calendar_slider').slick({
			dots: false,
			infinite: false,
			speed: 400,
			slidesToShow: 7,
			slidesToScroll: 7,
			adaptiveHeight: true,
			variableWidth: false,
			arrows: true,
			fade: false,
			responsive: [
		    {
		      breakpoint: 420,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 4,
		        arrows: true,
		      }
		    },
		  ]
		});
		return false;
	});

	// pu good
	$(".pu_zapis .btn_main, #pu_good .closeform").click(function(){
		$("#pu_zapis").slideToggle(200);
		$(".pu_good").slideToggle(300);
		return false;
	});

	$(".pu_good .closeform, .pu_good .btn_main").click(function(){
		$(".pu_rgba").fadeToggle(400);
		$(".pu_good").slideToggle(700);
		$("body").toggleClass("overflow");
		return false;
	});

});

// block height
$(window).on("load resize",function(){
	// h1
    $.fn.equivalent = function (){
	    var $blocks = $(this),
	        maxH    = $blocks.eq(0).height(); 
	    $blocks.each(function(){
	        maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
	    });
	    $blocks.height(maxH); 
	}
	$('.block_height').equivalent();
});

// back top line
$(function(){
     $(".scoll_top").click(function(){
	    $("html,body").stop().animate({scrollTop : 0});
	    return false;
	  });
});

// hamburger
var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};

    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }