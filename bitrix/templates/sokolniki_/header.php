<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Page\Asset;
$dir = $APPLICATION->GetCurDir();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="yandex-verification" content="ac5ee8db2c3d9207" />
		<meta name="google-site-verification" content="o1M5bWMvyd2z1hC5asZhvGgYSuqf0BRAkLzSlLEtoPE" />
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
		<meta name='yandex-verification' content='7fd2934be33d6a4e' />
		<meta name='yandex-verification' content='6b3c0eebf5da4e5f' />
		<!-- sk -->
		<meta name="yandex-verification" content="aab45bbdb61e1a2f" />
		<meta name="google-site-verification" content="068ncQMAAAngVh4b7vvn0ag04SO7cglO_2_I6Krg7LQ" />
		
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" href="/upload/template/image/favicon.ico"/>
		
		<?
		Asset::getInstance()->addCss('/upload/template/css/jquery.jscrollpane.css');
		Asset::getInstance()->addCss('/upload/template/css/style.css');
		Asset::getInstance()->addCss('/upload/template/css/poiskpro.css');

		Asset::getInstance()->addCss('/upload/template/css/style_dev.css');
		
		Asset::getInstance()->addJs('/upload/template/js/jquery.maskedinput.min.js');
		Asset::getInstance()->addJs('/upload/template/js/ie-detector.js');
		Asset::getInstance()->addJs('/upload/template/js/modernizr.js');
		//Asset::getInstance()->addJs('/upload/template/js/production.min.js');
		
		Asset::getInstance()->addJs('/upload/template/js/libs.js');
		Asset::getInstance()->addJs('/upload/template/js/jquery.mixitup.min.js');
		Asset::getInstance()->addJs('/upload/template/js/common.js');
		Asset::getInstance()->addJs('/upload/template/js/d-goals.js');
		?>
		
		<!--[if lte IE 9]><meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->
		
		<!--quon-->
<script type="text/javascript">
	var s = document.createElement("script");
	s.src = "//counter.quon.ru/?a=init&t=" + Date.now();
	s.async = 1;
	document.head.appendChild(s)
</script>
<!-- VK Pixel Code -->
<script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?159",t.onload=function(){VK.Retargeting.Init("VK-RTRG-268478-911CF"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-268478-911CF" style="position:fixed; left:-999px;" alt=""/></noscript>
<!-- VK Pixel Code -->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1730209357075973');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1730209357075973&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!--/quon-->
	</head>
<body>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110920192-1"></script>
	<script>
		$(function () {
			function hideExtraPrices(pricelist) {
				var $items = $(pricelist).find('tr');

				if ($items.length > 10) {
					$.each($items, function (index, item) {
						if (index >= 10) {
							$(item).addClass('hidden');
						}
					});

					$(pricelist).closest('.price-item1').append(
						'<a href="javascript:void(0);" '+
						'class="arrow-down show-more-prices">'+
						'Показать ещё <i class="material-icons">&#xE5C5;</i></a>'
					);
				}
			}

			$(document).on('click.show_price_list', '.show-more-prices', function () {
				var $pricelist = $(this).parent();

				$(this).remove();
				$pricelist.find('tbody tr').removeClass('hidden');
				$pricelist.append(
					'<a href="javascript:void(0);" '+
					'class="arrow-down show-less-prices">'+
					'Свернуть список <i class="material-icons">&#xE5C7;</i></a>'
				);
			});

			$(document).on('click.show_price_list', '.show-less-prices', function () {
				var $pricelist = $(this).parent();

				$(this).remove();
				hideExtraPrices($pricelist.find('tbody'));

				$(document.scrollingElement).stop().animate({ scrollTop: $pricelist.offset().top - 200 }, 1000);
			});

			var $targets = $('.price-item1 tbody');
			$.each($targets, function (index, pricelist) {
				hideExtraPrices(pricelist);
			});
		});
	</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-110920192-1');
	</script>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
		<div style="display:none; width:0px; height:0px;" id="langIndicator">русский</div>
	</div>

<div id="page-wrapper" class="page-wrapper">

<header id="header" class="wrapper header">
    <div class="top-header">
            <div class="col-xs-4 left-item">
                <div class="logo-block">
                    <a href="<?=SITE_DIR?>">
                        <img src="/upload/template/image/logo.png" class="logo-img-white" alt="">
                        <img src="/upload/template/image/logo-blue.png" class="logo-img-blue" alt="">
                    </a>
                </div>
                <div class="social-icon">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/social_icon.php"
	)
);?>
                </div>
            </div>
            <div class="col-xs-8 right-item">
				<div class="search-block">
<?$APPLICATION->IncludeComponent("bitrix:search.form", "search", Array(
	"COMPONENT_TEMPLATE" => ".default",
		"PAGE" => "/search/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
		"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
	),
	false
);?>	
				</div>
                <div class="tel-block">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/header_phone.php"
	)
);?>
                </div>
                <div class="link-block">
                    <a href="#"><?=GetMessage('TEMPLATE_APPOINTMENT');?></a>
                </div>
            </div>
        <div class="clear"></div>
    </div>
    <div class="bot-header">

<div id="google_translate_selector">
<img src="/upload/template/image/united-kingdom-flag.png">
<div id="google_translate_element"></div>
<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'ru', includedLanguages: 'en,ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</div>
	
<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "2",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "86400",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
);?>
    </div>
    <div class="btn-right">
        <div class="btn-item btn-app">
            <i class="mdi mdi-lead-pencil"></i>
            <a href="#"><?=GetMessage('TEMPLATE_APPOINTMENT');?></a>
        </div>
        <div class="btn-item btn-call">
            <i class="mdi mdi-phone"></i>
            <a href="#"><?=GetMessage('TEMPLATE_REQ_A_CALL');?></a>
        </div>
		<div class="btn-item btn-sub">
            <i class="mdi mdi-email"></i>
            <a href="#"><?=GetMessage('TEMPLATE_REQ_SUB');?></a>
        </div>
    </div>

    <script type="text/javascript">  
$(function() {
    $('.parent').click(function() {
        $(this).nextUntil('.parent', 'tr.child').slideToggle();
        $(this).toggleClass('open');
    });
});
</script> 
</header>

<div id="page-body" class="page-body<?if(($dir == '/') || ($dir == '/en/')){?> page-home<?}?>">
