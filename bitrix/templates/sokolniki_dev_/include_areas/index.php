<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)  die();?>
<!-- begin head_slider  -->
<div class="head_slider">
    <div>
        <div class="hs_inner hs_bg1 cover">
            <div class="hs_title">
                <div class="hs_title_inner">
                    Первичная консультация <br><b>к любому</b> специалисту — бесплатно!
                    <span>
                        + дисконтная карта со скидками до 10% в подарок!
                    </span>
                </div>
            </div>
            <div class="align_center">
                <a id="btn_zapis" class="btn_main" href="#">ЗАПИСАТЬСЯ</a>
            </div>
        </div>
    </div>
    <div>
        <div class="hs_inner hs_bg2">
            <div class="hs_left">
                <div class="hsl_table">
                    <div class="hsl_cell">
                        <div class="hsl_inner">
                            <div class="hsl_inner2">
                                <div class="hsl_title">
                                    Счастливые часы <br>для имплантации!
                                </div>
                                <div class="hsl_data">
                                    С 17 августа по 17 сентября
                                </div>
                                <div class="hsl_time">
                                    <span>9:00 - 15:00</span>
                                </div>
                                <div class="hsl_info">
                                    Имплантация зуба под ключ
                                </div>
                                <div class="hsl_price">
                                    <span>От 35 000 <i>Р</i></span>
                                </div>
                                <div class="hsl_price_old">
                                    <span>От 55 000 <i>Р</i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?/*<div>
        <div class="hs_inner hs_bg3">
            <div class="hs_left">
                <div class="hsl_table">
                    <div class="hsl_cell">
                        <div class="hsl_inner">
                            <div class="hsl_inner2">
                                <div class="hsl_title">
                                    На учебу <br>с улыбкой!
                                </div>
                                <div class="hsl_data">
                                    С 15 августа по 15 сентября
                                </div>
                                <div class="hsl_info">
                                    СКИДКа 15%
                                </div>
                                <div class="hsl_about">
                                    на все терапевтические услуги <br>для школьников, <br>студентов и педагогов
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>*/?>
    <?/*<div>
        <div class="hs_inner hs_bg4 cover">
            <div class="hs_title">
                
            </div>
            <div class="head_slide_links">
                <a class="btn_main" href="/aktsii/">Перейти</a>
           </div>
        </div>
    </div>*/?>
</div>
<!-- end head_slider -->

<!-- begin spa  -->
<div class="spa">
    <div class="wmain">
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "block_clinic_photo", Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
            "AJAX_MODE" => "N",	// Включить режим AJAX
            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "CACHE_TIME" => "864000",	// Время кеширования (сек.)
            "CACHE_TYPE" => "N",	// Тип кеширования
            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
            "DISPLAY_DATE" => "N",	// Выводить дату элемента
            "DISPLAY_NAME" => "Y",	// Выводить название элемента
            "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
            "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
            "FIELD_CODE" => array(	// Поля
                0 => "",
                1 => "",
            ),
            "FILTER_NAME" => "",	// Фильтр
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
            "IBLOCK_ID" => 11,	// Код информационного блока
            "IBLOCK_TYPE" => 'other',	// Тип информационного блока (используется только для проверки)
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
            "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
            "NEWS_COUNT" => "100",	// Количество новостей на странице
            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
            "PAGER_TITLE" => "Новости",	// Название категорий
            "PARENT_SECTION" => "",	// ID раздела
            "PARENT_SECTION_CODE" => "",	// Код раздела
            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
            "PROPERTY_CODE" => array(	// Свойства
                0 => "",
                1 => "",
            ),
            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
            "SET_STATUS_404" => "N",	// Устанавливать статус 404
            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
            "SHOW_404" => "N",	// Показ специальной страницы
            "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
            "SORT_BY2" => "ID",	// Поле для второй сортировки новостей
            "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        ),
            false
        );?>


        <div class="spa_r">
            <div class="title_decor">
					<span>
						Солнечный остров — SPA-курорт для зубов
					</span>
            </div>
            <div class="spa_info">
                С нами нет причин бояться стоматологов. Мы лечим настолько бережно и безболезненно, что пациенты могут спать на процедуре. Мы сделали все, чтобы пребывание у нас было комфортным и ничем не напоминало стандартные визиты в больницу.
            </div>
        </div>
        <div class="clr"></div>

        <ul class="spa_preim_list">

            <li>
                <div class="spl_ico">
						<span>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico1.png">
						</span>
                </div>
                <div class="spl_name">
                    Без боли
                </div>
                <div class="spl_info">
                    Благодаря компьютерной анестезии вы ничего не почувствуете даже во время введения обезболивающего препарата
                </div>
            </li>
            <li>
                <div class="spl_ico">
						<span>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico2.png">
						</span>
                </div>
                <div class="spl_name">
                    Без стресса
                </div>
                <div class="spl_info">
                    Во время лечения пациент смотрит фильм, слушает любимую музыку или аудиокнигу или спит в мягком ортопедическом кресле
                </div>
            </li>
            <li>
                <div class="spl_ico">
						<span>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico3.png">
						</span>
                </div>
                <div class="spl_name">
                    Домашняя обстановка
                </div>
                <div class="spl_info">
                    Наша клиника не похожа на больницу. У нас уютный интерьер, теплая атмосфера и пейзажи Мальдивского острова
                </div>
            </li>
            <li>
                <div class="spl_ico">
						<span>
							<img src="<?=SITE_TEMPLATE_PATH?>/img/spa_list_ico4.png">
						</span>
                </div>
                <div class="spl_name">
                    Приветливые врачи
                </div>
                <div class="spl_info">
                    Лечащий врач подробно рассказывает о лечении и отвечают на вопросы. Вы понимаете, за какие услуги платите и какой результат получите
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- end spa -->
<!-- begin ysl  -->
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "block_services",
    Array(
        "ADD_SECTIONS_CHAIN" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "864000",
        "CACHE_TYPE" => "A",
        "COUNT_ELEMENTS" => "N",
        "IBLOCK_ID" => "9",
        "IBLOCK_TYPE" => "main",
        "SECTION_CODE" => "",
        "SECTION_FIELDS" => array("NAME","PICTURE"),
        "SECTION_ID" => "",
        "SECTION_URL" => "",
        "SECTION_USER_FIELDS" => array("UF_PREVIEW_TEXT",""),
        "TOP_DEPTH" => "1"
    )
);?>
<!-- end ysl -->
<!-- begin what  -->
<div class="what">
    <div class="tm">
        <div class="tm_inner">
				<span>
					почему мы?
				</span>
        </div>
    </div>
    <ul class="what_list">
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>100% гарантия</b>
							Возвращаем деньги, если вы недовольны работой. Гарантия и условия закреплены в договоре
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>Заключаем договор</b>
							Вы знаете, за какие услуги платите и какой результат получите
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>Бесплатная консультация</b>
							Первичные консультации любого специалиста бесплатны
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>Бесплатные осмотры раз в полгода</b>
							Регулярный профосмотр — залог здоровых зубов
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>Прием в любое время </b>
							Мы работаем с 9 до 21, но если вы не можете прийти в это время, примем поздно вечером или рано утром
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>Передовые технологии</b>
							Используем технологии ведущих клиник и университетов России, США, Западной Европы и Израиля
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>В кредит и рассрочку</b>
							Можно получить красивую улыбку уже сейчас, а оплату вносить постепенно
						</span>
                </div>
            </div>
        </li>
        <li>
            <div class="wl_circle">
                <div class="wl_inner">
						<span>
							<b>Скидки</b>
							Карта со скидкой от 2 до 10% каждому пациенту и еще много акций
						</span>
                </div>
            </div>
        </li>
    </ul>
</div>
<!-- end what -->
<!-- begin equipment  -->
<div class="equipment">
    <div class="wmain">
        <div class="tm tm2">
            <div class="tm_inner">
					<span>
						ПЕРЕДОВОЕ ОСНАЩЕНИЕ
					</span>
            </div>
            <i>
                Используем последние модели стоматологического оборудования из России, США, Германии, Японии и Израиля. <br>Оно помогает быстро поставить точный диагноз и проводить безболезненные процедуры
            </i>
        </div>
        <ul class="equipment_list">
            <li>
                <div class="eql_inner">
                    <div class="eql_img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img1.jpg">
                    </div>
                    <div class="eql_info block_height">
                        Комплекс для изготовления зубных протезов с CAD/CAM системой
                        <span>
								Позволяет изготавливать любые вкладки, виниры, коронки в день обращения
							</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="eql_inner">
                    <div class="eql_img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img2.jpg">
                    </div>
                    <div class="eql_info block_height">
                        3D-томограф Veraviewepocs — снимок за 5 секунд
                        <span>
								Используется при протезировании, ортодонтическом лечении и имплантации
							</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="eql_inner">
                    <div class="eql_img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img3.jpg">
                    </div>
                    <div class="eql_info block_height">
                        Стоматологический микроскоп Opmi Proergo
                        <span>
								Помогает выявить патологии, невидимые невооруженным глазом
							</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="eql_inner">
                    <div class="eql_img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img4.jpg">
                    </div>
                    <div class="eql_info block_height">
                        Комплекс визуализации SuperCam
                        <span>
								Выводит изображение дефекта на экран и помогает наглядно показать дальнейшую тактику лечения
							</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="eql_inner">
                    <div class="eql_img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img5.jpg">
                    </div>
                    <div class="eql_info block_height">
                        Система отбеливания Zoom 4
                        <span>
								Ваши зубы становятся светлее на 8-10 тонов за один визит
							</span>
                    </div>
                </div>
            </li>
            <li>
                <div class="eql_inner">
                    <div class="eql_img">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/tech_img6.jpg">
                    </div>
                    <div class="eql_info block_height">
                        Представитель принципиально новой лазерной технологи BIOLASE MD
                        <span>
								Высочайшие клинические результаты в гораздо более приятных для пациента условиях
							</span>
                    </div>
                </div>
            </li>
        </ul>
        <div class="align_center">
            <a class="btn_main" href="/about-us/equipment/">ПОСМОТРЕТЬ ВСЕ ОБОРУДОВАНИЕ</a>
        </div>
    </div>
</div>
<!-- end equipment -->
<!-- begin doctor  -->
<?
global $arrDoctors;
$arrDoctors = array("PROPERTY"=>array("MAIN_VALUE"=>'Y'));
?>
<?$APPLICATION->IncludeComponent("bitrix:news.list", "block_doctors_main", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
    "CACHE_GROUPS" => "N",	// Учитывать права доступа
    "CACHE_TIME" => "864000",	// Время кеширования (сек.)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
    "DISPLAY_DATE" => "N",	// Выводить дату элемента
    "DISPLAY_NAME" => "Y",	// Выводить название элемента
    "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
    "FIELD_CODE" => array(	// Поля
        0 => "",
        1 => "",
    ),
    "FILTER_NAME" => "arrDoctors",	// Фильтр
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => 1,	// Код информационного блока
    "IBLOCK_TYPE" => 'main',	// Тип информационного блока (используется только для проверки)
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
    "NEWS_COUNT" => "20",	// Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
    "PAGER_TITLE" => "Новости",	// Название категорий
    "PARENT_SECTION" => "",	// ID раздела
    "PARENT_SECTION_CODE" => "",	// Код раздела
    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
    "PROPERTY_CODE" => array(	// Свойства
        0 => "PROFESSION",
        1 => "",
    ),
    "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
    "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
    "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
    "SET_STATUS_404" => "N",	// Устанавливать статус 404
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "SHOW_404" => "N",	// Показ специальной страницы
    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
    "SORT_BY2" => "ID",	// Поле для второй сортировки новостей
    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
    "CLASS" => 'main-team'
),
    false
);?>

<!-- end doctor -->
<!-- begin otz  -->
<?$APPLICATION->IncludeComponent("bitrix:news.list", "block_reviews", Array(
    "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
    "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
    "AJAX_MODE" => "N",	// Включить режим AJAX
    "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
    "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
    "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
    "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
    "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
    "CACHE_GROUPS" => "N",	// Учитывать права доступа
    "CACHE_TIME" => "864000",	// Время кеширования (сек.)
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
    "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
    "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
    "DISPLAY_DATE" => "N",	// Выводить дату элемента
    "DISPLAY_NAME" => "Y",	// Выводить название элемента
    "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
    "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
    "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
    "FIELD_CODE" => array(	// Поля
        0 => "",
        1 => "",
    ),
    "FILTER_NAME" => "",	// Фильтр
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
    "IBLOCK_ID" => 16,	// Код информационного блока
    "IBLOCK_TYPE" => 'other',	// Тип информационного блока (используется только для проверки)
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
    "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
    "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
    "NEWS_COUNT" => "10",	// Количество новостей на странице
    "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
    "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
    "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
    "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
    "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
    "PAGER_TITLE" => "Новости",	// Название категорий
    "PARENT_SECTION" => "",	// ID раздела
    "PARENT_SECTION_CODE" => "",	// Код раздела
    "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
    "PROPERTY_CODE" => array(	// Свойства
        0 => "",
        1 => "",
    ),
    "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
    "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
    "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
    "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
    "SET_STATUS_404" => "N",	// Устанавливать статус 404
    "SET_TITLE" => "N",	// Устанавливать заголовок страницы
    "SHOW_404" => "N",	// Показ специальной страницы
    "SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
    "SORT_BY2" => "ID",	// Поле для второй сортировки новостей
    "SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
    "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
),
    false
);?>
<!-- end otz -->
<!-- begin licens  -->
<div class="licens">
    <div class="wmain">
        <div class="tm tm5">
            <div class="tm_inner">
					<span>
						лицензии и сертификаты
					</span>
            </div>
        </div>
        <ul class="licens_list">

            <li>
                <a  data-fancybox="licens_group" class="fb" href="<?=SITE_TEMPLATE_PATH?>/img/licens_img1.jpg">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img1.jpg">
                </a>
            </li>
            <li>
                <a data-fancybox="licens_group" class="fb" href="<?=SITE_TEMPLATE_PATH?>/img/licens_img2.jpg">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img2.jpg">
                </a>
            </li>
            <li>
                <a  data-fancybox="licens_group" class="fb" href="<?=SITE_TEMPLATE_PATH?>/img/licens_img3.jpg">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img3.jpg">
                </a>
            </li>
            <li>
                <a  data-fancybox="licens_group" class="fb" href="<?=SITE_TEMPLATE_PATH?>/img/licens_img4.jpg">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/licens_img4.jpg">
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- end licens -->
<div id="3dtour" style="display:none" >
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "COMPONENT_TEMPLATE" => ".default",
            "EDIT_TEMPLATE" => "",
            "PATH" => SITE_DIR."include/about-us/3dtour_loader.php"
        )
    );?>
</div>

