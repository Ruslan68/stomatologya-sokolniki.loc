<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?$this->setFrameMode(true);?>


<div class="priem poiskpro-form">
    <div class="wmain">
        <div class="spa_l">
            <div class="priem_img">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/priem_img.jpg">
            </div>
        </div>
        <div class="spa_r">
            <div class="title_decor">
					<span>
						Запишитесь на прием к врачу и получите бонусы:
					</span>
            </div>
            <ul class="spa_list">
                <li>
                    Бесплатная первичная консультация любого специалиста
                </li>
                <li>
                    Карта со скидкой от 2 до 10% на все услуги
                </li>
            </ul>
            <div class="message">
                <?if(!empty($arResult["ERROR_MESSAGE"])){?>
                    <div class="bg-danger">
                        <?foreach($arResult["ERROR_MESSAGE"] as $v)
                            ShowError($v);
                        ?>
                    </div>
                <?}?>
                <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
                    <div class="bg-success">
                        <?=$arResult["OK_MESSAGE"]?>
                    </div>
                <?}?>

                <!-- targets form order form footer -->
                <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            console.log("from_order_form_footer");
                            sendYandex('Form_Submit_Order', 'Macroconversion');
                            sendGtag('Submit', 'Form', 'Order_header');

                            sendYandex('Form_Submitm_Schedule_an_appointment_embedded', 'Macroconversion_marketing');
                            sendGtag('Submitm', 'Form', 'Schedule_an_appointment_embedded');
                        });
                    </script>
                <?}?>

            </div>
            <form class="form_style" action="<?=POST_FORM_ACTION_URI?>" method="POST">
                <?=bitrix_sessid_post()?>
                <div class="in_2">
						<span>
							<input class="in_name <?if(isset($arResult['ERROR']["user_name"])) echo 'has-error';?>" id="app-name" type="text" name="user_name" value="<?=$arResult['ACTIVE']["user_name"]?>" data="Введите имя" placeholder="Введите имя">
						</span>
                    <span>
							<input class="in_phone"  id="app-tel" type="text"  <?if(isset($arResult['ERROR']["user_phone"])) echo ' class="has-error"';?> name="user_phone" value="<?=$arResult['ACTIVE']["user_phone"]?>" data="Введите телефон" placeholder="Введите телефон">
						</span>
                </div>
                <label class="form_personal">
                    <input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-4" name="personal-agree" type="checkbox" value="1" checked>
                    <span>
							<?=GetMessage("MFT_AGREE2")?> <a href="/upload/template/pdf/152-f3.pdf" target="_blank">«<?=GetMessage("MFT_AGREE4")?>»</a>
						</span>
                </label>
                <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
                <input type="hidden" name="action_order" value="" />
                <input id="submit-app" type="submit" class="btn_main" value="<?=GetMessage("MFT_SUBMIT2")?>"/>
            </form>


        </div>
    </div>
</div>

<script>
	$('form #submit-app').click(function(){
		yaCounter32528320.reachGoal('ysp-zapis-na-priem');
		ga('send', 'pageview', '/ysp-zapis-na-priem');
		console.log('ysp-zapis-na-priem');
	});
</script>
