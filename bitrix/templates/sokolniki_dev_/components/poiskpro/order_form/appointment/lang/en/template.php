<?
$MESS ['MFT_NAME'] = "Name";
$MESS ['MFT_EMAIL'] = "Your E-mail";
$MESS ['MFT_MESSAGE'] = "Message";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "Send";
$MESS ['MFT_SUBMIT2'] = "Enroll";

$MESS ['MFT_MAKE_APPOINT'] = "Make an appointment";
$MESS ['MFT_PHONE'] = "Your phone number";
$MESS ['MFT_AGREE1'] = "I confirm my consent";
$MESS ['MFT_AGREE2'] = "- To use as information channels contained in the application, open communication channels of the Internet.";
$MESS ['MFT_AGREE3'] = "- To process my personal data in accordance with the federal law of the Russian Federation 27.07.2006 № 152-Ф3";
$MESS ['MFT_AGREE4'] = "About personal data";
?>