<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["SECTIONS"])){?>
<div class="ysl">
    <div class="wmain">
        <div class="tm">
            <div class="tm_inner">
					<span>
						<?=GetMessage("CSL_BL_OUR_SERVICES")?>
					</span>
            </div>
            <i>
                <?=GetMessage("CSL_BL_TEETH_TEXT")?>
            </i>
        </div>
        <ul class="ysl_list">
        <?
        $i=0;
        foreach($arResult["SECTIONS"] as $key=>$item){
            if($i>=10) {
                break;
            }
            $i++;
            ?>
            <li>
                <a class="yl_name_main" href="<?=$item["SECTION_PAGE_URL"]?>">
                    <div >
                    <img src="<?=$item["PICTURE"]['SRC']?>" alt="<?=$item["PICTURE"]['ALT']?>">
                    </div>
                    <div class="yl_name_inner">
							<span>
								<?=$item["NAME"]?>
							</span>
                    </div>
                </a>
                <a class="yl_inner_list" href="<?=$item["SECTION_PAGE_URL"]?>">
                    <?=$item["UF_PREVIEW_TEXT"]?>
                </a>
            </li>
            <?}?>
        </ul>
    </div>
</div>
 <?}?>