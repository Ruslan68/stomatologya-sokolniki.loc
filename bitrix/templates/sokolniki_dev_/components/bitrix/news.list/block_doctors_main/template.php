<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>
<div class="doctor">
    <div class="tm tm3">
        <div class="tm_inner">
				<span>
					ОПЫТНЫЕ ВРАЧИ
				</span>
        </div>
        <i>
            Наши врачи регулярно повышают квалификацию, участвуют в выставках, конференциях, симпозиумах в России и за рубежом. Среди них члены Израильской и Российской стоматологических ассоциаций, доктора и кандидаты наук, а также врачи без громких регалий, но с хорошим опытом и золотыми руками.
        </i>
    </div>
    <div class="doctor_slider">
    <?foreach($arResult["ITEMS"] as $key=>$arItem){?>
        <div>
            <div class="ds_inner">
                <div class="ds_img">
						<span>
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
						</span>
                </div>
                <div class="ds_name">
                    <?=$arItem['NAME']?>
                    <?if(isset($arItem['PROPERTIES']['PROFESSION']) &&  !empty($arItem['PROPERTIES']['PROFESSION']['VALUE'])){?>
                        <span>
                            <?=$arItem['PROPERTIES']['PROFESSION']['VALUE']?>
                        </span>
                    <?}?>
                </div>
                <div class="align_center">
                    <a class="btn_main" href="#">
                        ЗАПИСАТЬСЯ
                    </a>
                </div>
            </div>
        </div>
        <?}?>
    </div>
    <div class="zapis_title">
        Вы можете выбрать врача и записаться к нему <br>на бесплатный первичный прием
    </div>
    <div class="manager">
        <div class="wmain">
            <div class="tm tm4">
                <div class="tm_inner">
						<span>
							СЛОВО РУКОВОДИТЕЛЯ
						</span>
                </div>
            </div>
            <div class="manager_plashka">
                <div class="mp_img">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/manager_people.jpg" >
                </div>
                <div class="mp_content">
                    <div class="mp_citata">
                        Мне важно, чтобы гости были довольны нашими услугами, поэтому я лично контролирую все процессы и несу личную ответственность за работу нашей клиники.
                    </div>
                    <div class="mp_cont">
                        У вас есть пожелания или вопросы? Я всегда готова выслушать и помочь. <br>
                        Звоните: <a class="mp_phone" href="tel:+79161734408">+7 916 173 44 08</a> <br>
                        Пишите: <a href="mailto:info@stomatologya-sokolniki.ru">info@stomatologya-sokolniki.ru</a>
                    </div>
                    <div class="mp_help">
                        Именно вы помогаете нам становиться лучше
                    </div>
                    <div class="mp_autor">
                        Старобор Светлана Александровна
                        <span>
								Учредитель компании, генеральный директор
							</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?}?>