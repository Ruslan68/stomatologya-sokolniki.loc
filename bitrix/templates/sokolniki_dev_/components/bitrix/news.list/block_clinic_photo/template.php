<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
require_once 'Mobile_Detect.php';
$a = new Mobile_Detect;
$f = $a->isMobile();
?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>

            <div class="spa_l">
                <div class="spa_slider_big">
    <?
    $i = 0;
    foreach($arResult["ITEMS"] as $key=>$arItem){?>
        <?$fileStd = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>666, 'height'=>442), BX_RESIZE_IMAGE_EXACT, false);
        $fileSmall = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>92, 'height'=>61), BX_RESIZE_IMAGE_EXACT, false);
        ?>
        <div>
            <div class="ssb_img">
                <img src="<?=$fileStd["src"]?>">
            </div>
        </div>
        <?
        $i++;
        if($i == 3 && !$a) {?>
            <div>
                <a class="ssb_img 3d_popup" href="#dtour">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/3d.jpg">
                </a>
            </div>
        <?} ?>
        <?}?>
                </div>

                <div class="spa_slider_th">

    <?
    $i = 0;
    foreach($arResult["ITEMS"] as $key=>$arItem){?>
        <?$fileSmall = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>92, 'height'=>61), BX_RESIZE_IMAGE_EXACT, false);?>
        <div>
            <div class="sst_img">
                <img src="<?=$fileSmall["src"]?>">
            </div>
        </div>
        <?
        $i++;
        if($i == 3 && !$a) {?>
            <a class="3d_popup" href="#dtour">
                <div class="sst_img">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/spa_th4.png">
                </div>
            </a>
        <?} ?>
        <?}?>
              </div>
            </div>

<?}?>