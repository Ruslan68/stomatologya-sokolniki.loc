<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

$dir = $APPLICATION->GetCurDir();
$form_property = $APPLICATION->GetProperty("form");
?>

			
<?if(!$form_property){?>

    <!-- begin priem  -->
    <?$APPLICATION->IncludeComponent(
        "poiskpro:order_form",
        "popup_order",
        array(
            'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
            'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
            "AJAX_MODE" => 'Y',
            "TYPE" => 'order'
        ),
        false
    );?>
    <!-- end priem -->
<?}?>
</div>
<!-- / #page-body-->

<!-- begin footer  -->
<div class="footer">
    <div class="wmain">
        <div class="f_left">
            <div class="f_left_inner">
                <div class="f_logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/f_logo.png">
                </div>
                <ul class="f_cont">
                    <li>
							<span>
                             <img src="<?=SITE_TEMPLATE_PATH?>/img/f_cont_ico1.png">
							<?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => SITE_DIR."include/footer_address_new.php"
                                )
                            );?>
							</span>
                    </li>
                    <li>
							<span>
                                <img src="<?=SITE_TEMPLATE_PATH?>/img/f_cont_ico2.png">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_DIR."include/footer_phone_new.php"
                                    )
                                );?>

							</span>
                    </li>
                    <li>
							<span>
								<img src="<?=SITE_TEMPLATE_PATH?>/img/f_cont_ico3.png">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => SITE_DIR."include/footer_time_new.php"
                                    )
                                );?>

							</span>
                    </li>
                </ul>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "COMPONENT_TEMPLATE" => ".default",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/social_icon_new.php"
                    )
                );?>
            </div>
        </div>
        <div class="f_map" id = "map-footer">
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "COMPONENT_TEMPLATE" => ".default",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => SITE_DIR."include/footer_map_new.php"
                )
            );?>
        </div>
    </div>
    <a class="scoll_top" href="#"></a>
</div>
<!-- begin f_line  -->
<div class="f_line">
    <?$APPLICATION->IncludeComponent(
        "bitrix:main.include",
        "",
        Array(
            "AREA_FILE_SHOW" => "file",
            "AREA_FILE_SUFFIX" => "inc",
            "COMPONENT_TEMPLATE" => ".default",
            "EDIT_TEMPLATE" => "",
            "PATH" => SITE_DIR."include/copyright.php"
        )
    );?>
</div>
<!-- end f_line -->
<!-- begin pu_rgba  -->
<div id="rgba_appointment" style="display: none;" class="pu_rgba">
    <div class="pu_table poiskpro-form">
        <div class="pu_cell">
<?$APPLICATION->IncludeComponent(
    "poiskpro:order_form",
    "appointment",
    array(
        'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
        'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
        "AJAX_MODE" => 'Y',
        "TYPE" => 'appointment',
        "IBLOCK_ID_DOCTORS" => '1',
        "SECTION_ID_DOCTORS" => '2'
    ),
    false
);?>
    </div>
    </div>
</div>

<!-- end pu_rgba -->

<div class="popup popup-call">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"popup_call",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_ORDER_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'popup_call'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>

<div id="rgba_3dtour" style="display: none;" class="pu_rgba">
    <div class="pu_table">
        <div class="pu_cell">
            <div id="pu_3d" style="display: none;" class="pu_inner pu_zapis_main">
                <a class="closeform" href="#"></a>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "COMPONENT_TEMPLATE" => ".default",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => SITE_DIR."include/about-us/3dtour_loader.php"
                    )
                );?>
            </div>
        </div>
    </div>
</div>

<div class="popup popup-appointment">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"appointment",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_REQ_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'appointment'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>


<div class="popup popup-sub">
    <div class="popup-container">
        <div class="close">
            <img src="/upload/template/image/close.png" alt="">
        </div>
        <div class="form-block">
<?$APPLICATION->IncludeComponent(
	"poiskpro:subscribe.form",
	"",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_SUBSCRIBE_OK'),
		'REQUIRED_FIELDS' => array('EMAIL'),
		'IBLOCK_ID' => 35,
		"AJAX_MODE" => 'Y',
		'SOLT' => 'email',
		'PAGE' => '/subscribe/'
	),
false
);?>
        </div>
    </div>
    <div class="popup-overlay"></div>
</div>

<?$APPLICATION->IncludeComponent(
	"poiskpro:one_click",
	"",
	array(
		'OK_TEXT' => GetMessage('TEMPLATE_ORDER_ACCEPTED'),
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y'
	),
false
);?>

		</div>
		<!-- / #page-wrapper-->
		
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/script.php"
	)
);?>	

<?$APPLICATION->IncludeComponent("poiskpro:one_click","",Array());?>
	
	<!-- RedHelper -->
<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" 
src="https://web.redhelper.ru/service/main.js?c=stomatologyasokolniki">
</script> 
<!--/Redhelper -->
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/schema_contacts.php"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/seo.php"
	)
);?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.head_slider').slick({
            dots: true,
            infinite: true,
            speed: 600,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: false,
            fade: false,
        });
        $('.spa_slider_big').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            asNavFor: '.spa_slider_th',
            dots: false,
            infinite: true,
            adaptiveHeight: true,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                    }
                },
            ]
        });
        $('.spa_slider_th').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.spa_slider_big',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
            ]
        });
        $('.doctor_slider').slick({
            dots: false,
            infinite: true,
            speed: 400,
            slidesToShow: 4,
            slidesToScroll: 1,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: true,
            fade: false,
            responsive: [
                {
                    breakpoint: 1550,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 920,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 460,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                    }
                },
            ]
        });
        $('.otz_slider').slick({
            dots: false,
            infinite: true,
            speed: 400,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: true,
            fade: false,
            responsive: [
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false,
                    }
                },
            ]
        });
    });
</script>
	</body>

<!-- Slider -->



<script type="text/javascript">
    (function($) {
        $(function() {
            $('input, select').styler({
                fileBrowse: 'Выбрать',
            });
        })
    })(jQuery)
</script>
<?if($APPLICATION->GetCurPage() == "/"):?>
<script>
    jQuery(document).ready(function () {
        'use strict';
        jQuery('#datepicker_time').datetimepicker();
    });
</script>

<?endif?>
</html>
