<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$monthes = [
    'январь',
    'февраль',
    'март',
    'апрель',
    'май',
    'июнь',
    'июль',
    'август',
    'сентябрь',
    'октябрь',
    'ноябрь',
    'декабрь'
];
$monthesR = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июль',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
];
$days = array(
    'ВСК', 'ПН', 'ВТ', 'СР',
    'ЧТ', 'ПТ', 'СБ'
);
// echo"<pre>";print_r($arResult);echo"</pre>";
?>
<?$this->setFrameMode(true);?>


            <!-- pu -->
            <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
                <div class="pu_inner pu_good">
                    <a class="closeform" href="#"></a>
                    <div class="pu_title">
                        Вы успешно записаны на прием!
                        <span>
						Мы перезвоним вам и подтвердим дату и время
					</span>
                    </div>
                    <div class="align_center">
                        <button class="btn_main">ЗАКРЫТЬ</button>
                    </div>
                </div>
                <script>
                    $("#pu_good .closeform, .pu_good .btn_main").on("click",function(e){
                        e.preventDefault();
                        $(this).closest(".pu_good").slideToggle(200);
                        $("body").toggleClass("overflow");
                        $(".pu_rgba").fadeToggle(400);

                    });
                </script>
            <?}else {?>
            <div id="pu_zapis" style="<?=(!empty($arResult["ERROR_MESSAGE"]))? "": "display: none"?>" class="pu_inner pu_zapis_main">
                <a class="closeform" href="#"></a>
                <div class="tm tm4">
                    <div class="tm_inner">
						<span>
							записаться на прием <?=date();?>
						</span>
                    </div>
                </div>
                <form class="form_style" action="<?=POST_FORM_ACTION_URI?>" method="POST">
                    <?=bitrix_sessid_post()?>
                    <div class="pu_zapis">
                        <div class="in_2 in_pu_select">
							<span>
								<select class="sel_doctor" name="doctor_name">
									<option value="Любой" selected>Выберите врача</option>
                                    <?foreach ($arResult["DOCTORS_LIST"] as $doctor):?>
                                        <option value="<?=$doctor["NAME"]?>" <?if($doctor["NAME"] == $_POST['doctor_name']) echo 'selected=""'?>><?=$doctor["NAME"]?>, <?=$doctor["PROPERTY_PROFESSION_VALUE"]?> </option>
                                    <?endforeach;?>
								</select>
                                <input class="in_data <?if($_POST && !$_POST['TIME_APPOINTMENT']) echo 'has-error';?>" type="text" name="TIME_APPOINTMENT" value="<?=$_POST['TIME_APPOINTMENT']?>" data="">
                                <div class="pu_calendar_slider">

                                        <?for($i=0; $i<50; $i++){?>
                                            <?
                                            $curTimeStamp = time() + $i*86400;
                                            ?>
                                     <div>
                                            <div class="pcs_inner">
											<ul class="pcs_list">
												<li>
													<div class="pcs_list_name">
														<?if($i == 0 || date("j", $curTimeStamp) == 1):?>
                                                           <?=$monthes[date('n',$curTimeStamp )-1]?>
                                                        <?endif?>
													</div>
													<div class="pcs_list_data">
                                                        <?=date("j", $curTimeStamp)." ".$days[date("w", $curTimeStamp)]?>
													</div>
													<ul class="pcs_list_time" data-day="<?=($i==0)?"Сегодня": date("j", $curTimeStamp)." ".$monthesR[date('n',$curTimeStamp )-1];?>">
														<li >10:00</li>
														<li>11:00</li>
														<li>12:00</li>
														<li>13:00</li>
														<li>14:00</li>
														<li>15:00</li>
														<li>16:00</li>
														<li>17:00</li>
														<li>18:00</li>
														<li>19:00</li>
														<li>20:00</li>
													</ul>
												</li>
											</ul>
										</div>
                                         	</div>
                                        <?}?>




								</div>
							</span>
                            <span>
                                <?
                                    // var_dump($arResult['ACTIVE']["user_phone"]);
                                ?>
                                <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
                                <input class="in_name <?if(isset($arResult['ERROR']["user_name"])) echo 'has-error';?>" id="app-name" type="text" name="user_name" value="<?=$arResult['ACTIVE']["user_name"]?>" data="Введите имя" placeholder="Введите имя">
                                <input class="in_phone <?if($arResult['ACTIVE']["user_phone"] == '_(___)_______') echo 'has-error';?> <?if(isset($arResult['ERROR']["user_phone"])) echo 'has-error';?>"  id="app-tel" type="text"  name="user_phone" value="<?=$arResult['ACTIVE']["user_phone"]?>" data="Введите телефон" placeholder="Введите телефон">
								<label class="form_personal">
                                <input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-testim3" name="personal-agree" type="checkbox" value="1" checked>
                                 <span>
							    <?=GetMessage("MFT_AGREE2")?> <a href="/upload/template/pdf/152-f3.pdf" target="_blank">«<?=GetMessage("MFT_AGREE4")?>»</a>
						        </span>
								</label>
								<button class="btn_main" id="submit-app" type="submit" ><?=GetMessage("MFT_SUBMIT2")?></button>
							</span>

                        </div>
                    </div>
                </form>
            </div>
<?}?>
            <!-- pu -->
            <?if(!empty($arResult["ERROR_MESSAGE"])){?>
           <!--        <div class="pu_inner pu_good">
                <a class="closeform" href="#"></a>
                <div class="pu_title">
                    <?/*foreach($arResult["ERROR_MESSAGE"] as $v)
                        ShowError($v);
                   */ ?>
                </div>
                <div class="align_center">
                    <button class="btn_main">ЗАКРЫТЬ</button>
                </div>
            </div>-->
                <script>
              /*      $("#pu_good .closeform, .pu_good .btn_main").on("click",function(e){
                        e.preventDefault();
                        $(this).closest(".pu_good").slideToggle(200);
                        $("body").toggleClass("overflow");
                        $(".pu_rgba").fadeToggle(400);

                    });*/
      $('.pu_calendar_slider').slick({
            dots: false,
            infinite: false,
            speed: 400,
            slidesToShow: 7,
            slidesToScroll: 7,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: true,
            fade: false,
            responsive: [
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        arrows: true,
                    }
                },
            ]
        });

                  $(document).ready(function() {
                    
                /*      $(document).on('click','#pu_zapis .closeform', function() {
                          $('#rgba_appointment').fadeToggle(400);
                          console.log('ajaaaxx');
                      });*/
                  });





              $(".pcs_list_time li").click( function() {
              $self = $(this);
              alert("fdasdfsa");
              $("input[name='TIME_APPOINTMENT']").val($self.parent().data("day")+", "+$self.text());
          });

                </script>

                <script type="text/javascript">
                    (function($) {
                        $(function() {
                            $('input, select').styler({
                                fileBrowse: 'Выбрать',
                            });
                        })
                    })(jQuery)
                </script>
            <?}?>
