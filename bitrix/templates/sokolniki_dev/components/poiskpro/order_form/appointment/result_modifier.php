<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>
<?
if(CModule::IncludeModule("Iblock")){
    $arFilter = array(
        "IBLOCK_ID" => $arParams["IBLOCK_ID_DOCTORS"],
        "SECTION_ID" => $arParams["SECTION_ID_DOCTORS"],
        "ACTIVE" => "Y"
    );
    $arSelect = array(
        "ID", "IBLOCK_ID","NAME", "PROPERTY_PROFESSION"
    );

    $dbDoctors = CIBlockElement::GetList(
        array("SORT" => "asc"),
        $arFilter,
        false,
        false,
        $arSelect
    );
    while($arDoctor = $dbDoctors->GetNext()) {
        $arResult["DOCTORS_LIST"][$arDoctor["ID"]] = $arDoctor;
    }

}
