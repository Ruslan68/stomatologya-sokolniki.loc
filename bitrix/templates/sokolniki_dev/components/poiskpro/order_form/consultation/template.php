<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

$monthes = [
    'январь',
    'февраль',
    'март',
    'апрель',
    'май',
    'июнь',
    'июль',
    'август',
    'сентябрь',
    'октябрь',
    'ноябрь',
    'декабрь'
];
$monthesR = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июль',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
];
$days = array(
    'ВСК', 'ПН', 'ВТ', 'СР',
    'ЧТ', 'ПТ', 'СБ'
);
// echo"<pre>";print_r($arResult);echo"</pre>";
?>
<?$this->setFrameMode(true);?>


            <!-- pu -->
            <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
                <div class="pu_inner pu_good">
                    <a class="closeform" href="#"></a>
                    <div class="pu_title">
                        Вы успешно записаны на прием!
                        <span>
						Мы перезвоним вам и подтвердим дату и время
					</span>
                    </div>
                    <div class="align_center">
                        <button class="btn_main">ЗАКРЫТЬ</button>
                    </div>
                </div>
                <script>
                    $("#pu_good .closeform, .pu_good .btn_main").on("click",function(e){
                        e.preventDefault();
                        $(this).closest(".pu_good").slideToggle(200);
                        $("body").toggleClass("overflow");
                        $(".pu_rgba").fadeToggle(400);

                    });
                </script>
            <?}else {?>
            <div id="pu_consultation" style="<?=(!empty($arResult["ERROR_MESSAGE"]))? "": "display: none"?>" class="pu_inner pu_zapis_main">
                <a class="closeform" href="#"></a>
                <div class="tm tm4">
                    <div class="tm_inner">
						<span>
						Первичная консультация<br/> любого специалиста БЕСПЛАТНО
						</span>
                    </div>
                </div>
				<div class="fdesk">Оставьте свой номер телефона, мы перезвоним и запишем вас на бесплатную консультацию к любому специалисту</div>
                <form class="form_style" action="<?=POST_FORM_ACTION_URI?>" method="POST">
                    <?=bitrix_sessid_post()?>
                    <div class="pu_zapis">
                        <div class="in_2 in_pu_select">
							<span>
							 <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>"/>
                                <input class="in_name <?if(isset($arResult['ERROR']["user_name"])) echo 'has-error';?>" id="app-name" type="text" name="user_name" value="<?=$arResult['ACTIVE']["user_name"]?>" data="Введите имя" placeholder="Введите имя">
							</span>
                            <span>
                                <?
                                    // var_dump($arResult['ACTIVE']["user_phone"]);
                                ?>
                               
                                <input class="in_phone <?if($arResult['ACTIVE']["user_phone"] == '_(___)_______') echo 'has-error';?> <?if(isset($arResult['ERROR']["user_phone"])) echo 'has-error';?>"  id="app-tel" type="text"  name="user_phone" value="<?=$arResult['ACTIVE']["user_phone"]?>" data="Введите телефон" placeholder="Введите телефон">
							
							</span>
							<div class="in_1">
								<label class="form_personal">
                                <input class="personal-agree<?if(isset($arResult['ERROR']["personal-agree"])) echo ' has-error';?>" id="personal-agree-testim3" name="personal-agree" type="checkbox" value="1" checked>
                                 <span>
							    Согласие на  <a href="/upload/template/pdf/152-f3.pdf" target="_blank">обработку персональных данных</a>
						        </span>
								</label>
								<button class="btn_main" id="submit-app" type="submit" ><?=GetMessage("MFT_SUBMIT2")?></button>
</div>
                        </div>
                    </div>
                </form>
            </div>
<?}?>
            <!-- pu -->
            <?if(!empty($arResult["ERROR_MESSAGE"])){?>
           <!--        <div class="pu_inner pu_good">
                <a class="closeform" href="#"></a>
                <div class="pu_title">
                    <?/*foreach($arResult["ERROR_MESSAGE"] as $v)
                        ShowError($v);
                   */ ?>
                </div>
                <div class="align_center">
                    <button class="btn_main">ЗАКРЫТЬ</button>
                </div>
            </div>-->
                <script>
              /*      $("#pu_good .closeform, .pu_good .btn_main").on("click",function(e){
                        e.preventDefault();
                        $(this).closest(".pu_good").slideToggle(200);
                        $("body").toggleClass("overflow");
                        $(".pu_rgba").fadeToggle(400);

                    });*/
      $('.pu_calendar_slider').slick({
            dots: false,
            infinite: false,
            speed: 400,
            slidesToShow: 7,
            slidesToScroll: 7,
            adaptiveHeight: true,
            variableWidth: false,
            arrows: true,
            fade: false,
            responsive: [
                {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        arrows: true,
                    }
                },
            ]
        });

                  $(document).ready(function() {
                    
                /*      $(document).on('click','#pu_zapis .closeform', function() {
                          $('#rgba_appointment').fadeToggle(400);
                          console.log('ajaaaxx');
                      });*/
                  });





              $(".pcs_list_time li").click( function() {
              $self = $(this);
              alert("fdasdfsa");
              $("input[name='TIME_APPOINTMENT']").val($self.parent().data("day")+", "+$self.text());
          });

                </script>

                <script type="text/javascript">
                    (function($) {
                        $(function() {
                            $('input, select').styler({
                                fileBrowse: 'Выбрать',
                            });
                        })
                    })(jQuery)
                </script>
            <?}?>
