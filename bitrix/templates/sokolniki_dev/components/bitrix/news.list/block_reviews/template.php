<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo "<pre>";print_r($arResult);echo "</pre>";
?>


<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>
<div class="otz">
    <div class="wmain">
        <div class="tm tm5">
            <div class="tm_inner">
					<span>
						ОТ НАС УХОДЯТ С УЛЫБКОЙ
					</span>
            </div>
        </div>

        <div class="otz_slider">
            <?foreach($arResult["ITEMS"] as $key=>$arItem){?>
                <?$file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>250, 'height'=>250), BX_RESIZE_IMAGE_EXACT, false);?>

                <div>
                    <div class="os_inner">
                        <div class="os_ava">
                <?if($arItem['PREVIEW_PICTURE']){?>
                            <img src="<?=$file['src']?>">
                <?}/*else {?>
                    <img src="/upload/template/image/doctor-testimonial-2.png">
                <?}*/?>
                        </div>
                        <div class="os_content">
                            <div class="main_otz_inner">
                                <?=$arItem['PREVIEW_TEXT']?>
                            </div>
                            
                            <span>
								 <?=$arItem['NAME']?>
								<i>
									Гость
								</i>
							</span>
                        </div>
                    </div>
                </div>

            <?}?>

        </div>
    </div>
</div>
<?}?>
