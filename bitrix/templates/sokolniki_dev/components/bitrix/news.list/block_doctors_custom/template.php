<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(isset($arResult["ITEMS"]) && !empty($arResult["ITEMS"])){?>
<div class="doctor">
    <div class="tm tm3">
        <div class="tm_inner">
				<span>
					ОПЫТНЫЕ ВРАЧИ
				</span>
        </div>
        <i>Наши врачи регулярно повышают квалификацию, участвуют в выставках, конференциях, симпозиумах в России и за рубежом. Среди них члены Израильской и Российской стоматологических ассоциаций, доктора и кандидаты наук, а также врачи без громких регалий, но с хорошим опытом и золотыми руками.</i>
    </div>
    <div class="doctor_slider_custom">
    <?foreach($arResult["ITEMS"] as $key=>$arItem){?>
        <div>
            <div class="ds_inner">
                <div class="ds_img">
						<span>
							<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
						</span>
                </div>
                <div class="ds_name">
                    <?=$arItem['NAME']?>
                    <?if(isset($arItem['PROPERTIES']['PROFESSION']) &&  !empty($arItem['PROPERTIES']['PROFESSION']['VALUE'])){?>
                        <span>
                            <?=$arItem['PROPERTIES']['PROFESSION']['VALUE']?>
                        </span>
                    <?}?>
                </div>
                <div class="align_center">
                    <a class="btn_main" href="#">
                        ЗАПИСАТЬСЯ
                    </a>
                </div>
            </div>
        </div>
        <?}?>
    </div>
</div>
<?}?>