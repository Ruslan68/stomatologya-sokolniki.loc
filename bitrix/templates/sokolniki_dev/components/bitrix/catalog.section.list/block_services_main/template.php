<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!empty($arResult["SECTIONS"])){?>
<div class="ysl">
    <div class="wmain">
        <div class="tm">
            <div class="tm_inner">
					<span>
						<?=GetMessage("CSL_BL_OUR_SERVICES")?>
					</span>
            </div>
            <i>
                <?=GetMessage("CSL_BL_TEETH_TEXT")?>
            </i>
        </div>
        <ul class="ysl_list">
        <?
        $i=0;
        foreach($arResult["SECTIONS"] as $key=>$item){
            if($i>=10) {
                break;
            }
            $i++;
            ?>
            <li class="yl_<?=$i?>">
                <div class="yl_name_main">
                    <?
                    $img = CFile::ResizeImageGet($item["PICTURE"]["ID"], array('width'=>50, 'height'=>50), BX_RESIZE_IMAGE_EXACT, false);
                    ?>
                    <img src="<?=$img['src']?>" alt="<?=$item["PICTURE"]['ALT']?>">
                    <div class="yl_name_inner">
                        <span>
                            <?=$item["NAME"]?>
                        </span>
                    </div>
                </div>
                <div class="yl_inner_list">
                    <?
                    foreach($item['UF_MAIN_TEXT'] as $text){
                    ?>
                        <span><?=$text?></span>
                    <?}?>
                </div>
            </li>
            <?}?>
        </ul>
    </div>
</div>
 <?}?>

