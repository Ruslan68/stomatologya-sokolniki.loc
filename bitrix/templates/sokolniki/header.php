<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();

use \Bitrix\Main\Page\Asset;
$asset = Asset::getInstance();
$dir = $APPLICATION->GetCurDir();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="yandex-verification" content="ac5ee8db2c3d9207" />
		<meta name="google-site-verification" content="o1M5bWMvyd2z1hC5asZhvGgYSuqf0BRAkLzSlLEtoPE" />
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name='yandex-verification' content='7fd2934be33d6a4e' />
		<meta name='yandex-verification' content='6b3c0eebf5da4e5f' />
		<!-- sk -->
		<meta name="yandex-verification" content="aab45bbdb61e1a2f" />
		<meta name="google-site-verification" content="068ncQMAAAngVh4b7vvn0ag04SO7cglO_2_I6Krg7LQ" />
		
		<title><?$APPLICATION->ShowTitle();?></title>
        <link rel="shortcut icon" href="/upload/template/image/favicon.ico"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <!--<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">-->

        <?
        CJSCore::Init( 'jquery' );
        $asset->addCss(SITE_TEMPLATE_PATH.'/css/style.css');
        	$asset->addCss(SITE_TEMPLATE_PATH.'/css/hamburgers.css');
        $asset->addCss(SITE_TEMPLATE_PATH.'/css/slick.css');
        //- Form Styler -->
        $asset->addCss(SITE_TEMPLATE_PATH.'/css/jquery.formstyler.css');
        $asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.formstyler.min.js');
        //form styler
        $asset->addCss('/upload/template/css/jquery.jscrollpane.css');
     	$asset->addCss('/upload/template/css/style_dev.css');
        $asset->addCss('/upload/template/css/poiskpro.css');

        $asset->addJs('/upload/template/js/jquery.maskedinput.min.js');
        $asset->addJs('/upload/template/js/ie-detector.js');
        $asset->addJs('/upload/template/js/modernizr.js');
        //Asset::getInstance()->addJs('/upload/template/js/production.min.js');

        $asset->addJs(SITE_TEMPLATE_PATH.'/js/slick.min.js');
        $asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.inputmask.js');
        
        $asset->addJs('/upload/template/js/libs.js');
        $asset->addJs('/upload/template/js/jquery.mixitup.min.js');
        $asset->addJs('/upload/template/js/common_dev.js');
        $asset->addJs('/upload/template/js/d-goals.js');
        $asset->addJs(SITE_TEMPLATE_PATH.'/js/script_js.js');
        ?>
        <?if($APPLICATION->GetCurPage() == "/") {
        	
            $asset->addCss(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.css');
            $asset->addJs(SITE_TEMPLATE_PATH.'/js/fancybox/jquery.fancybox.min.js');
           $asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.datetimepicker.full.min.js');
            $asset->addCss(SITE_TEMPLATE_PATH.'/css/jquery.datetimepicker.css');
/*            $asset->addCss(SITE_TEMPLATE_PATH.'/css/magnific-popup.css');
            $asset->addJs(SITE_TEMPLATE_PATH.'/js/jquery.magnific-popup.min.js');*/
       }?>
        <?$APPLICATION->ShowHead();?>

		<!--[if lte IE 9]><meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->
		
		<!--quon-->
<script type="text/javascript">
	var s = document.createElement("script");
	s.src = "//counter.quon.ru/?a=init&t=" + Date.now();
	s.async = 1;
	document.head.appendChild(s)
</script>
<!--/quon-->
	</head>
<body class="<?if($APPLICATION->GetCurDir() == '/') echo 'main';?>">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110920192-1"></script>
	<script>
		$(function () {
			function hideExtraPrices(pricelist) {
				var $items = $(pricelist).find('tr');

				if ($items.length > 10) {
					$.each($items, function (index, item) {
						if (index >= 10) {
							$(item).addClass('hidden');
						}
					});

					$(pricelist).closest('.price-item1').append(
						'<a href="javascript:void(0);" '+
						'class="arrow-down show-more-prices">'+
						'Показать ещё <i class="material-icons">&#xE5C5;</i></a>'
					);
				}
			}

			$(document).on('click.show_price_list', '.show-more-prices', function () {
				var $pricelist = $(this).parent();

				$(this).remove();
				$pricelist.find('tbody tr').removeClass('hidden');
				$pricelist.append(
					'<a href="javascript:void(0);" '+
					'class="arrow-down show-less-prices">'+
					'Свернуть список <i class="material-icons">&#xE5C7;</i></a>'
				);
			});

			$(document).on('click.show_price_list', '.show-less-prices', function () {
				var $pricelist = $(this).parent();

				$(this).remove();
				hideExtraPrices($pricelist.find('tbody'));

				$(document.scrollingElement).stop().animate({ scrollTop: $pricelist.offset().top - 200 }, 1000);
			});

			var $targets = $('.price-item1 tbody');
			$.each($targets, function (index, pricelist) {
				hideExtraPrices(pricelist);
			});
		});
	</script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-110920192-1');
	</script>
	<div id="panel">
		<?$APPLICATION->ShowPanel();?>
		<div style="display:none; width:0px; height:0px;" id="langIndicator">русский</div>
	</div>

<div id="page-wrapper" class="page-wrapper <?if($APPLICATION->GetCurDir() == '/') echo 'container_main';?>">
<div class="hl_fixed">
	<div class="hlf_top">
	<div class="top-header_menu nav-block"><br></div>
		<header id="header" class="wrapper header">
		 <div class="bot-header" style="background: #FFF; margin-top: -140px; z-index:3;">

		<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "86400",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "new_top",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_menu"
	),
	false
);?>
		    </div>

		    <div class="top-header" style="margin-top: 42px; z-index: 1;">
		            <div class="col-xs-4 left-item logo_main">
		                <div class="logo-block">
		                    <a href="<?=SITE_DIR?>">
		                        <img src="/upload/template/image/logo.png" class="logo-img-white" alt="">
		                        <img src="/upload/template/image/logo-blue.png" class="logo-img-blue" alt="">
		                    </a>
		                </div>
		            </div>
		            <div class="col-xs-8 right-item">
						<div class="search-block">
		<?$APPLICATION->IncludeComponent("bitrix:search.form", "search", Array(
			"COMPONENT_TEMPLATE" => ".default",
				"PAGE" => "/search/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
				"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
			),
			false
		);?>	
						</div>
						<div class="adress">
						<p><i class="fas fa-map-marker-alt" style="font-size: 22px; margin-left: -28px;">&#8194;</i>Москва, ул. Шумкина, 11А</p>
                        <p>м. Сокольники</p>
                        <p>Каждый день: 9:00-21:00</p>
                        </div>
		                <div class="link-block">
		                    <a href="#"><?=GetMessage('TEMPLATE_APPOINTMENT');?></a>
		                </div>
		            </div>
		        <div class="clear"></div>
		    </div>
		    <div class="bot-header nav-block nav-block_menu" style="background: #FFF; z-index: 0;">


		<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"horizontal_multilevel_custom", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "4",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "86400",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "all_services",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "horizontal_multilevel_custom"
	),
	false
);?>
		    </div>

		     <div class="bot-header uslugi" style="background: #FFF;margin-top: 0px;z-index:3;"> 

		<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_menu_uslugi", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "3",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "86400",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "menu_uslugi",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_menu_uslugi"
	),
	false
);?>
		    </div>
		   <?/* <div class="btn-right">
		        <div class="btn-item btn-app_">
		            <i class="mdi mdi-lead-pencil"></i>
		            <a href="#"><?=GetMessage('TEMPLATE_APPOINTMENT');?></a>
		        </div>
		        <div class="btn-item btn-call">
		            <i class="mdi mdi-phone"></i>
		            <a href="#"><?=GetMessage('TEMPLATE_REQ_A_CALL');?></a>
		        </div>
		    </div>*/?>

		    <script type="text/javascript">  
		$(function() {
		    $('.parent').click(function() {
		        $(this).nextUntil('.parent', 'tr.child').slideToggle();
		        $(this).toggleClass('open');
		    });
		});
		</script> 
		</header>
	</div>
</div>
<div id="page-body" class="page-body<?if(($dir == '/') || ($dir == '/en/')){?> page-home<?}?>">
