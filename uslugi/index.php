<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Услуги | Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ»");
$APPLICATION->SetPageProperty("keywords", "стоматологические услуги, услуги стоматолога");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» предлагает полный спектр стоматологических услуг. Записаться на прием можно по телефонам, указанным на сайте. ");
$APPLICATION->SetTitle("Услуги");
?>

<? if ($_SERVER['REQUEST_URI'] == '/uslugi/terapevticheskaya-stomatologiya/restavratsiya-zubov/'): ?>
	<? $APPLICATION->AddHeadString('<link rel="canonical" href="https://stomatologya-sokolniki.ru/uslugi/otbelivanie-zubov/restavratsiya-zubov/" />',true); ?>
<? endif; ?>

<?
global $arrFilter;
$arrFilter = array("!=PROPERTY_HIDDEN_VALUE"=>"Y");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"page_Services", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BASKET_URL" => "/personal/basket.php",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "864000",
		"CACHE_TYPE" => "A",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BRAND_USE" => "N",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_PROPERTY_CODE" => array(
			0 => "TRITMENT_CREDIT_1",
			1 => "TRITMENT_CREDIT_2",
			2 => "",
		),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILE_404" => "",
		"FILTER_VIEW_MODE" => "VERTICAL",
		"IBLOCK_ARTICLES" => array(
			"IBLOCK_ID" => "8",
			"IBLOCK_TYPE" => "Other",
		),
		"IBLOCK_DOCTORS" => array(
			"IBLOCK_ID" => "1",
			"IBLOCK_TYPE" => "main",
		),
		"IBLOCK_ID" => "9",
		"IBLOCK_PHOTOS" => array(
			"IBLOCK_ID" => "5",
			"IBLOCK_TYPE" => "Other",
		),
		"IBLOCK_PRICE" => array(
			"IBLOCK_ID" => "2",
			"IBLOCK_TYPE" => "main",
		),
		"IBLOCK_TYPE" => "main",
		"IBLOCK_WHYWE" => array(
			"IBLOCK_ID" => "9",
			"IBLOCK_TYPE" => "Other",
		),
		"INCLUDE_SUBSECTIONS" => 'Y',
		"INSTANT_RELOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_PROPERTY_CODE" => array(
			0 => "TRITMENT_CREDIT_1",
			1 => "TRITMENT_CREDIT_2",
			2 => "UF_PREVIEW_TEXT",
			3 => "",
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => "Сравнение",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "35",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
		),
		"PRICE_VAT_INCLUDE" => "N",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "3",
		"SEF_FOLDER" => "/uslugi/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "N",
		"SHOW_404" => "Y",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "N",
		"SIDEBAR_DETAIL_SHOW" => "Y",
		"SIDEBAR_PATH" => "",
		"SIDEBAR_SECTION_SHOW" => "Y",
		"TEMPLATE_THEME" => "blue",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"TOP_VIEW_MODE" => "SECTION",
		"USE_COMPARE" => "N",
		"USE_ELEMENT_COUNTER" => "N",
		"USE_FILTER" => "Y",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_STORE" => "N",
		"IBLOCK_EQUIPMENT" => array(
			"IBLOCK_ID" => "4",
			"IBLOCK_TYPE" => "other",
		),
		"IBLOCK_GALLERY" => array(
			"IBLOCK_ID" => "14",
			"IBLOCK_TYPE" => "other",
		),
		"IBLOCK_VIDEO" => array(
			"IBLOCK_ID" => "15",
			"IBLOCK_TYPE" => "other",
		),
		"FILTER_NAME" => "arrFilter",
		"COMPONENT_TEMPLATE" => "page_Services",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "TRITMENT_CREDIT_1",
			1 => "TRITMENT_CREDIT_2",
			2 => "",
		),
		"FILTER_PRICE_CODE" => array(
		),
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE_PATH#/",
			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_ID#/filter/#SMART_FILTER_PATH#/apply/",
		),		
		
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
