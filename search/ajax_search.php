<?
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
?>

<?
	function getStrongTextTitle($str, $q) {
		$q = mb_strtolower($q);
		$str = mb_strtolower($str);

		if (strpos($str, $q) !== false) {
			return true;
		}

		return false;
	}

	function getStrongTextBody($input_str, $q) {
		$q = mb_strtolower($q);
		$str = mb_strtolower($input_str);

		$pos = mb_strpos($str, $q);

		if ($pos !== false) {
			$first_content = mb_substr($str, 0, $pos);
			$last_space_pos = (int)mb_strrpos($first_content, ' ');
			$length = $pos - $last_space_pos + mb_strlen($q);
			$first_content = trim(mb_substr($first_content, $last_space_pos, $length));

			$first_content .= $q;			

			$tmp_pos = $pos + mb_strlen($q);

			$second_content = mb_substr($str, $tmp_pos);
			$first_space_pos = mb_strpos($second_content, ' ');
			$second_content = trim(mb_substr($second_content, 0, $first_space_pos));

			$sub_str = $first_content.$second_content;


			$words = explode(' ', $str);
			$input_words = explode(' ', $input_str);

			/*Проверка на количество слов входного запроса*/
			$arr_q = explode(' ', $sub_str);

			$result = '';


			if (count($arr_q) == 1) {
				$index = array_search($sub_str, $words);

				if ($index > 0) {
					$result .= '...';
					$tmp_index = ($index-1);
					$result .= $input_words[$tmp_index].' ';
				}

				$result .= $input_words[$index];

				if ($index < (count($words) - 1)) {
					$tmp_index = ($index + 1);
					$result .= ' '.$input_words[$tmp_index].'...';
				}

			} else {
				$result = '...';

				foreach ($arr_q as $key => $val) {
					$index = array_search($val, $words);
					$result .= $input_words[$index].' ';
				}


				$result = mb_substr($result, 0, strlen($result) - 1);
				$result .= '...';
			}
			/*EOF Проверка на количество слов входного запроса*/

			
			return $result;
		}	

		return false;
	}

	$result = false;	

	//$result = getStrongText('<b>Корзун</b>');
	//$result = getStrongTextBody('это не единственное достоинство партии', 'Стои');

	if ((CModule::IncludeModule('search'))&&(CModule::IncludeModule('iblock'))){
		$category_ids = array(
			array(),
			array(5, 4, 14, 34, 33),
			9,
			//array(),
			2, 13, 1
		);

		$query = trim(strip_tags($_POST['query_string']));
		$category = (int)$_POST['category_number'];
		$category = $category_ids[$category];

		$category = (empty($category)) ? false : $category;

		$result = $category;


		$obSearch = new CSearch;
		$obSearch->Search(array(
                            "QUERY" => $query,
                            "CHECK_DATES" => 'Y',
                            "MODULE_ID" => 'iblock',
                            "PARAM2" => $category
            )
        );

		$result = array();

		while ($res = $obSearch->GetNext()){
			$result_item = array();
        	$id = $res['ITEM_ID'];
        	


        	$result_item['TITLE'] = $res['TITLE'];
        	$result_item['TITLE_FORMATED'] = $res['TITLE_FORMATED'];
        	$result_item['URL'] = $res['URL'];
        	$result_item['BODY_FORMATED'] = $res['BODY_FORMATED'];
        	$result_item['BODY'] = $res['BODY'];

        	$title_output = getStrongTextTitle($res['TITLE'], $query);
        	$result_item['OUTPUT'] = false;
        	$result_item['SECOND_OUTPUT'] = false;

        	if ($title_output) {
        		$result_item['OUTPUT'] = 'TITLE';
        	} else {
        		$result_item['SECOND_OUTPUT'] = getStrongTextBody(strip_tags($res['BODY_FORMATED']), $query);
        	}

        	if ($result_item['OUTPUT'] || $result_item['SECOND_OUTPUT']) {
        		$result[] = $result_item;
        	}
    	}
	}

	echo json_encode($result);
?>
