<?
	require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
    require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/search/prolog.php");
?>

<?
    $result = array();
    error_reporting(E_ALL);
    if(CModule::IncludeModule("search")){
        $sTableID = "tbl_search_phrase_list";
        
        $dbStatistic = CSearchStatistic::GetList(array('COUNT'=>'desc'), array('!PHRASE' => false), array('PHRASE', 'COUNT', 'COUNT'), true);        
        $dbStatistic = new CAdminResult($dbStatistic, $sTableID);
        $dbStatistic->NavStart(5);
        
        while($arStatistic = $dbStatistic->NavNext(true, 'f_')) {
            $result[] = $arStatistic;
        }
    }  
    echo json_encode($result);
?>
