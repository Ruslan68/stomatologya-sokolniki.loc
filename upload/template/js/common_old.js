'use strict';
if(!window.console) window.console = {};
if(!window.console.memory) window.console.memory = function() {};
if(!window.console.debug) window.console.debug = function() {};
if(!window.console.error) window.console.error = function() {};
if(!window.console.info) window.console.info = function() {};
if(!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if(!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function(){
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

// placeholder
//-----------------------------------------------------------------------------
  $('input[placeholder], textarea[placeholder]').placeholder();
    $('.bot-nav li a').each(function () {
        var location = window.location.href;
        var link = this.href;
        if(location == link) {
            $(this).addClass('active');
        }
    });
    if($('.fancybox').length) {
        $('.fancybox').fancybox();
    }
    $('.nav-block li a').each(function () {
        var location = window.location.href;
        var link = this.href;
        if(location == link) {
            $(this).addClass('active');
        }
    });
    $('.main-slider').slick({
        arrows:false,
        autoplay: true,
        autoplaySpeed: 2000
    });
    $('.main-offers-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots:true,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    dots:false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows:false,
                    dots:false
                }
            }
        ]
    });
    $('.main-gallery-slider').slick({});
    $('.partner-slider-about').slick({
        slidesToShow: 3,
        slidesToScroll: 1
    });
    $('.popup-slider').slick();
    $('.team-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    dots:false
                }
            }
        ]
    });
    $('.team-direction-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                dots:false
            }
        }
    ]
    });
    $('.work-direction-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1
    //     responsive: [
    //         {
    //         breakpoint: 768,
    //         settings: {
    //             slidesToShow: 2,
    //             slidesToScroll: 2
    //         }
    //     },
    //     {
    //         breakpoint: 480,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1,
    //             dots:false
    //         }
    //     }
    // ]
    });
    $('.certificates-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow:1
                }
            }
        ]
    });
    $('.partner-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow:1
                }
            }
        ]
    });
    $('.top-nav-btn').on('click',function(){
        var $this = $(this);
        if($('.top-nav-btn').hasClass('btn-open')){
            $('body').css('overflow', 'hidden');
            $('.top-nav-btn').addClass('btn-close').removeClass('btn-open');
            $('.bot-header').addClass('bot-nav-open');
            $('.header').addClass('header-open');

        }else{
            $this.addClass('btn-open').removeClass('btn-close');
            $('body').css('overflow', 'auto');
            $('.bot-header').removeClass('bot-nav-open');
            $('.header').removeClass('header-open');
        }
    });
    $('.map').on('click', function () {
        if($(this).hasClass('map-active')){
            $(this).removeClass('map-active');
        }else{
            $(this).addClass('map-active');
        }

    });
    $('.btn-call').on('click', function (e) {
        e.preventDefault();
        $('.popup-call').addClass('popup-open');
        if($('.popup-call').hasClass('popup-open')){
            $('.popup-call').addClass('popup-open');
        }else{
            $('.popup-call').removeClass('popup-open');
        }
    });
    $('.btn-app').on('click', function (e) {
		//debugger;
		if($(this).hasClass('link-offers')){
			
			var id = $(this).data('id');
			$('.popup-appointment').find('input[name="action_order"]').val(id);
			
		}
        e.preventDefault();
        $('.popup-appointment').addClass('popup-open');
        if($('.popup-appointment').hasClass('popup-open')){
            $('.popup-appointment').addClass('popup-open');
        }else{
            $('.popup-appointment').removeClass('popup-open');
        }
    });
    $('.btn-director').on('click', function (e) {
        e.preventDefault();
        $('.popup-director').addClass('popup-open');
        if($('.popup-director').hasClass('popup-open')){
            $('.popup-director').addClass('popup-open');
        }else{
            $('.popup-director').removeClass('popup-open');
        }
    });
    $('.main-gallery-slider .slide').on('click', function (e) {
        e.preventDefault();
        $('.popup-equ').addClass('popup-open');
        if($('.popup-equ').hasClass('popup-open')){
            $('.popup-equ').addClass('popup-open');
            console.log(11)
        }else{
            $('.popup-equ').removeClass('popup-open');
        }
    });
    $('.equipment-item .equipment-media').on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
            $thisItem = $this.parents('.equipment-item'),
            $thisPopup = $this.parents('.equipment-item').find('.popup-equ');
        $('.popup-slider').slick("setPosition", 0);
        $thisPopup.addClass('popup-open');
        $thisItem.removeClass('equipment-item-open');
        if($thisPopup.hasClass('popup-open')){
            $thisPopup.addClass('popup-open');
            $('.equipment-page-wrapper').addClass('equipment-open');
            $thisItem .addClass('equipment-item-open');
        }else{
            $thisPopup.removeClass('popup-open');
            $('.equipment-page-wrapper').removeClass('equipment-open');
            $thisItem.removeClass('equipment-item-open');
        }
    });
    $('.link-block a').on('click', function (e) {
        e.preventDefault();
        $('.popup-appointment').addClass('popup-open');
        if($('.popup-appointment').hasClass('popup-open')){
            $('.popup-appointment').addClass('popup-open');
        }else{
            $('.popup-call').removeClass('popup-open');
        }
    });
    $('.close').on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('popup-open');
        $('.equipment-page-wrapper').removeClass('equipment-open');
        $('.equipment-item').removeClass('equipment-item-open');
    });
    $('.popup-overlay').on('click', function (e) {
        e.preventDefault();
        $('.popup').removeClass('popup-open');
        $('.equipment-page-wrapper').removeClass('equipment-open');
        $('.equipment-item').removeClass('equipment-item-open');
    });
    $('.accordion-default-skin .accordion-item-title').on("click", function () {
        var $this = $(this),
            $thisItem = $this.closest('.accordion-item'),
            $thisContent = $thisItem.find('.accordion-item-content');
        if($thisItem.hasClass('open')) {
            $thisItem.removeClass('open');
            $thisContent.stop(true, true).slideUp();
        } else {
            $thisItem.addClass('open').siblings().removeClass('open').find('.accordion-item-content').stop(true, true).slideUp();
            $thisContent.stop(true, true).slideDown();
        }
    });
    $(".custom_select-left .current_option").on('click', function(){
        var $this = $(this),
            $selectLeft = $this.parent(".custom_select-left"),
            $customOptions = $selectLeft.find('.custom_options');
        $this.addClass("open");
        if ($this.hasClass("open")) {
            $customOptions.removeClass('hidden');
            $this.removeClass("open")
        }
        else {
            $this.addClass("open");
            $customOptions.addClass('hidden');
        }
    });
    $(".custom_select-left .custom_options li").on('click', function(){
       var $this = $(this),
           $current_option = $this.parents().find('.custom_select-left .current_option span'),
           $customSelectLeft = $(".custom_select-left .current_option"),
           choosenValue = $this.attr("data-value");
        $current_option.text($(this).text());
        $customSelectLeft.attr("data-value", choosenValue);
        $(".custom_select-left .custom_options").addClass('hidden');
    });
    $(".custom_select-right .current_option").on('click', function(){
        var $this = $(this),
            $selectLeft = $this.parent(".custom_select-right"),
            $customOptions = $selectLeft.find('.custom_options');
        if ($this.hasClass("open")) {
            $customOptions.removeClass('hidden');
            $this.removeClass("open")
        }
        else {
            $this.addClass("open");
            $customOptions.addClass('hidden');
        }
    });
    $(".custom_select-right .custom_options li").on('click', function(){
       var $this = $(this),
           $current_option = $this.parents().find('.custom_select-right .current_option span'),
           $customSelectLeft = $(".custom_select-left .current_option"),
           choosenValue = $this.attr("data-value");
        $current_option.text($(this).text());
        $customSelectLeft.attr("data-value", choosenValue);
        $(".custom_select-right .custom_options").addClass('hidden');
    });
    $('.scroll-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
    $('#gallery-page-container').mixItUp();
    $('#gallery-container').mixItUp();
});

jQuery(function($){
    $(document).mouseup(function (e){
        var div = $(".custom_select-left .custom_options");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.addClass('hidden');
        }
    });
});
jQuery(function($){
    $(document).mouseup(function (e){
        var div = $(".custom_select-right .custom_options");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            div.addClass('hidden');
        }
    });
});
$(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
        $('.header').addClass('header-scroll');
    }
    else {
        $('.header').removeClass('header-scroll');
    }
});
$(window).scroll(function () {
    if ($(this).scrollTop() > 870) {
        $('.scroll-to-top').fadeIn().css('display','block');
    }else {
        $('.scroll-to-top').fadeOut().css('display','none');
    }
});
function checkWidth() {
    var windowSize = $(window).width();
    if (windowSize <= 992) {
        $('.sub-nav .sub-nav-link').on("click", function (e) {
            e.preventDefault();
            $('.sub-nav').addClass('open-nav');
            var $this = $(this),
                $thisItem = $this.closest('.sub-nav'),
                $thisContent = $thisItem.find('.sub-nav ul');
            if($thisItem.hasClass('open')) {
                $thisItem.removeClass('open');
                $thisContent.stop(true, true).slideUp();
            } else {
                $thisItem.addClass('open').siblings().removeClass('open').find('.sub-nav ul').stop(true, true).slideUp();
                $thisContent.stop(true, true).slideDown();
            }
        });
    }
    else {
        $(window).on('scroll',function () {
            if ($(this).scrollTop() > 40) {
                $('.left-nav').addClass('left-nav-scroll');
                }else {
                $('.left-nav').removeClass('left-nav-scroll');
            }

        });
    }
}
$(function(){
    $(document).scroll(function(){
        if($(this).scrollTop() >= $('.form-bottom').offset().top - 1000) {
            $(".left-nav").addClass("fixed");
        } else {
            $(".left-nav").removeClass("fixed");
        }
    });

});
checkWidth();
$(window).resize(checkWidth);
$(window).load(function() {
    var wow = new WOW({
        boxClass:     'wow',
        animateClass: 'animated',
        offset:       200,
        mobile:       false,
        live:         true,
        callback:     function(box) {
        }
    });
    wow.init();
});
$(document).ready(function() {
    var movementStrength = 25;
    var height = movementStrength / $(window).height();
    var width = movementStrength / $(window).width();
    $(".left-img-container .main-about__image").mousemove(function(e){
        var pageX = e.pageX - ($(window).width() / 2);
        var pageY = e.pageY - ($(window).height() / 2);
        var newvalueX = width * pageX * +1 + 10;
        var newvalueY = height * pageY * +1 + 20;
        $('.left-img-container .main-about__image').css("background-position", newvalueX+"%     "+newvalueY+"%");
    });
});
$(document).ready(function() {
    var movementStrength = 25;
    var height = movementStrength / $(window).height();
    var width = movementStrength / $(window).width();
    $(".right-img-container .main-about__image").mousemove(function(e){
        var pageX = e.pageX - ($(window).width() / 2);
        var pageY = e.pageY - ($(window).height() / 2);
        var newvalueX = width * pageX * +1 + 10;
        var newvalueY = height * pageY * +1 + 10;
        $('.right-img-container .main-about__image').css("background-position", newvalueX+"%     "+newvalueY+"%");
    });
});
$(document).ready(function() {
    var movementStrength = 25;
    var height = movementStrength / $(window).height();
    var width = movementStrength / $(window).width();
        $(".palm-b:before").mousemove(function(e){
        var pageX = e.pageX - ($(window).width() / 2);
        var pageY = e.pageY - ($(window).height() / 2);
        var newvalueX = width * pageX * + 1 + 10;
        var newvalueY = height * pageY * + 1 + 10;
        $('.palm-b:before').css("background-position", newvalueX + "%" +newvalueY + "%");
    });
});

jQuery(function($){
   $("input[name='user_phone']").mask("+7 999 999 99 99");
});
