$( document ).ready(function() {
    $('.calc_block_select select').styler({
		selectSmartPositioning:false
	});
	$('.calc_block_select .calc_text > span').hover(
		function() {
		$(this).parent('.calc_text').next('.calc_help').show();
		},
		function() {
		$(this).parent('.calc_text').next('.calc_help').hide();
	});
	$('.calc_block_buttons .calc_button').click(function(){
		$('.calc_block_buttons .calc_button').removeClass('active');
		$(this).addClass('active');
	});
	$('.calc_block_buttons .calc_button.otsut').click(function(){
		$('.calc_img > span').addClass('active');
	});
	$('.calc_block_buttons .calc_button.chast').click(function(){
		$('.calc_img > span').removeClass('active');
	});
	$('.calc_img > span').click(function(){
		$(this).toggleClass('active');
		fnCalcReset();
	});
	$('.calc_block_select select').on('change',function(){
		fnCalcReset();		
	});
	
	$('.calc_submit').click(fnErrors);	
});

/*Функция проверки на заполнение*/
function fnErrors(){
	var error_text = '';
	
	
	if($('.calc_img > span.active').length == 0){
		error_text += '<div>Не выбрано ни одного зуба</div>';
	}
	if($('.calc_select_1 select option:selected').val() == 'none'){		
		error_text += '<div>Укажите как давно были удалены зубы</div>';
	}
	if($('.calc_select_2 select option:selected').val() == 'none'){
		error_text += '<div>Не выбрана система имплантов</div>';
	}
	if($('.calc_select_3 select option:selected').val() == 'none'){
		error_text += '<div>Не выбрана система коронок</div>';
	}
	if(error_text.length > 0){
		$('.calc_errors').html('<div class="calc_text">Не все поля заполнены:</div>'+error_text)
	}else{
		fnCalc();
	}	
}

/*Функция для сброса расчета*/
function fnCalcReset(){
	$('.calc_errors').html('');
	$('.calc_bot_text').html('');
	$('.calc_summ').html('');
	$('.calc_submit').show();
}

/*Функция расчета*/
function fnCalc(){	
	
	var calc_summ = '';
	var bot_text = '';
	var zubov = $('.calc_img > span.active').length; 								//количество зубов
	var price_imp = $('.calc_select_2 select option:selected').data('price');		//цена имплантов
	var price_kor = $('.calc_select_3 select option:selected').data('price');		//цена коронок
	
	//получаем итоговую сумму
	if(parseInt(zubov) > 0 && parseInt(price_imp) > 0 && parseInt(price_kor) > 0){
		calc_summ = parseInt(zubov) * (parseInt(price_imp) + parseInt(price_kor));
	}	
	
	$('.calc_submit').hide();
	
	// Условия для вывода текста	
	if($('.calc_select_1 select option:selected').val() == '1'){
		bot_text += '<div>Стоимость указана "под ключ".<div>';
	}
	if(
		($('.calc_img > .l_t_5').hasClass('active') ||
		$('.calc_img > .l_t_6').hasClass('active') ||
		$('.calc_img > .l_t_7').hasClass('active') ||
		$('.calc_img > .r_t_5').hasClass('active') ||
		$('.calc_img > .r_t_6').hasClass('active') ||
		$('.calc_img > .r_t_7').hasClass('active')) &&
		($('.calc_select_1 select option:selected').val() == '1-2' ||
		$('.calc_select_1 select option:selected').val() == '2-3')
		){
		bot_text += '<div>Вероятнее всего потребуется операция по восстановлению объема костной ткани дна гайморовой пазухи (синус-лифтинг). При одиночно отсутствующем зубе проводится закрытый синус-лифтинг, при установке нескольких имплантов - открытый.<br>Стоимость операции закрытого синус-лифтинга в области 1-го зависит от индивидуальных особенностей и составляет от 25 до 79 тысяч. Стоимость операции открытого синус-лифтинга, также - определяется клинической картиной и индивидуальными особенностями и составляет от 25 до 79 тысяч. Костный материал во всех случаях оплачивается отдельно. Его стоимость варьирует от 9 до 15 тысяч рублей за грамм. Количество костного материала будет зависеть от объема операции и будет определена доктором.<div>';
	}
	if(
		($('.calc_img > .l_b_1').hasClass('active') ||
		$('.calc_img > .l_b_2').hasClass('active') ||
		$('.calc_img > .l_b_3').hasClass('active') ||
		$('.calc_img > .l_b_4').hasClass('active') ||
		$('.calc_img > .l_b_5').hasClass('active') ||
		$('.calc_img > .l_b_6').hasClass('active') ||
		$('.calc_img > .l_b_7').hasClass('active') ||
		$('.calc_img > .l_b_8').hasClass('active') ||
		$('.calc_img > .r_b_1').hasClass('active') ||
		$('.calc_img > .r_b_2').hasClass('active') ||
		$('.calc_img > .r_b_3').hasClass('active') ||
		$('.calc_img > .r_b_4').hasClass('active') ||
		$('.calc_img > .r_b_5').hasClass('active') ||
		$('.calc_img > .r_b_6').hasClass('active') ||
		$('.calc_img > .r_b_7').hasClass('active') ||
		$('.calc_img > .r_b_8').hasClass('active')) &&
		($('.calc_select_1 select option:selected').val() == '1-2' ||
		$('.calc_select_1 select option:selected').val() == '2-3')
	){
		bot_text += '<div>Может потребоваться операция костной аугментации, стоимостью 24 960 в области одного зуба.</div>';
	}
	// Выводим описание
	if(bot_text.length > 0){
		$('.calc_bot_text').html('<div class="calc_text">Описание:</div>'+bot_text);
	}

	// Выводим итоговую цену
	$('.calc_summ').html('Сумма: '+number_format(calc_summ, 0, '.', ' ')+' Руб.');

}

// Функция для преобразования цены в number_format
function number_format( number, decimals, dec_point, thousands_sep ) {	// Format a number with grouped thousands
	// 
	// +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	 bugfix by: Michael White (http://crestidg.com)

	var i, j, kw, kd, km;

	// input sanitation & defaults
	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");


	return km + kw + kd;
}
