$(document).ready(function() {
	$('a[href^="tel:"]').click(function(){
		yaCounter32528320.reachGoal('klik-telefon');
		ga('send', 'pageview', '/klik-telefon');
		console.log('klik-telefon');
	});
	$('#header .link-block a, #header .btn-right .btn-item.btn-app').click(function(){
		yaCounter32528320.reachGoal('klik-zapisat');
		ga('send', 'pageview', '/klik-zapisat');

		sendYandex('Form_Click_Order_header');
		sendGtag('Click', 'Form', 'Order_header');

		console.log('klik-zapisat');
	});
	$('#header .btn-right .btn-item.btn-call').click(function(){
		yaCounter32528320.reachGoal('klik-zakaz-zvon');
		ga('send', 'pageview', '/klik-zakaz-zvon');

		sendYandex('Form_Click_Callback');
		sendGtag('Click', 'Form', 'Click_Callback');

		console.log('klik-zakaz-zvon');
	});

	$('#header .btn-right .btn-item.btn-sub').click(function(){
		sendYandex('Form_Click_Subscribe');
		sendGtag('Click', 'Form', 'Click_Subscribe');

		console.log('klik-rassilka');
	});


	$('#btn_zapis').click(function(){
		sendYandex('Button_Click_Schedule_an_appointment_banner');
		sendGtag('Click', 'Button', 'Schedule_an_appointment_banner');
	});

	$(".ds_inner .align_center .btn_main").click(function(){
		sendYandex('Button_Click_Experienced_doctors');
		sendGtag('Click', 'Button', 'Experienced_doctors');
	});


	//~ Goal in file /bitrix/components/poiskpro/contact_form/templates/.default/template.php
	//~ $('.form-contact.poiskpro-form form #submit-fot').click(function(){
		//~ yaCounter32528320.reachGoal('ysp-otpr-obrst-svjaz');
		//~ ga('send', 'pageview', '/ysp-otpr-obrst-svjaz');
		//~ console.log('ysp-otpr-obrst-svjaz');
	//~ });
	//~ Goal in file /bitrix/components/poiskpro/order_form/templates/popup_order/template.php
	//~ $('.popup.popup-appointment form #submit-app').click(function(){
		//~ yaCounter32528320.reachGoal('ysp-zapis-na-priem');
		//~ ga('send', 'pageview', '/ysp-zapis-na-priem');
		//~ console.log('ysp-zapis-na-priem');
	//~ });
	//~ Goal in file /bitrix/components/poiskpro/order_form/templates/popup_call/template.php
	//~ $('.popup.popup-call form #submit-call').click(function(){
		//~ yaCounter32528320.reachGoal('ysp-zakaz-zvonka');
		//~ ga('send', 'pageview', '/ysp-zakaz-zvonka');
		//~ console.log('ysp-zakaz-zvonka');
	//~ });
	//~ Goal in file /bitrix/components/poiskpro/order_form/templates/.default/template.php
	//~ $('.footer-form form #submit-fot').click(function(){
		//~ yaCounter32528320.reachGoal('ysp-zapis-na-priem-vnizy');
		//~ ga('send', 'pageview', '/ysp-zapis-na-priem-vnizy');
		//~ console.log('ysp-zapis-na-priem-vnizy');
	//~ });
})
