<link href="/upload/template/css/calc.css" type="text/css"  rel="stylesheet" />
<link href="/upload/template/css/jquery.formstyler.css" type="text/css"  rel="stylesheet" />
<link href="/upload/template/css/jquery.formstyler.theme.css" type="text/css"  rel="stylesheet" />
<script src="/upload/template/js/calc.js"></script>
<script src="/upload/template/js/jquery.formstyler.min.js"></script>

<div class="clear"></div>

        <div class="title-section calc_t">
          <div class="text-block__title">
			калькулятор стоимости
          </div>         
        </div>
        
        
        
        <div id="calc_block">
			<div class="calc_block_buttons">
				<div class="calc_button chast active">частичное отсутствие зубов</div>
				<div class="calc_button otsut">полное отсутствие зубов</div>
			</div>
			<div class="calc_block_main">
				<div class="calc_block_left">
					<div class="calc_text">Выберите отсутствующие зубы</div>
					<div class="calc_img">
						<img src="/images/calc_img.png">
						<span class="l_t_8">8</span>
						<span class="l_t_7">7</span>
						<span class="l_t_6">6</span>
						<span class="l_t_5">5</span>
						<span class="l_t_4">4</span>
						<span class="l_t_3">3</span>
						<span class="l_t_2">2</span>
						<span class="l_t_1">1</span>
						<span class="r_t_1">1</span>
						<span class="r_t_2">2</span>
						<span class="r_t_3">3</span>
						<span class="r_t_4">4</span>
						<span class="r_t_5">5</span>
						<span class="r_t_6">6</span>
						<span class="r_t_7">7</span>
						<span class="r_t_8">8</span>
						
						<span class="l_b_8">8</span>
						<span class="l_b_7">7</span>
						<span class="l_b_6">6</span>
						<span class="l_b_5">5</span>
						<span class="l_b_4">4</span>
						<span class="l_b_3">3</span>
						<span class="l_b_2">2</span>
						<span class="l_b_1">1</span>
						<span class="r_b_1">1</span>
						<span class="r_b_2">2</span>
						<span class="r_b_3">3</span>
						<span class="r_b_4">4</span>
						<span class="r_b_5">5</span>
						<span class="r_b_6">6</span>
						<span class="r_b_7">7</span>
						<span class="r_b_8">8</span>							
					</div>				
				</div>
				<div class="calc_block_right">
					<div class="calc_block_select">
						<div class="calc_select_1">
							<div class="calc_text">КАК ДАВНО БЫЛИ УДАЛЕНЫ ЗУБЫ<span>?</span></div>
							<div class="calc_help">Эта информация необходима, чтобы определить – с какой вероятностью вам потребуется костная пластика для наращивания объема кости в месте имплантации.</div>
							<select>
								<option value="none">- - - - - - - - - </option>
								<option value="1">менее 1 года назад</option>
								<option value="1-2" >1-2 года назад</option>
								<option value="2-3" >2-3 года назад</option>
							</select>
						</div>
						<div class="calc_select_2">
							<div class="calc_text">ВЫБОР СИСТЕМЫ ИМПЛАНТОВ<span>?</span></div>
							<div class="calc_help">Выберете одну из предложенных систем имплантации разных производителей.</div>
							<select>
								<option value="none">- - - - - - - - - </option>
								<option value="nobel" data-price="70490">Нобель Байокеа (США)</option>
								<option value="ankiloz" data-price="54400">Анкилоз СХ (Дентсплай, Германия)</option>
								<option value="osstem" data-price="33000">Осстем (Корея)</option>
							</select>
						</div>
						<div class="calc_select_3">
							<div class="calc_text">ВЫБОР СИСТЕМЫ коронок<span>?</span></div>
							<div class="calc_help">Выберете тип коронки на импланте. К каждому типу коронки автоматически подбирается соответствующий тип абатмента (металлический или керамический из диоксида циркония, соответственно).</div>
							<select>
								<option value="none">- - - - - - - - - </option>
								<option value="metall" data-price="18500">Металлокерамика</option>
								<option value="nemetall" data-price="30500">Безметалловая керамика (циркониевые коронки)</option>								
							</select>
						</div>
					</div>
					<div class="calc_block_sub">					
						<div class="calc_submit">рассчитать</div>
					</div>
					
					<div class="calc_bot_text"></div>
					<div class="calc_errors"></div>
					<div class="calc_summ"></div>
					
					
				</div>			
			</div>			        
        </div>

<div class="clear"></div>
