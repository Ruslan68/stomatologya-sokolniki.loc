<? if($APPLICATION->GetCurDir() != '/contacts/'): ?>
<div itemscope itemtype="http://schema.org/Organization" style="display: none;">
<span itemprop="name">Солнечный остров</span>
<a itemprop="url" href="https://stomatologya-sokolniki.ru">https://stomatologya-sokolniki.ru</a>
<img alt="Логотип Солнечный остров" src="/upload/template/image/logo-blue.png" title="Логотип Солнечный остров" itemprop="logo">
<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress">метро Сокольники ул. Шумкина, дом 11А</span>
<span itemprop="postalCode"></span>
<span itemprop="addressLocality">Москва, Россия</span>
</div>
<span itemprop="telephone">+7 (495) 603-30-00</span>
<span itemprop="email">info@stomatologya-sokolniki.ru</span>
<span itemprop="email">reception@stomatologya-sokolniki.ru</span>
</div>
<? endif; ?>
