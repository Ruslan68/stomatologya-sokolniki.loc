<div class="title-wrap">
	 Необходимые документы для получения кредита:
</div>
<div class="main-text">
	<p>
 <span style="font-weight: bold;"><span style="color: #004a80;">Обязательный документ:</span></span>
	</p>
	<div class="list-wrap">
		<ul>
			<li>
			паспорт гражданина РФ; </li>
			<li>
			оригинал пенсионного удостоверения (для неработающих пенсионеров). </li>
		</ul>
	</div>
	<p>
 <span style="color: #0076a4;"><span style="font-weight: bold;"><br>
 </span></span>
	</p>
	<p>
 <span style="color: #0076a4;"><span style="font-weight: bold;">Дополнительные документы (Банк вправе запросить при необходимости) Предоставляются, если сумма Кредита превышает 100 000 рублей:</span></span>
	</p>
	<div class="list-wrap">
		<ul>
			<li>
			Водительское удостоверение; </li>
			<li>
			Паспорт гражданина Российской Федерации, удостоверяющий личность гражданина Российской Федерации за пределами Российской Федерации; </li>
			<li>
			свидетельство о присвоении ИНН; </li>
			<li>
			страховое свидетельство обязательного пенсионного страхования. </li>
		</ul>
	</div>
	<p>
 <span style="color: #0076a4;"><span style="font-weight: bold;"><span style="color: #000000;"><br>
 </span></span></span>
	</p>
	<p>
 <span style="color: #0076a4;"><span style="font-weight: bold;"><span style="color: #000000;">Для индивидуальных предпринимателей, если первоначальный взнос составляет менее 40% от стоимости приобретаемого Товара:</span></span></span>
	</p>
	<div class="list-wrap">
		<ul>
			<li>Свидетельство о регистрации ИП или Свидетельство о внесении записи в единый государственный реестр ИП;</li>
			<li>Свидетельство о присвоении ИНН. </li>
		</ul>
	</div>
	<p>
 <span style="color: #0076a4;"><span style="font-weight: bold;"><span style="color: #000000;"><br>
 </span></span></span>
	</p>
	<p>
 <span style="color: #0076a4;"><span style="font-weight: bold;"><span style="color: #000000;">Для неработающих пенсионеров (получающих трудовую пенсию по старости), в случае если сумма Кредита превышает 100 000 рублей:</span></span></span>
	</p>
	<div class="list-wrap">
		<ul>
			<li>справка о сумме пенсии и сумме ежемесячных денежных выплат (далее - ЕДВ)</li>
		</ul>
	</div>
	<p>
	</p>
	<p>
 <span style="font-weight: bold;"><span style="color: #ff0000;"><br>
 </span></span>
	</p>
	<p>
 <span style="font-weight: bold;"><span style="color: #ff0000;">Подробную информацию по условиям предоставления кредита АО "Кредит Европа Банк", а также по условиям погашения кредита, Вы можете получить у сотрудников компании или у сотрудников Банка. </span></span>
	</p>
 <span style="font-weight: bold;"><span style="color: #ff0000;"> </span></span>
	<p>
 <span style="font-weight: bold;"><span style="color: #ff0000;">
		Кредиты предоставляются АО «Кредит Европа Банк» на цели оплаты работ/услуг.</span></span>
	</p>
</div>
 <br>
