<ul>
    <li>
        Налоговые льготы*– уменьшают налогооблагаемую базу по налогу на прибыль!
    </li>
    <li>
        Уменьшение непредвиденных расходов, которые могут возникнуть в связи с временной нетрудоспособностью сотрудников!
        Как следствие – сокращение потери рабочего времени, вызванного болезнью сотрудников.
    </li>
    <li>
        Повышение престижности рабочих мест вашей компании!
    </li>
    <li>
        Снижение «текучки» кадров!
    </li>
    <li>
        Корпоративное медицинское обслуживание сегодня – это существенно более выгодный для бюджета Вашей компании способ мотивации сотрудников, нежели повышение заработной платы.
    </li>
    <li>
        Формирование корпоративного духа, позитивный имидж руководителя и компании в целом.
    </li>
</ul>