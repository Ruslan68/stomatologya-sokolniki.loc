<ul>
	<li>
		Сертификат действителен в течение 3-х месяцев с момента приобретения и не подлежит обмену на денежные средства или восстановлению в случае утери;
	</li>
	<li>
		Подарочный сертификат не является именным. При невозможности использования подарка, Вы можете передать сертификат третьему лицу для его использования в пределах установленных правил;
	</li>
	<li>
		Для Вашего удобства запись на обследование и лечение осуществляется предварительно по одному из наших контактных телефонов.
	</li>
</ul>