<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
global $APPLICATION;

$aMenuLinksExt =$APPLICATION->IncludeComponent(
	"bitrix:menu.sections",
	"",
	Array(
		"CACHE_TIME" => "864000",
		"CACHE_TYPE" => "Y",
		"DEPTH_LEVEL" => "1",
		"DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMET_CODE#",
		"IBLOCK_ID" => "24",
		"IBLOCK_TYPE" => "other_en",
		"ID" => $_REQUEST["ID"],
		"IS_SEF" => "Y",
		"SECTION_PAGE_URL" => "#SECTION_CODE#/",
		"SECTION_URL" => "",
		"SEF_BASE_URL" => "/en/patients/news/"
	)
);


$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);



?>
