<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лечение в кредит");
?>

<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumb",
	Array(
		"PATH" => "",
		"SITE_ID" => SITE_ID,
		"START_FROM" => "0"
	)
);?>


<section class="wrapper credit-info">
    <div class="container">
        <div class="col-xs-12 col-md-6 col-md-offset-6">
			<div class="title-section">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/text1.php"
	)
);?>
			</div>
			<div class="credit-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/text2.php"
	)
);?>
			</div>
        </div>
    </div>
</section>
<section class="wrapper credit-wrapper">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="credit-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/text3.php"
	)
);?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-md-offset-6">
				<div class="credit-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/text4.php"
	)
);?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="credit-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/credit/text5.php"
	)
);?>
				</div>
			</div>
        </div>
    </div>
</section>


<?$APPLICATION->IncludeComponent(
	"poiskpro:order_form",
	"credit_form",
	array(
		'OK_TEXT' => 'Заявка принята',
		'REQUIRED_FIELDS' => array('NAME', 'PHONE'),
		"AJAX_MODE" => 'Y',
		"TYPE" => 'credit_form'
	),
false
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>