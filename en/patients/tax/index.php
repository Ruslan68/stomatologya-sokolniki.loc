<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Налоговый вычет на стоматологические услуги");
$APPLICATION->SetPageProperty("keywords", "стоматология в Сокольниках, стоматологическая клиника &quot;Солнечный остров&quot;, стоматологическая клиника, лазер, эстетика,");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» в Сокольниках это SPA-курорт для Ваших зубов. Для этого у нас есть все необходимое!");
$APPLICATION->SetTitle("Налоговый вычет");
?>


<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"breadcrumb",
	Array(
		"PATH" => "",
		"SITE_ID" => SITE_ID,
		"START_FROM" => "0"
	)
);?>

<section class="wrapper tax-wrapper">
	<div class="container">
        <div class="title-section">
			<div class="text-block__title">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text0.php"
	)
);?> 
			</div>
        </div>
        <div class="row">
			<div class="col-xs-12 col-md-6 wow fadeInLeft">
				<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text1.php"
	)
);?> 
				</div>
			</div>
			<div class="col-xs-12 col-md-6 wow fadeInRight">
				<div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text2.php"
	)
);?>
				</div>
			</div>
        </div>
    </div>
</section>

<section class="wrapper tax-documentation">
    <div class="container">
        <div class="row">
			<div class="col-xs-12 col-md-6 col-md-push-6 tax-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text3.php"
	)
);?>
			</div>
			<div class="col-xs-12 col-md-6 col-md-pull-6 tax-item">
				<div class="blue-text-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text4.php"
	)
);?>
				</div>
			</div>
        </div>
    </div>
</section>

<section class="wrapper tax-example">
    <div class="left-img-container">
        <div class="main-about__image">
          <!--<img src="image/main-about-img.png" alt="">-->
        </div>
    </div>
    <div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-6 tax-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text5.php"
	)
);?>
			</div>
			<div class="col-xs-12 col-md-6 tax-item">
				<div class="blue-text-item">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/tax/text6.php"
	)
);?>
				</div>
			</div>
		</div>
	</div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>