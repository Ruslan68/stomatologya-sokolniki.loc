<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Прайс-лист стоматологической клиники &quot;Солнечный остров&quot;");
$APPLICATION->SetPageProperty("keywords", "стоматология в Сокольниках, стоматологическая клиника &quot;Солнечный остров&quot;, стоматологическая клиника, лазер, эстетика,");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» в Сокольниках это SPA-курорт для Ваших зубов. Для этого у нас есть все необходимое!");
$APPLICATION->SetTitle("Цены");
?>

<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "pricePage", Array(
	"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"CACHE_GROUPS" => "N",	// Учитывать права доступа
		"CACHE_TIME" => "864000",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
		"IBLOCK_ID" => "18",	// Инфоблок
		"IBLOCK_TYPE" => "main_en",	// Тип инфоблока
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_FIELDS" => array(	// Поля разделов
			0 => "NAME",
			1 => "SORT",
		),
		"SECTION_ID" => "",	// ID раздела
		"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
		"SECTION_USER_FIELDS" => array(	// Свойства разделов
			0 => "",
			1 => "",
		),
		"TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>