<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6 price-info__item">
            <div class="blue-text-item">
                <div class="img-block">
					<img src="/upload/template/image/inf-price-1.png" alt="">
                </div>
                <div class="text-block">
					Полная стоимость услуг рассчитывается после консультации, осмотра врача и составления плана лечения.
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 price-info__item">
            <div class="blue-text-item">
                <div class="img-block">
					<img src="/upload/template/image/inf-price-2.png" alt="">
                </div>
                <div class="text-block">
					Оплату вы можете произвести удобным для Вас способом: наличными, пластиковой картой или безналичным переводом.
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 price-info__item">
            <div class="blue-text-item">
                <div class="img-block">
					<img src="/upload/template/image/inf-price-3.png" alt="">
                </div>
                <div class="text-block">
					Вы можете оплачивать лечение в Нашей клинике поэтапно
					<span>
						<a href="/patients/credit/">Подробнее</a>
					</span>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 price-info__item">
            <div class="blue-text-item">
                <div class="img-block">
					<img src="/upload/template/image/inf-price-4.png" alt="">
                </div>
                <div class="text-block">
					Вы можете проходить лечение в Нашей клинике в кредит
					<span>
						<a href="/patients/credit/">Подробнее</a>
					</span>
                </div>
            </div>
        </div>
    </div>
</div>