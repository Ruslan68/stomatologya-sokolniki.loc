<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Корпоративным клиентам");
$APPLICATION->SetPageProperty("keywords", "стоматология в Сокольниках, стоматологическая клиника &quot;Солнечный остров&quot;, стоматологическая клиника, лазер, эстетика,");
$APPLICATION->SetPageProperty("description", "Стоматологическая клиника «СОЛНЕЧНЫЙ ОСТРОВ» в Сокольниках это SPA-курорт для Ваших зубов. Для этого у нас есть все необходимое!");
$APPLICATION->SetTitle("Корпоративным клиентам");
?>

<section class="wrapper corporativ-wrapper">
      <div class="page-img">
        <img src="/upload/template/image/corp-img.png" alt="">
      </div>
      <div class="container">
        <div class="title-section">
          <div class="text-block__title">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text0.php"
	)
);?>
          </div>
          <div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text1.php"
	)
);?>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-6 col-md-offset-6">
            <div class="title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text2.php"
	)
);?>
            </div>
            <div class="main-text">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text3.php"
	)
);?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper benefits-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <div class="title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text4.php"
	)
);?>
            </div>
            <div class="list-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text5.php"
	)
);?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper advantage-wrapper">
      <div class="left-img-container left-img-news">
        <div class="main-about__image">
          <!--<img src="image/main-about-img.png" alt="">-->
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-6 col-md-offset-6">
            <div class="title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text6.php"
	)
);?>
            </div>
            <div class="list-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text7.php"
	)
);?> 
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper advantage-clinic">
      <div class="container">
        <div class="title-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text8.php"
	)
);?> 
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-6">
            <div class="list-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text9.php"
	)
);?>
            </div>
          </div>
          <div class="col-xs-12 col-md-6">
            <div class="list-wrap">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "inc",
		"COMPONENT_TEMPLATE" => ".default",
		"EDIT_TEMPLATE" => "",
		"PATH" => SITE_DIR."include/korporativnoe-obsluzhivanie/text10.php"
	)
);?>
            </div>
          </div>
        </div>
      </div>
    </section>
	
	
<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"faqBlock",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "864000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("", ""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "19",
		"IBLOCK_TYPE" => "other_en",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("", ""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC"
	)
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>