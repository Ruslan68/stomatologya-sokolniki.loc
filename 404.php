<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

/*$APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
	"LEVEL"	=>	"3",
	"COL_NUM"	=>	"2",
	"SHOW_DESCRIPTION"	=>	"Y",
	"SET_TITLE"	=>	"Y",
	"CACHE_TIME"	=>	"36000000"
	)
);*/?>
<section class="wrapper corporativ-wrapper" style="margin-top: 50px;">
	<div class="container">
		<div class="title-section">
          <div class="text-block__title"> <h1>Данная страница не существует!</h1></div>
          <div class="main-text"><p>Вы можете <a href="/">перейти на главную страницу сайта</a></p></div>
      	</div>
	</div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>